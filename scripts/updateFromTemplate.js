const fs = require('fs');
const path = require('path');
const git = require('simple-git')(path.join(__dirname, '../'));
const { renderString } = require('template-file');
const packageJson = require('../package.json');

function replaceFromTemplate(src, dst, variables) {
  const COMMENTS = '##### DO NOT MODIFY THIS FILE DIRECTLY #####';
  const data = fs.readFileSync(src, 'utf-8');
  const result = renderString(data, variables);
  fs.writeFileSync(dst, COMMENTS + '\n' + result);
}

// Update client version
const clientPackageJson = require('../client/package.json');
clientPackageJson.version = packageJson.version;
fs.writeFileSync(
  path.join(__dirname, '../client/package.json'),
  JSON.stringify(clientPackageJson, null, 2)
);

// Update server version
const serverPackageJson = require('../server/package.json');
serverPackageJson.version = packageJson.version;
fs.writeFileSync(
  path.join(__dirname, '../server/package.json'),
  JSON.stringify(serverPackageJson, null, 2)
);

// Replace variables in bitbucket-pipelines.yml from template
replaceFromTemplate(
  path.join(__dirname, './template/bitbucket-pipelines.yml'),
  path.join(__dirname, '../bitbucket-pipelines.yml'),
  { version: packageJson.version }
);
console.log('bitbucket-pipelines.yml has been updated with version');

// Replace variables in Dockerrun.aws.json from template
let dockerrunAwsJson = require('./template/Dockerrun.aws.json');
dockerrunAwsJson = Object.assign(
  { _comment: '##### DO NOT MODIFY THIS FILE DIRECTLY #####' },
  dockerrunAwsJson
);
const dockerrunAwsJsonStr = renderString(
  JSON.stringify(dockerrunAwsJson, null, 2),
  {
    version: packageJson.version,
  }
);
fs.writeFileSync(
  path.join(__dirname, '../Dockerrun.aws.json'),
  dockerrunAwsJsonStr
);
console.log('Dockerrun.aws.json has been updated with version');

// Add files to git stage
git.add(['client/package.json']);
git.add(['server/package.json']);
git.add(['bitbucket-pipelines.yml']);
git.add(['Dockerrun.aws.json']);
