module.exports = {
  product: (parent, args, context) => context.prisma.levelPrice({ id: parent.id }).product(),
};
