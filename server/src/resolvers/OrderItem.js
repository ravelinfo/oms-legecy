module.exports = {
  customerProduct: (parent, args, context) => context.prisma.orderItem({ id: parent.id }).levelPrice(),
};
