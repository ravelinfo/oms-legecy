module.exports = {
  fromCustomer: (parent, args, context) => context.prisma.notification({ id: parent.id }).fromCustomer(),
  fromAdmin: (parent, args, context) => context.prisma.notification({ id: parent.id }).fromAdmin(),
  toCustomer: (parent, args, context) => context.prisma.notification({ id: parent.id }).toCustomer(),
  toAdmin: (parent, args, context) => context.prisma.notification({ id: parent.id }).toAdmin(),
  orderCustomer: (parent, args, context) => context.prisma.notification({ id: parent.id }).orderCustomer(),
};
