const { getAdminId } = require('../utils');
const { getCustomerId } = require('../utils');
const { ORDER_STATUS } = require('../utils/constants');

module.exports = {
  adminMe: (root, args, context) => context.prisma.admin({ id: getAdminId(context) }),
  customerMe: (root, args, context) => context.prisma.customer({ id: getCustomerId(context) }),
  admins: (root, args, context) => context.prisma.admins(),
  admin: (root, args, context) => {
    getAdminId(context);
    return context.prisma.admin({ id: args.id });
  },
  customers: (root, args, context) => {
    getAdminId(context);
    return context.prisma.customers();
  },
  customer: (root, args, context) => {
    getAdminId(context);
    return context.prisma.customer({ id: args.id });
  },
  levels: (root, args, context) => {
    getAdminId(context);
    return context.prisma.levels();
  },
  level: (root, args, context) => {
    getAdminId(context);
    return context.prisma.level({ id: args.id });
  },
  adminProducts: (root, args, context) => {
    getAdminId(context);
    const where = args.filter && args.filter.name ? { name_contains: args.filter.name } : {};
    return context.prisma.products({ where });
  },
  adminProduct: (root, args, context) => {
    getAdminId(context);
    return context.prisma.product({ id: args.id });
  },
  levelPrice: (root, args, context) => {
    getAdminId(context);
    return context.prisma.levelPrice({ id: args.id });
  },
  customerProducts: async (root, args, context) => {
    const customerId = getCustomerId(context);

    const where = args.filter && args.filter.name ? { product: { name_contains: args.filter.name } } : {};
    where.level = { id: (await context.prisma.customer({ id: customerId }).level()).id };
    where.isAvailable = true;

    return context.prisma.levelPrices({ where });
  },
  orderItems: (root, args, context) => {
    const customerId = getCustomerId(context);
    return context.prisma.customer({ id: customerId }).orderItems();
  },
  adminOrders: (root, args, context) => {
    getAdminId(context);
    const where = args.filter && args.filter.status ? { status_in: args.filter.status } : {};
    return context.prisma.orders({ where });
  },
  orders: (root, args, context) => {
    const customerId = getCustomerId(context);
    const where = args.filter && args.filter.status ? { status_in: args.filter.status } : {};
    return context.prisma.customer({ id: customerId }).orders({ where });
  },
  adminOrder: async (root, args, context) => {
    getAdminId(context);
    let result = await context.prisma.orders({
      where: {
        orderNumber: args.orderNumber,
        customer: { company: args.company },
      },
    });
    result = result && result[0];
    return result;
  },
  order: async (root, args, context) => {
    const customerId = getCustomerId(context);
    let result = await context.prisma.customer({ id: customerId }).orders({ where: { orderNumber: args.orderNumber } });
    result = result && result[0];
    return result;
  },
  adminNotifications: async (root, args, context) => {
    const id = getAdminId(context);
    return context.prisma.notifications({ where: { toAdmin: { id }, hasBeenRead: false } });
  },
  notifications: async (root, args, context) => {
    const id = getCustomerId(context);
    return context.prisma.notifications({ where: { toCustomer: { id }, hasBeenRead: false } });
  },
  totalSales: async (root, args, context) => {
    getAdminId(context);

    const results = await context.prisma.orders({
      where: {
        status: ORDER_STATUS.COMPLETED,
        updatedAt_gte: new Date(args.startDate).toISOString(),
        updatedAt_lt: new Date(args.endDate).toISOString(),
      },
    });
    return results.reduce((accumulator, currentValue) => accumulator + currentValue.total, 0);
  },
  bestSellerList: async (root, args, context) => {
    getAdminId(context);

    const orders = await context.prisma.orders({
      where: {
        status: ORDER_STATUS.COMPLETED,
        updatedAt_gte: new Date(args.startDate).toISOString(),
        updatedAt_lt: new Date(args.endDate).toISOString(),
      },
    }).$fragment(/* GraphQL */ `
      {
        customer {
          level {
            name
          }
        }
        orderItems
      }
    `);

    const resultsMap = {};
    orders.forEach((order) => {
      order.orderItems.forEach((orderItem) => {
        const productName = orderItem.customerProduct.product.name;
        if (resultsMap[productName] === undefined) resultsMap[productName] = {};

        if (resultsMap[productName].total === undefined) resultsMap[productName].total = 0;
        resultsMap[productName].total += orderItem.quantity;

        const levelName = order.customer.level.name;
        if (resultsMap[productName].levels === undefined) resultsMap[productName].levels = {};
        if (resultsMap[productName].levels[levelName] === undefined) resultsMap[productName].levels[levelName] = 0;

        resultsMap[productName].levels[levelName] += orderItem.quantity;
      });
    });

    const results = [];
    Object.keys(resultsMap).forEach((productName) => {
      const levels = [];

      Object.keys(resultsMap[productName].levels).forEach((levelName) => {
        levels.push({
          levelName,
          total: resultsMap[productName].levels[levelName],
        });
      });
      levels.sort((a, b) => b.total - a.total);

      results.push({
        productName,
        total: resultsMap[productName].total,
        levels,
      });
    });
    results.sort((a, b) => b.total - a.total);

    return results;
  },
};
