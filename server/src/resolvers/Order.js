module.exports = {
  messages: (parent, args, context) => context.prisma.order({ id: parent.id }).messages(),
};
