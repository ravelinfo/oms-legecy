module.exports = {
  customer: (parent, args, context) => context.prisma.order({ id: parent.id }).customer(),
  messages: (parent, args, context) => context.prisma.order({ id: parent.id }).messages(),
};
