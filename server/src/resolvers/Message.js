module.exports = {
  customer: (parent, args, context) => context.prisma.message({ id: parent.id }).customer(),
  admin: (parent, args, context) => context.prisma.message({ id: parent.id }).admin(),
};
