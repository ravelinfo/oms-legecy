const { getAdminId } = require('../utils');

module.exports = {
  newNotification: {
    subscribe: (parent, args, context) => {
      const id = getAdminId(context);
      return context.prisma.$subscribe.notification({ mutation_in: ['CREATED'], node: { toAdmin: { id } } }).node();
    },
    resolve: (payload) => payload,
  },
};
