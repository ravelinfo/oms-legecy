const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const AWS = require('aws-sdk');
const { jwtSecretKey } = require('../config/keys');
const { getAdminId } = require('../utils');
const { getCustomerId } = require('../utils');
const {
  URL,
  EMAIL_NOTIFY_ADDRESS,
  AWS_SES_REGION,
  jwtExpirySeconds,
  ORDER_STATUS,
  ROLE,
} = require('../utils/constants');
const ec = require('../utils/error-code');

AWS.config.update({ region: AWS_SES_REGION });

async function loginAdmin(root, args, context) {
  const admin = await context.prisma.admin({ username: args.username });
  if (!admin) {
    throw new Error(ec.ADM_NOT_EXIST);
  }

  const valid = await bcrypt.compare(args.password, admin.password);
  if (!valid) {
    throw new Error(ec.ADM_WRONG_PASSWORD);
  }

  return {
    token: jwt.sign(
      {
        adminId: admin.id,
        exp: Math.floor(Date.now() / 1000) + jwtExpirySeconds,
      },
      jwtSecretKey
    ),
    admin,
  };
}

async function loginCustomer(root, args, context) {
  const customer = await context.prisma.customer({ username: args.username });
  if (!customer) {
    throw new Error(ec.CTM_NOT_EXIST);
  }

  const valid = await bcrypt.compare(args.password, customer.password);
  if (!valid) {
    throw new Error(ec.CTM_WRONG_PASSWORD);
  }

  return {
    token: jwt.sign(
      {
        customerId: customer.id,
        exp: Math.floor(Date.now() / 1000) + jwtExpirySeconds,
      },
      jwtSecretKey
    ),
    customer,
  };
}

async function createAdmin(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.createAdmin({
      username: args.username,
      password: await bcrypt.hash(args.password, 10),
      email: args.email,
      name: args.name,
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3010:
        throw new Error(ec.ADM_EXIST);
      default:
        throw err;
    }
  }
}

async function updateAdmin(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.updateAdmin({
      data: {
        password: args.password ? await bcrypt.hash(args.password, 10) : undefined,
        email: args.email,
        name: args.name,
      },
      where: {
        id: args.id,
      },
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3039:
        throw new Error(ec.ADM_NOT_EXIST);
      default:
        throw err;
    }
  }
}

async function deleteAdmin(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.deleteAdmin({
      id: args.id,
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3039:
        throw new Error(ec.ADM_NOT_EXIST);
      default:
        throw err;
    }
  }
}

async function createCustomer(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.createCustomer({
      username: args.username,
      password: await bcrypt.hash(args.password, 10),
      email: args.email,
      company: args.company,
      phone: args.phone,
      address: args.address,
      level: { connect: { id: args.levelId } },
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3010:
        throw new Error(ec.CTM_EXIST);
      case 3039:
        throw new Error(ec.LEVEL_NOT_EXIST);
      default:
        throw err;
    }
  }
}

async function updateCustomer(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.updateCustomer({
      where: {
        id: args.id,
      },
      data: {
        username: args.username,
        password: args.password ? await bcrypt.hash(args.password, 10) : undefined,
        email: args.email,
        company: args.company,
        phone: args.phone,
        address: args.address,
        level: args.levelId ? { connect: { id: args.levelId } } : undefined,
      },
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3010:
        throw new Error(ec.CTM_EXIST);
      case 3039:
        if (err.result.errors[0].message.includes('Customer')) {
          throw new Error(ec.CTM_NOT_EXIST);
        } else if (err.result.errors[0].message.includes('Level')) {
          throw new Error(ec.LEVEL_NOT_EXIST);
        } else {
          throw err;
        }
      default:
        throw err;
    }
  }
}

async function deleteCustomer(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.deleteCustomer({
      id: args.id,
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3039:
        throw new Error(ec.CTM_NOT_EXIST);
      default:
        throw err;
    }
  }
}

async function adminUpdateMyProfile(root, args, context) {
  const myId = getAdminId(context);
  return context.prisma.updateAdmin({
    where: {
      id: myId,
    },
    data: {
      email: args.email,
      name: args.name,
      isEmailNotificationEnabled: args.isEmailNotificationEnabled,
    },
  });
}

async function updateMyProfile(root, args, context) {
  const myId = getCustomerId(context);
  return context.prisma.updateCustomer({
    where: {
      id: myId,
    },
    data: {
      email: args.email,
      company: args.company,
      phone: args.phone,
      address: args.address,
      isEmailNotificationEnabled: args.isEmailNotificationEnabled,
    },
  });
}

async function adminUpdateMyPassword(root, args, context) {
  const id = getAdminId(context);

  try {
    if (!(await bcrypt.compare(args.currentPassword, (await context.prisma.admin({ id })).password))) {
      throw new Error(ec.CURRENT_PASSWORD_INCORRECT);
    }
  } catch (e) {
    throw new Error(ec.CURRENT_PASSWORD_INCORRECT);
  }

  return context.prisma.updateAdmin({
    where: {
      id,
    },
    data: {
      password: await bcrypt.hash(args.newPassword, 10),
    },
  });
}

async function updateMyPassword(root, args, context) {
  const id = getCustomerId(context);

  try {
    if (!(await bcrypt.compare(args.currentPassword, (await context.prisma.customer({ id })).password))) {
      throw new Error(ec.CURRENT_PASSWORD_INCORRECT);
    }
  } catch (e) {
    throw new Error(ec.CURRENT_PASSWORD_INCORRECT);
  }

  return context.prisma.updateCustomer({
    where: {
      id,
    },
    data: {
      password: await bcrypt.hash(args.newPassword, 10),
    },
  });
}

async function createLevel(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.createLevel({
      name: args.name,
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3010:
        throw new Error(ec.LEVEL_EXIST);
      default:
        throw err;
    }
  }
}

async function updateLevel(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.updateLevel({
      where: {
        id: args.id,
      },
      data: {
        name: args.name,
      },
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3010:
        throw new Error(ec.LEVEL_EXIST);
      case 3039:
        throw new Error(ec.LEVEL_NOT_EXIST);
      default:
        throw err;
    }
  }
}

async function deleteLevel(root, args, context) {
  getAdminId(context); // Authorization check
  try {
    return await context.prisma.deleteLevel({
      id: args.id,
    });
  } catch (err) {
    switch (err.result.errors[0].code) {
      case 3039:
        throw new Error(ec.LEVEL_NOT_EXIST);
      case 3042:
        throw new Error(ec.LEVEL_IN_USE);
      default:
        throw err;
    }
  }
}

function createProduct(root, args, context) {
  getAdminId(context);
  return context.prisma.createProduct({
    name: args.name,
    description: args.description,
    quantity: args.quantity,
  });
}

function createLevelPrice(root, args, context) {
  getAdminId(context);

  if (args.price < 0) throw new Error(ec.PRICE_LT_0);

  return context.prisma.createLevelPrice({
    level: { connect: { id: args.levelId } },
    price: args.price,
    product: { connect: { id: args.productId } },
    isAvailable: args.isAvailable,
    level_product: `${args.levelId}_${args.productId}`,
  });
}

function updateProduct(root, args, context) {
  getAdminId(context);
  return context.prisma.updateProduct({
    where: {
      id: args.id,
    },
    data: {
      name: args.name,
      description: args.description,
      quantity: args.quantity,
    },
  });
}

function updateLevelPrice(root, args, context) {
  getAdminId(context);

  if (args.price < 0) throw new Error(ec.PRICE_LT_0);

  return context.prisma.updateLevelPrice({
    where: {
      id: args.id,
    },
    data: {
      price: args.price,
      isAvailable: args.isAvailable,
    },
  });
}

function deleteProduct(root, args, context) {
  getAdminId(context);
  return context.prisma.deleteProduct({
    id: args.id,
  });
}

function deleteLevelPrice(root, args, context) {
  getAdminId(context);
  return context.prisma.deleteLevelPrice({
    id: args.id,
  });
}

async function checkIsLevelIdTheSame(context, customerId, levelPriceId, errorCode) {
  try {
    const customerLevelId = (await context.prisma.customer({ id: customerId }).level()).id;
    const InputLevelId = (await context.prisma.levelPrice({ id: levelPriceId }).level()).id;
    if (customerLevelId !== InputLevelId) throw new Error(errorCode);
  } catch (err) {
    throw new Error(errorCode);
  }
}

async function createOrderItem(root, args, context) {
  const id = getCustomerId(context);

  if (args.quantity <= 0) throw new Error(ec.QUANTITY_LTE_0);

  await checkIsLevelIdTheSame(context, id, args.customerProductId, ec.ADD_TO_CART_FAIL);

  return context.prisma.createOrderItem({
    customer: { connect: { id } },
    levelPrice: { connect: { id: args.customerProductId } },
    customer_levelPrice: `${id}_${args.customerProductId}`,
    quantity: args.quantity,
  });
}

function updateOrderItem(root, args, context) {
  getCustomerId(context);

  if (args.quantity <= 0) throw new Error(ec.QUANTITY_LTE_0);

  return context.prisma.updateOrderItem({
    where: {
      id: args.id,
    },
    data: {
      quantity: args.quantity,
    },
  });
}

function deleteOrderItem(root, args, context) {
  getCustomerId(context);
  return context.prisma.deleteOrderItem({
    id: args.id,
  });
}

function sendEmailNotification(paramsObject) {
  const { toAddresses } = paramsObject;
  if (toAddresses.length > 0) {
    const emailBody = {
      Charset: 'UTF-8',
      Data: `To see more details in OMS: ${URL.PRODUCTION}/${paramsObject.to.role.toLowerCase()}/order/${encodeURI(
        paramsObject.company
      )}-${paramsObject.orderNumber}`,
    };
    toAddresses.forEach((currentValue) => {
      new AWS.SES({ apiVersion: '2010-12-01' })
        .sendEmail({
          Destination: { ToAddresses: [currentValue] },
          Message: {
            Body: { Html: emailBody, Text: emailBody },
            Subject: { Charset: 'UTF-8', Data: paramsObject.subject },
          },
          Source: EMAIL_NOTIFY_ADDRESS,
        })
        .promise();
    });
  }
}

async function placeOrder(root, args, context) {
  const id = getCustomerId(context);

  const orderItems = await context.prisma.orderItems({
    where: { customer: { id } },
  }).$fragment(/* GraphQL */ `
    {
      id
      levelPrice {
        id
        price
        product {
          id
          name
          quantity
        }
      }
      quantity
    }
  `);

  if (!orderItems.length > 0) throw new Error(ec.PRODUCT_EQ_0);

  let total = 0;

  const orderItemsForConnect = [];
  const orderItemsOutput = [];

  await Promise.all(
    orderItems.map(async (currentValue) => {
      await checkIsLevelIdTheSame(context, id, currentValue.levelPrice.id, ec.LEVEL_PRICE_ID_NOT_AVAILABLE);

      total += currentValue.levelPrice.price * currentValue.quantity;
      orderItemsForConnect.push({ id: currentValue.id });
      orderItemsOutput.push({
        customerProduct: {
          price: currentValue.levelPrice.price,
          product: {
            id: currentValue.levelPrice.product.id,
            name: currentValue.levelPrice.product.name,
          },
        },
        quantity: currentValue.quantity,
      });
    })
  );

  await Promise.all(
    orderItems.map((currentValue) => {
      const quantity = currentValue.levelPrice.product.quantity - currentValue.quantity;
      return context.prisma.updateProduct({
        where: {
          id: currentValue.levelPrice.product.id,
        },
        data: {
          quantity,
        },
      });
    })
  );

  const orderNumber = (
    await context.prisma.updateCustomer({
      data: {
        lastOrderNumber: (await context.prisma.customer({ id })).lastOrderNumber + 1,
      },
      where: { id },
    })
  ).lastOrderNumber;

  const newOrder = await context.prisma.createOrder({
    orderNumber,
    customer_orderNumber: `${id}_${orderNumber}`,
    customer: { connect: { id } },
    orderItems: orderItemsOutput,
    total,
    status: ORDER_STATUS.PLACED,
  });

  await context.prisma.deleteManyOrderItems({
    customer: { id },
  });

  const toAddresses = [];
  await Promise.all(
    (await context.prisma.admins()).map((currentValue) => {
      if (currentValue.isEmailNotificationEnabled && currentValue.email) toAddresses.push(currentValue.email);
      return context.prisma.createNotification({
        fromCustomer: { connect: { id } },
        toAdmin: { connect: { id: currentValue.id } },
        orderCustomer: { connect: { id } },
        orderNumber: newOrder.orderNumber,
        orderStatusTo: newOrder.status,
      });
    })
  );
  const { company } = await context.prisma.customer({ id });
  sendEmailNotification({
    toAddresses,
    to: {
      role: ROLE.ADMIN,
    },
    company,
    orderNumber,
    subject: `[OMS] There’s a new order from ${company}`,
  });
  return newOrder;
}

async function notifyOtherAdminsAndOrderCustomer(context, fromAdminId, createNotificationData, originalOrderData) {
  const createNotificationDataToAdmin = JSON.parse(JSON.stringify(createNotificationData));

  const toAddressesAdmin = [];
  const notifyArray = (await context.prisma.admins({ where: { id_not: fromAdminId } })).map((admin) => {
    if (admin.isEmailNotificationEnabled && admin.email) toAddressesAdmin.push(admin.email);
    createNotificationDataToAdmin.toAdmin = { connect: { id: admin.id } };
    return context.prisma.createNotification(createNotificationDataToAdmin);
  });

  const { company } = originalOrderData.customer;
  const { orderNumber } = originalOrderData;
  sendEmailNotification({
    toAddresses: toAddressesAdmin,
    to: {
      role: ROLE.ADMIN,
    },
    company,
    orderNumber,
    subject: `[OMS] The order ${company}-${orderNumber} has been updated by ${
      (await context.prisma.admin({ id: fromAdminId })).name
    }`,
  });

  const createNotificationDataToCustomer = JSON.parse(JSON.stringify(createNotificationData));
  createNotificationDataToCustomer.toCustomer = { connect: { id: originalOrderData.customer.id } };
  notifyArray.push(context.prisma.createNotification(createNotificationDataToCustomer));

  if (originalOrderData.customer.isEmailNotificationEnabled && originalOrderData.customer.email) {
    sendEmailNotification({
      toAddresses: [originalOrderData.customer.email],
      to: {
        role: ROLE.CUSTOMER,
      },
      company,
      orderNumber,
      subject: `[OMS] The order ${company}-${orderNumber} has been updated by Admin`,
    });
  }

  return Promise.all(notifyArray);
}

async function checkIsOthersOrder(context, customerId, orderId) {
  try {
    if (customerId !== (await context.prisma.order({ id: orderId }).customer()).id) {
      throw new Error(ec.ORDER_NOT_EXIST);
    }
  } catch (err) {
    throw new Error(ec.ORDER_NOT_EXIST);
  }
}

async function cancelOrderSharedPart(args, context, id, isAdmin) {
  let customerId = null;
  let adminId = null;
  if (isAdmin) {
    adminId = id;
  } else {
    customerId = id;
  }
  let originalOrderData;
  try {
    originalOrderData = await context.prisma.order({ id: args.id }).$fragment(/* GraphQL */ `
      {
        orderNumber
        customer {
          id
          company
          email
          isEmailNotificationEnabled
        }
        status
      }
    `);
    if (originalOrderData.status !== ORDER_STATUS.PLACED && originalOrderData.status !== ORDER_STATUS.IN_PROCESS) {
      throw new Error(ec.ORDER_STATUS_IS_TOO_LATE);
    }
  } catch (err) {
    throw new Error(ec.ORDER_STATUS_IS_TOO_LATE);
  }

  if (customerId) {
    await checkIsOthersOrder(context, customerId, args.id);
  }

  const result = await context.prisma.updateOrder({
    where: {
      id: args.id,
    },
    data: {
      status: ORDER_STATUS.CANCELED,
    },
  });

  const orderResult = await context.prisma.order({ id: result.id });
  const { orderItems } = orderResult;

  await Promise.all(
    orderItems.map(async (orderItem) => {
      const product = await context.prisma.product({ id: orderItem.customerProduct.product.id });
      return context.prisma.updateProduct({
        where: { id: product.id },
        data: { quantity: product.quantity + orderItem.quantity },
      });
    })
  );

  if (customerId) {
    const toAddresses = [];
    await Promise.all(
      (await context.prisma.admins()).map((currentValue) => {
        if (currentValue.isEmailNotificationEnabled && currentValue.email) toAddresses.push(currentValue.email);
        return context.prisma.createNotification({
          fromCustomer: { connect: { id: customerId } },
          toAdmin: { connect: { id: currentValue.id } },
          orderCustomer: { connect: { id: originalOrderData.customer.id } },
          orderNumber: originalOrderData.orderNumber,
          orderStatusFrom: originalOrderData.status,
          orderStatusTo: result.status,
        });
      })
    );

    const { company } = originalOrderData.customer;
    const { orderNumber } = originalOrderData;
    sendEmailNotification({
      toAddresses,
      to: {
        role: ROLE.ADMIN,
      },
      company,
      orderNumber,
      subject: `[OMS] The order ${company}-${orderNumber} has been updated by ${company}`,
    });
  } else {
    const createNotificationData = {
      fromAdmin: { connect: { id: adminId } },
      orderCustomer: { connect: { id: originalOrderData.customer.id } },
      orderNumber: originalOrderData.orderNumber,
      orderStatusFrom: originalOrderData.status,
      orderStatusTo: result.status,
    };

    await notifyOtherAdminsAndOrderCustomer(context, adminId, createNotificationData, originalOrderData);
  }

  return result;
}

async function updateOrder(root, args, context) {
  const id = getAdminId(context);

  if (args.status === ORDER_STATUS.CANCELED) {
    return cancelOrderSharedPart(args, context, id, true);
  }
  if (args.total < 0) throw new Error(ec.TOTAL_LT_0);

  const createNotificationData = {
    fromAdmin: { connect: { id } },
  };
  let hasBeenChanged = false;
  const originalOrderData = await context.prisma.order({ id: args.id }).$fragment(/* GraphQL */ `
    {
      orderNumber
      customer {
        id
        company
        email
        isEmailNotificationEnabled
      }
      total
      status
    }
  `);
  if (originalOrderData.status !== args.status) {
    hasBeenChanged = true;
  }
  if (originalOrderData.total !== args.total) {
    hasBeenChanged = true;
  }
  if (!hasBeenChanged) return null;

  const result = await context.prisma.updateOrder({
    where: {
      id: args.id,
    },
    data: {
      total: args.total,
      status: args.status,
    },
  });

  if (originalOrderData.status !== result.status) {
    createNotificationData.orderStatusFrom = originalOrderData.status;
    createNotificationData.orderStatusTo = result.status;
  }
  if (originalOrderData.total !== result.total) {
    createNotificationData.orderTotalFrom = originalOrderData.total;
    createNotificationData.orderTotalTo = result.total;
  }

  createNotificationData.orderCustomer = { connect: { id: originalOrderData.customer.id } };
  createNotificationData.orderNumber = originalOrderData.orderNumber;

  await notifyOtherAdminsAndOrderCustomer(context, id, createNotificationData, originalOrderData);

  return result;
}

function cancelOrder(root, args, context) {
  const customerId = getCustomerId(context);

  return cancelOrderSharedPart(args, context, customerId);
}

function deleteOrder(root, args, context) {
  getAdminId(context);

  return context.prisma.deleteOrder({ id: args.id });
}

async function adminCreateMessage(root, args, context) {
  const adminId = getAdminId(context);

  const order = await context.prisma.order({ id: args.id }).$fragment(/* GraphQL */ `
    {
      orderNumber
      customer {
        id
        company
        email
        isEmailNotificationEnabled
      }
    }
  `);

  const result = await context.prisma.createMessage({
    order: { connect: { id: args.id } },
    customer: {
      connect: {
        id: order.customer.id,
      },
    },
    admin: { connect: { id: adminId } },
    from: ROLE.ADMIN,
    content: args.content,
  });

  await context.prisma.createNotification({
    fromAdmin: { connect: { id: adminId } },
    toCustomer: { connect: { id: order.customer.id } },
    messageContent: result.content,
    orderNumber: order.orderNumber,
  });

  if (order.customer.isEmailNotificationEnabled && order.customer.email) {
    const { company } = order.customer;
    const { orderNumber } = order;
    sendEmailNotification({
      toAddresses: [order.customer.email],
      to: {
        role: ROLE.CUSTOMER,
      },
      company,
      orderNumber,
      subject: `[OMS] You got a new message in order ${company}-${orderNumber} from Admin`,
    });
  }

  return result;
}

async function createMessage(root, args, context) {
  const customerId = getCustomerId(context);

  await checkIsOthersOrder(context, customerId, args.id);

  const result = await context.prisma.createMessage({
    order: { connect: { id: args.id } },
    customer: { connect: { id: customerId } },
    from: ROLE.CUSTOMER,
    content: args.content,
  });

  const order = await context.prisma.order({ id: args.id });

  const toAddresses = [];
  await Promise.all(
    (await context.prisma.admins()).map((currentValue) => {
      if (currentValue.isEmailNotificationEnabled && currentValue.email) toAddresses.push(currentValue.email);
      return context.prisma.createNotification({
        fromCustomer: { connect: { id: customerId } },
        toAdmin: { connect: { id: currentValue.id } },
        messageContent: result.content,
        orderNumber: order.orderNumber,
      });
    })
  );

  if (toAddresses.length > 0) {
    const { company } = await context.prisma.customer({ id: customerId });
    const { orderNumber } = order;
    sendEmailNotification({
      toAddresses,
      to: {
        role: ROLE.ADMIN,
      },
      company,
      orderNumber,
      subject: `[OMS] You got a new message in order ${company}-${orderNumber} from ${company}`,
    });
  }

  return result;
}

async function adminUpdateNotification(root, args, context) {
  const adminId = getAdminId(context);
  const results = await context.prisma.updateManyNotifications({
    where: {
      id_in: args.ids,
      toAdmin: { id: adminId },
    },
    data: {
      hasBeenRead: true,
    },
  });
  return results;
}

async function updateNotification(root, args, context) {
  const customerId = getCustomerId(context);
  const results = await context.prisma.updateManyNotifications({
    where: {
      id_in: args.ids,
      toCustomer: { id: customerId },
    },
    data: {
      hasBeenRead: true,
    },
  });
  return results;
}

const reprocessPrismaError = (resolver) => async (...args) => {
  function getMessageArray(message) {
    let messageArray = [];
    if (typeof message === 'string') {
      messageArray = message.toUpperCase().split(' ');
    }
    return messageArray;
  }
  try {
    return await resolver(...args);
  } catch (err) {
    if (err && err.result && err.result.errors && err.result.errors.length > 0) {
      const { message } = err.result.errors[0];
      switch (err.result.errors[0].code) {
        case 3010: {
          const messageArray = getMessageArray(message);
          const fieldName = messageArray[messageArray.length - 1];
          throw new Error(`${fieldName}_EXIST`);
        }
        case 3039: {
          const messageArray = getMessageArray(message);
          const modelName = messageArray[5];
          const fieldName = messageArray[10];
          throw new Error(`${modelName}_${fieldName}_NOT_EXIST`);
        }
        case 3042: {
          const messageArray = getMessageArray(message);
          const relationName = messageArray[12].replace(/'/g, '');
          throw new Error(`${relationName}_IN_USE`);
        }
        default:
          throw err;
      }
    } else {
      throw err;
    }
  }
};

const functionExports = {
  loginAdmin,
  loginCustomer,
  createAdmin,
  updateAdmin,
  deleteAdmin,
  createCustomer,
  updateCustomer,
  deleteCustomer,
  adminUpdateMyProfile,
  updateMyProfile,
  adminUpdateMyPassword,
  updateMyPassword,
  createLevel,
  updateLevel,
  deleteLevel,
  createProduct,
  createLevelPrice,
  updateProduct,
  updateLevelPrice,
  deleteProduct,
  deleteLevelPrice,
  createOrderItem,
  updateOrderItem,
  deleteOrderItem,
  placeOrder,
  updateOrder,
  cancelOrder,
  deleteOrder,
  adminCreateMessage,
  createMessage,
  adminUpdateNotification,
  updateNotification,
};

Object.keys(functionExports).forEach((prop) => {
  functionExports[prop] = reprocessPrismaError(functionExports[prop]);
});

module.exports = functionExports;
