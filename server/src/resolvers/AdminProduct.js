module.exports = {
  levelPrices: (parent, args, context) => context.prisma.product({ id: parent.id }).levelPrices(),
};
