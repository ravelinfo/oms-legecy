module.exports = {
  level: (parent, args, context) => context.prisma.levelPrice({ id: parent.id }).level(),
  product: (parent, args, context) => context.prisma.levelPrice({ id: parent.id }).product(),
};
