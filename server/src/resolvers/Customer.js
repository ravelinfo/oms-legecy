module.exports = {
  level: (parent, args, context) => context.prisma.customer({ id: parent.id }).level(),
};
