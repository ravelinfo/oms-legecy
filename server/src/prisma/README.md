# OMS works with Prisma
## Prisma folder structure
* prisma.yml
* datamodel.prisma
* generated/prisma-client
#### prisma.yml
It is a root configuration file for a Prisma service.
The basic config settings is
* endpoint
  * The URL client reaches
* datamodel
  * The filename which defines the data type
* generate
  * Define the client files generator and output destination
## Setup procedure
Everytime after datamodel change, this procedure has to be run.
1. Prerequesites: Prisma CLI
2. `prisma generate` which generate client files
3. Modify the endpoint in generated/prisma-client/index.js into `endpoint: 'http://prisma:4466'` in order to match the docker-compose service name in the project root folder
4. `docker-compose up` in the project root folder to start the prisma service
5. `prisma deploy`
