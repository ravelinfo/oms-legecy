const { GraphQLServer } = require('graphql-yoga');
const GraphQLLong = require('graphql-type-long');
const { prisma } = require('./prisma/generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const AdminOrder = require('./resolvers/AdminOrder');
const AdminProduct = require('./resolvers/AdminProduct');
const Customer = require('./resolvers/Customer');
const CustomerProduct = require('./resolvers/CustomerProduct');
const LevelPrice = require('./resolvers/LevelPrice');
const Message = require('./resolvers/Message');
const Notification = require('./resolvers/Notification');
const Order = require('./resolvers/Order');
const OrderItem = require('./resolvers/OrderItem');
const Subscription = require('./resolvers/Subscription');
const utils = require('./utils');

utils.createDefaultAdminAccount(prisma);

const resolvers = {
  Query,
  Mutation,
  AdminOrder,
  AdminProduct,
  Customer,
  CustomerProduct,
  GraphQLLong,
  LevelPrice,
  Message,
  Order,
  Notification,
  OrderItem,
  Subscription,
};

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: (request) => ({ ...request, prisma }),
});

server.start({ port: 5000, endpoint: '/api', playground: '/playground' });
