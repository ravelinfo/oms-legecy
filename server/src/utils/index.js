const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { jwtSecretKey } = require('../config/keys');
const ec = require('../utils/error-code');

const account = require('./../config/account');

async function createDefaultAdminAccount(prisma) {
  const admins = await prisma.admins();
  if (admins.length === 0) {
    const password = await bcrypt.hash(account.defaultAdminPassword, 10);
    return prisma.createAdmin({
      username: account.defaultAdminUsername,
      password,
      name: account.defaultAdminName,
    });
  }
  return null;
}

function getAdminId(context) {
  let auth = '';
  if (context.request) {
    auth = context.request.get('Authorization');
  } else if (context.connection) {
    auth = context.connection.context.Authorization;
  }
  if (auth) {
    const token = auth.replace('Bearer ', '');
    try {
      const { adminId } = jwt.verify(token, jwtSecretKey);
      if (!adminId) {
        throw new Error(ec.ADM_INVALID);
      }
      return adminId;
    } catch (err) {
      if (err.name === 'TokenExpiredError') {
        throw new Error(ec.ADM_TOKEN_EXPIRED);
      }
      throw err; // Unexpected error
    }
  }
  throw new Error(ec.ADM_NOT_AUTHENTICATED);
}

function getCustomerId(context) {
  const auth = context.request.get('Authorization');
  if (auth) {
    const token = auth.replace('Bearer ', '');
    try {
      const { customerId } = jwt.verify(token, jwtSecretKey);
      if (!customerId) {
        throw new Error(ec.CTM_INVALID);
      }
      return customerId;
    } catch (err) {
      if (err.name === 'TokenExpiredError') {
        throw new Error(ec.CTM_TOKEN_EXPIRED);
      }
      throw err; // Unexpected error
    }
  }
  throw new Error(ec.CTM_NOT_AUTHENTICATED);
}

module.exports = {
  createDefaultAdminAccount,
  getAdminId,
  getCustomerId,
};
