module.exports = {
  URL: {
    PRODUCTION: 'https://ptstar-oms.ravelinfo.com',
  },
  EMAIL_NOTIFY_ADDRESS: 'ptstar@ravelinfo.com',
  AWS_SES_REGION: 'ap-southeast-2',
  jwtExpirySeconds: 60 * 60 * 24,
  ORDER_STATUS: {
    PLACED: 'PLACED',
    IN_PROCESS: 'IN_PROCESS',
    SHIPPED: 'SHIPPED',
    COMPLETED: 'COMPLETED',
    CANCELED: 'CANCELED',
  },
  ROLE: {
    ADMIN: 'ADMIN',
    CUSTOMER: 'CUSTOMER',
  },
};
