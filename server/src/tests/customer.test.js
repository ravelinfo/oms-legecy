const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of Admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
        admin {
          name
        }
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client[result.loginAdmin.admin.name] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

const id = {};
orderedTest('createLevel level-1', async () => {
  let result;
  result = await client.Admin.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-1") {
        id
        name
      }
    }
  `);
  id[result.createLevel.name] = result.createLevel.id;

  result = await client.Admin.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-2") {
        id
        name
      }
    }
  `);
  id[result.createLevel.name] = result.createLevel.id;

  result = await client.Admin.request(/* GraphQL */ `
    mutation {
      createCustomer(
        username: "customer-level-1"
        password: "1"
        email: "customer-level-1@gmail.com"
        company: "Customer-level-1"
        phone: "+886 2 8101-8800"
        address: "No. 7, Sec. 5, Xinyi Rd., Xinyi Dist., Taipei City 110, Taiwan (R.O.C.)"
        levelId: "${id['level-1']}"
      ) {
        id
        username
        email
        company
        phone
        address
        level {
          name
        }
      }
    }
  `);
  id[result.createCustomer.company] = result.createCustomer.id;
  delete result.createCustomer.id;
  expect(result).toMatchSnapshot();
});

orderedTest('updateCustomer Customer-1 -> Customer-level-2', async () => {
  const result = await client.Admin.request(/* GraphQL */ `
    mutation {
      updateCustomer(
        id: "${id['Customer-level-1']}"
        username: "customer-level-2"
        password: "2"
        email: "customer-level-2@gmail.com"
        company: "Customer-level-2"
        phone: "886-2-6610-3600"
        address: "No. 221, Sec. 2, Zhishan Rd., Shilin Dist., Taipei City 111, Taiwan (R.O.C.)"
        levelId: "${id['level-2']}"
      ) {
        username
        email
        company
        phone
        address
        level {
          name
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
