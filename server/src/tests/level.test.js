const { GraphQLClient } = require('graphql-request');
const { prisma } = require('./../prisma/generated/prisma-client');

const utils = require('./../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

let adminToken;
let adminClient;
orderedTest('Get token of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  adminClient = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

let id;
orderedTest('Create level-9', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-9") {
        id
        name
      }
    }
  `);
  id = result.createLevel.id;
  delete result.createLevel.id;
  expect(result).toMatchSnapshot();
});

orderedTest('Edit level-9 -> level-999', async () => {
  const result = await adminClient.request(/* GraphQL */ `
        mutation {
            updateLevel(id: "${id}", name: "level-999") {
                name
            }
        }
    `);
  expect(result).toMatchSnapshot();
});

orderedTest('Query level-999', async () => {
  const result = await adminClient.request(/* GraphQL */ `
        query {
            level(id: "${id}") {
                name
            }
        }
    `);
  expect(result).toMatchSnapshot();
});

orderedTest('Delete level-999', async () => {
  const result = await adminClient.request(/* GraphQL */ `
        mutation {
            deleteLevel(id: "${id}") {
                name
            }
        }
    `);
  expect(result).toMatchSnapshot();
});

orderedTest('Query levels after creating level-1, level-10, level-100', async () => {
  await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-1") {
        name
      }
    }
  `);
  await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-10") {
        name
      }
    }
  `);
  await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-100") {
        name
      }
    }
  `);

  const result = await adminClient.request(/* GraphQL */ `
    query {
      levels {
        name
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
