const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client.adminDefault = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

const id = {};
orderedTest(
  `createLevel 1
        createLevel 10
        createProduct 1
        createProduct 2
        createLevelPrice 1`,
  async () => {
    let result;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-1") {
          id
        }
      }
    `);
    id.level1 = result.createLevel.id;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-10") {
          id
        }
      }
    `);
    id.level10 = result.createLevel.id;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-1", description: "description-1", quantity: 999) {
          id
        }
      }
    `);
    id.product1 = result.createProduct.id;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-2", description: "description-2", quantity: 999) {
          id
        }
      }
    `);
    id.product2 = result.createProduct.id;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id.product1}", levelId: "${id.level1}", price: 1) {
          id
          level {
            name
          }
          price
          isAvailable
          product {
            name
            description
            quantity
          }
        }
      }
    `);
    id.levelPrice1 = result.createLevelPrice.id;
    delete result.createLevelPrice.id;
    expect(result).toMatchSnapshot();
  }
);
orderedTest(`updateLevelPrice 1 -> 9`, async () => {
  const result = await client.adminDefault.request(/* GraphQL */ `
    mutation {
      updateLevelPrice(id: "${id.levelPrice1}", price: 9) {
        level {
          name
        }
        price
        isAvailable
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
orderedTest(`updateLevelPrice 9 -> -1 -> PRICE_LT_0`, async () => {
  try {
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        updateLevelPrice(id: "${id.levelPrice1}", price: -1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});
orderedTest(`createLevelPrice -1 -> PRICE_LT_0`, async () => {
  try {
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id.product1}", levelId: "${id.level10}", price: -1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});
orderedTest('Get client of Customer-level-1', async () => {
  let result;
  result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createCustomer(
          username: "customer-level-1"
          password: "1"
          email: "customer-level-1@gmail.com"
          company: "Customer-level-1"
          levelId: "${id.level1}"
        ) {
          id
          username
        }
      }
    `);
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "${result.createCustomer.username}", password: "1") {
          token
          customer {
            company
          }
        }
      }
    `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});
orderedTest(`createLevelPrice 2 isAvailable false`, async () => {
  const result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id.product2}", levelId: "${id.level1}", price: 2, isAvailable: false) {
          id
          level {
            name
          }
          price
          isAvailable
          product {
            name
            description
            quantity
          }
        }
      }
    `);
  id.levelPrice2 = result.createLevelPrice.id;
  delete result.createLevelPrice.id;
  expect(result).toMatchSnapshot();
});
orderedTest(`Customer-level-1 customerProducts`, async () => {
  const result = await client['Customer-level-1'].request(/* GraphQL */ `
    query {
      customerProducts {
        price
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
orderedTest(`updateLevelPrice 2 no any parameters`, async () => {
  const result = await client.adminDefault.request(/* GraphQL */ `
    mutation {
      updateLevelPrice(id: "${id.levelPrice2}") {
        level {
          name
        }
        price
        isAvailable
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
orderedTest(`Customer-level-1 customerProducts`, async () => {
  const result = await client['Customer-level-1'].request(/* GraphQL */ `
    query {
      customerProducts {
        price
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
orderedTest(`updateLevelPrice 2 isAvailable true`, async () => {
  const result = await client.adminDefault.request(/* GraphQL */ `
    mutation {
      updateLevelPrice(id: "${id.levelPrice2}", isAvailable: true) {
        level {
          name
        }
        price
        isAvailable
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
orderedTest(`Customer-level-1 customerProducts`, async () => {
  const result = await client['Customer-level-1'].request(/* GraphQL */ `
    query {
      customerProducts {
        price
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
