const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

const isEmailNotificationEnabled = true;

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of Admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
        admin {
          name
        }
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client[result.loginAdmin.admin.name] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

orderedTest(
  `createAdmin Admin-Ptstar
        Get client of Admin-2`,
  async () => {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        createAdmin(username: "admin-Ptstar", password: "1", email: "ptstar@ravelinfo.com", name: "Admin-Ptstar") {
          id
        }
      }
    `);

    await client.Admin.request(/* GraphQL */ `
      mutation {
        createAdmin(username: "admin-2", password: "1", email: "admin-2@gmail.com", name: "Admin-2") {
          id
          username
        }
      }
    `);
    const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginAdmin(username: "admin-2", password: "1") {
          token
          admin {
            name
          }
        }
      }
    `);
    const adminToken = result.loginAdmin.token;
    expect(adminToken).toBeTruthy();
    client[result.loginAdmin.admin.name] = new GraphQLClient(API_URL, {
      headers: {
        Authorization: `Bearer ${adminToken}`,
      },
    });
  }
);

const id = {};
orderedTest(
  `Admin-2 createProduct name product-1
        createProduct name product-2
        createLevel name level-1
        createLevel name level-10
        createLevel name level-100
        createLevelPrice price 1 with product-1, level-1
        createLevelPrice price 11 with product-1, level-10
        createLevelPrice price 101 with product-1, level-100
        createLevelPrice price 2 with product-2, level-1
        createLevelPrice price 12 with product-2, level-10
        createLevelPrice price 102 with product-2, level-100
        adminProducts`,
  async () => {
    let result;
    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-1", description: "description-1", quantity: 1000) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-2", description: "description-2", quantity: 1000) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-1") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-10") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-100") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-1']}", price: 1) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-10']}", price: 11) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-100']}", price: 101) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-1']}", price: 2) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-10']}", price: 12) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-100']}", price: 102) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client['Admin-2'].request(/* GraphQL */ `
      query {
        adminProducts {
          name
          description
          quantity
          levelPrices {
            level {
              name
            }
            price
          }
        }
      }
    `);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('Get client of Gene level 1', async () => {
  let result;
  result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createCustomer(
          username: "gene-level-1"
          password: "1"
          email: "gene.lai@ravelinfo.com"
          company: "Gene level 1"
          levelId: "${id['level-1']}"
        ) {
          id
          username
        }
      }
    `);
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "${result.createCustomer.username}", password: "1") {
          token
          customer {
            company
          }
        }
      }
    `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});
orderedTest('Get client of Customer-level-10', async () => {
  let result;
  result = await client['Admin-2'].request(/* GraphQL */ `
      mutation {
        createCustomer(
          username: "customer-level-10"
          password: "1"
          email: "customer-level-10@gmail.com"
          company: "Customer-level-10"
          levelId: "${id['level-10']}"
        ) {
          id
          username
        }
      }
    `);
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "${result.createCustomer.username}", password: "1") {
          token
          customer {
            company
          }
        }
      }
    `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest(`Admin-2 adminUpdateMyProfile isEmailNotificationEnabled ${isEmailNotificationEnabled}`, async () => {
  const result = await client['Admin-2'].request(/* GraphQL */ `
    mutation {
      adminUpdateMyProfile(isEmailNotificationEnabled: ${isEmailNotificationEnabled}) {
        isEmailNotificationEnabled
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(`Gene level 1 updateMyProfile isEmailNotificationEnabled ${isEmailNotificationEnabled}`, async () => {
  const result = await client['Gene level 1'].request(/* GraphQL */ `
    mutation {
      updateMyProfile(isEmailNotificationEnabled: ${isEmailNotificationEnabled}) {
        isEmailNotificationEnabled
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(
  `Gene level 1 createOrderItem createLevelPrice price 1 * 100
          createOrderItem createLevelPrice price 2 * 200
          placeOrder`,
  async () => {
    await client['Gene level 1'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-1`]}", quantity: 100) {
          id
        }
      }
    `);
    await client['Gene level 1'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-2`]}", quantity: 200) {
          id
        }
      }
    `);
    const result = await client['Gene level 1'].request(/* GraphQL */ `
      mutation {
        placeOrder {
          id
        }
      }
    `);

    expect(result.placeOrder.id).toBeTruthy();
    id.order1Id = result.placeOrder.id;
  }
);

orderedTest(`Admin-2 updateOrder`, async () => {
  const result = await client['Admin-2'].request(/* GraphQL */ `
    mutation {
      updateOrder(id: "${id.order1Id}", total: 999, status: "IN_PROCESS") {
        id
      }
    }
  `);

  expect(result.updateOrder.id).toBeTruthy();
});

orderedTest(`Gene level 1 cancelOrder`, async () => {
  const result = await client['Gene level 1'].request(/* GraphQL */ `
    mutation {
      cancelOrder(id: "${id.order1Id}") {
        id
      }
    }
  `);

  expect(result.cancelOrder.id).toBeTruthy();
});

orderedTest(`Admin-2 adminCreateMessage`, async () => {
  const result = await client['Admin-2'].request(/* GraphQL */ `
    mutation {
      adminCreateMessage(id: "${id.order1Id}", content: "message content written by Admin-2") {
        id
      }
    }
  `);

  expect(result.adminCreateMessage.id).toBeTruthy();
});

orderedTest(`Gene level 1 createMessage`, async () => {
  const result = await client['Gene level 1'].request(/* GraphQL */ `
    mutation {
      createMessage(id: "${id.order1Id}", content: "message content written by Gene") {
        id
      }
    }
  `);

  expect(result.createMessage.id).toBeTruthy();
});
