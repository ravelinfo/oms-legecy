const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of Admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
        admin {
          name
        }
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client[result.loginAdmin.admin.name] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

const id = {};
orderedTest(
  `3010 createProduct
        createProduct`,
  async () => {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-1", description: "description-1", quantity: 1) {
          id
        }
      }
    `);
    try {
      await client.Admin.request(/* GraphQL */ `
        mutation {
          createProduct(name: "product-1", description: "description-1", quantity: 1) {
            id
          }
        }
      `);
    } catch (e) {
      expect(e.response).toMatchSnapshot();
    }
  }
);

orderedTest(
  `3010 createProduct 
        updateProduct`,
  async () => {
    let result;
    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-2", description: "description-2", quantity: 2) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;
    try {
      result = await client.Admin.request(/* GraphQL */ `
      mutation {
        updateProduct(id: "${id['product-2']}", name: "product-1") {
          id
        }
      }
    `);
    } catch (e) {
      expect(e.response).toMatchSnapshot();
    }
  }
);

orderedTest(
  `3010 createLevel
        createLevelPrice
        createLevelPrice`,
  async () => {
    let result;
    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-1") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-1']}", price: 1) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    try {
      result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-1']}", price: 1) {
          id
        }
      }
    `);
    } catch (e) {
      expect(e.response).toMatchSnapshot();
    }
  }
);

orderedTest('Get client of Customer-level-1', async () => {
  let result;
  result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createCustomer(
          username: "customer-level-1"
          password: "1"
          email: "customer-level-1@gmail.com"
          company: "Customer-level-1"
          levelId: "${id['level-1']}"
        ) {
          id
          username
          company
        }
      }
    `);
  id[result.createCustomer.company] = result.createCustomer.id;
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "${result.createCustomer.username}", password: "1") {
          token
          customer {
            company
          }
        }
      }
    `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest(
  `3010 createOrderItem
        createOrderItem`,
  async () => {
    await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id['levelPrice-1']}", quantity: 1) {
          id
        }
      }
    `);
    try {
      await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id['levelPrice-1']}", quantity: 1) {
          id
        }
      }
    `);
    } catch (e) {
      expect(e.response).toMatchSnapshot();
    }
  }
);

orderedTest(`3039 createLevelPrice productId 123 levelId 321`, async () => {
  try {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "123", levelId: "321", price: 1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 createLevelPrice productId 123`, async () => {
  try {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "123", levelId: "${id['level-1']}", price: 1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 updateProduct`, async () => {
  try {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        updateProduct(id: "123") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 deleteProduct`, async () => {
  try {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        deleteProduct(id: "123") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 updateLevelPrice`, async () => {
  try {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        updateLevelPrice(id: "123") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 deleteLevelPrice`, async () => {
  try {
    await client.Admin.request(/* GraphQL */ `
      mutation {
        deleteLevelPrice(id: "123") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 updateOrderItem`, async () => {
  try {
    await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        updateOrderItem(id: "123", quantity: 1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`3039 deleteOrderItem`, async () => {
  try {
    await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        deleteOrderItem(id: "123") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(
  `3042 placeOrder
        createMessage
        deleteCustomer`,
  async () => {
    const result = await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        placeOrder {
          id
        }
      }
    `);
    id[`order-1`] = result.placeOrder.id;
    await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        createMessage(id: "${id[`order-1`]}", content: "message content") {
          id
        }
      }
    `);
    try {
      await client.Admin.request(/* GraphQL */ `
      mutation {
        deleteCustomer(id: "${id['Customer-level-1']}") {
          id
        }
      }
    `);
    } catch (e) {
      expect(e.response).toMatchSnapshot();
    }
  }
);
