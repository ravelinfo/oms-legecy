let testIndex = 0;

module.exports = {
  API_URL: 'http://localhost:5000/api',
  async initialDbData(prisma, utils) {
    await prisma.deleteManyNotifications();
    await prisma.deleteManyMessages();

    await prisma.deleteManyOrderItems();

    await prisma.deleteManyOrders();

    await prisma.deleteManyCustomers();
    await prisma.deleteManyProducts();

    await prisma.deleteManyLevels();

    await prisma.deleteManyAdmins();

    await utils.createDefaultAdminAccount(prisma);
  },
  orderedTest(description, cb) {
    testIndex += 1;
    return test(`${testIndex}. ${description}`, cb);
  },
};
