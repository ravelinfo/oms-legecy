const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client.adminDefault = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

orderedTest(`createAdmin admin-1 with password 11`, async () => {
  await client.adminDefault.request(/* GraphQL */ `
    mutation {
      createAdmin(username: "admin-1", password: "11", email: "admin-1@gmail.com", name: "Admin-1") {
        id
      }
    }
  `);

  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin-1", password: "11") {
        token
      }
    }
  `);
  const { token } = result.loginAdmin;
  expect(token).toBeTruthy();
  client.admin1 = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest(`adminUpdateMyPassword password 2 -> 1 => ADM_WRONG_PASSWORD`, async () => {
  try {
    await client.admin1.request(/* GraphQL */ `
      mutation {
        adminUpdateMyPassword(currentPassword: "2", newPassword: "1") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`adminUpdateMyPassword password 11 -> 1`, async () => {
  const result = await client.admin1.request(/* GraphQL */ `
    mutation {
      adminUpdateMyPassword(currentPassword: "11", newPassword: "1") {
        username
        email
        name
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

const id = {};
orderedTest(
  `createLevel-1
        createCustomer password 11`,
  async () => {
    let result;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "1") {
          id
        }
      }
    `);
    id.level1 = result.createLevel.id;

    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createCustomer(username: "customer-level-1", password: "11", email: "customer-level-1@gmail.com", company: "Customer-level-1", levelId: "${id.level1}") {
          id
        }
      }
    `);

    result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "customer-level-1", password: "11") {
          token
        }
      }
    `);
    const { token } = result.loginCustomer;
    expect(token).toBeTruthy();
    client.customerLevel1 = new GraphQLClient(API_URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
);

orderedTest(`updateMyPassword password 2 -> 1 => CTM_WRONG_PASSWORD`, async () => {
  try {
    await client.customerLevel1.request(/* GraphQL */ `
      mutation {
        updateMyPassword(currentPassword: "2", newPassword: "1") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`updateMyPassword password 11 -> 1`, async () => {
  const result = await client.customerLevel1.request(/* GraphQL */ `
    mutation {
      updateMyPassword(currentPassword: "11", newPassword: "1") {
        username
        email
        company
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
