const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const { token } = result.loginAdmin;
  expect(token).toBeTruthy();
  client.adminDefault = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

const id = {};
orderedTest(
  `createProduct name product-1
        createProduct name product-2
        createProduct name product-11
        createProduct name product-111
        createLevel name level-1
        createLevel name level-10
        createLevel name level-100
        createLevelPrice 1 with product-1, level-1
        createLevelPrice 11 with product-1, level-10
        createLevelPrice 101 with product-1, level-100
        createLevelPrice 2 with product-2, level-1
        createLevelPrice 22 with product-2-2, level-1
        createLevelPrice 12 with product-2, level-10
        createLevelPrice 102 with product-2, level-100
        adminProducts`,
  async () => {
    let result;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-1", description: "description-1", quantity: 1) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-2", description: "description-2", quantity: 2) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-2-2", description: "description-2-2", quantity: 2) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-11", description: "description-11", quantity: 11) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-111", description: "description-111", quantity: 111) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-1") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-10") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-100") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-1']}", price: 1) {
          id
        }
      }
    `);
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-10']}", price: 11) {
          id
        }
      }
    `);
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-100']}", price: 101) {
          id
        }
      }
    `);
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-1']}", price: 2) {
          id
        }
      }
    `);
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2-2']}", levelId: "${id['level-1']}", price: 22) {
          id
        }
      }
    `);
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-10']}", price: 12) {
          id
        }
      }
    `);
    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-100']}", price: 102) {
          id
        }
      }
    `);

    result = await client.adminDefault.request(/* GraphQL */ `
      query {
        adminProducts {
          name
          description
          quantity
          levelPrices {
            level {
              name
            }
            price
          }
        }
      }
    `);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('Get client of Customer-level-1', async () => {
  let result;
  result = await client.adminDefault.request(/* GraphQL */ `
    mutation {
      createCustomer(
        username: "customer-level-1"
        password: "1"
        email: "customer-level-1@gmail.com"
        company: "Customer-level-1"
        levelId: "${id['level-1']}"
      ) {
        id
        username
      }
    }
  `);
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginCustomer(username: "${result.createCustomer.username}", password: "1") {
        token
        customer {
          company
        }
      }
    }
  `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest(`adminProducts with filter.name 1`, async () => {
  const result = await client.adminDefault.request(/* GraphQL */ `
    query {
      adminProducts(filter: { name: "1" }) {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(`Customer-level-1 customerProducts`, async () => {
  const result = await client['Customer-level-1'].request(/* GraphQL */ `
    query {
      customerProducts {
        price
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(`Customer-level-1 customerProducts with filter.name 2`, async () => {
  const result = await client['Customer-level-1'].request(/* GraphQL */ `
    query {
      customerProducts(filter: { name: "2" }) {
        price
        product {
          name
          description
          quantity
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
