const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

let adminClient;
orderedTest('Get token of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  adminClient = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

let product1Id;
let product2Id;
orderedTest('get productIds after creating product-1, product-2', async () => {
  let result;
  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createProduct(name: "Product-1", description: "description-1", quantity: 1) {
        id
      }
    }
  `);
  product1Id = result.createProduct.id;

  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createProduct(name: "Product-2", description: "description-2", quantity: 2) {
        id
      }
    }
  `);
  product2Id = result.createProduct.id;

  result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

let level1Id;
let level10Id;
orderedTest('get levelIds after creating level-1, level-10', async () => {
  let result;
  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-1") {
        id
      }
    }
  `);
  level1Id = result.createLevel.id;

  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-10") {
        id
      }
    }
  `);
  level10Id = result.createLevel.id;

  result = await adminClient.request(/* GraphQL */ `
    query {
      levels {
        name
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest('create customer-level-1', async () => {
  let result;
  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createCustomer(username: "customer-level-1", password: "1", email: "customer-level-1@gmail.com", company: "Customer-level-1", levelId: "${level1Id}") {
        id
      }
    }
  `);

  result = await adminClient.request(/* GraphQL */ `
    query {
      customers {
        username
        email
        company
        level {
          name
        }
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

let level1Client;
orderedTest('Get token of customer-level-1', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginCustomer(username: "customer-level-1", password: "1") {
        token
      }
    }
  `);
  const level1ClientToken = result.loginCustomer.token;
  expect(level1ClientToken).toBeTruthy();
  level1Client = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${level1ClientToken}`,
    },
  });
});

let levelPrice1Id;
let levelPrice11Id;
let levelPrice2Id;
orderedTest(
  `query adminProducts after
        creating levelPrice 1 for product-1 and level-1
        creating levelPrice 11 for product-1 and level-10
        creating levelPrice 2 for product-2 and level-1
        creating levelPrice 22 for product-2 and level-20`,
  async () => {
    let result;
    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product1Id}", levelId: "${level1Id}", price: 1) {
          id
        }
      }
    `);
    levelPrice1Id = result.createLevelPrice.id;

    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product1Id}", levelId: "${level10Id}", price: 11) {
          id
        }
      }
    `);
    levelPrice11Id = result.createLevelPrice.id;

    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product2Id}", levelId: "${level1Id}", price: 2) {
          id
        }
      }
    `);
    levelPrice2Id = result.createLevelPrice.id;

    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product2Id}", levelId: "${level10Id}", price: 22) {
          id
        }
      }
    `);

    result = await adminClient.request(/* GraphQL */ `
      query {
        adminProducts {
          name
          description
          quantity
          levelPrices {
            level {
              name
            }
            price
          }
        }
      }
    `);

    expect(result).toMatchSnapshot();
  }
);

let id;
orderedTest('create OrderItem with Quantity 9 (levelPrice 1)', async () => {
  const result = await level1Client.request(/* GraphQL */ `
    mutation {
      createOrderItem(customerProductId: "${levelPrice1Id}", quantity: 9) {
        id
        customerProduct {
          price
          product {
            name
            description
            quantity
          }
        }
        quantity
      }
    }
  `);
  id = result.createOrderItem.id;
  delete result.createOrderItem.id;

  expect(result).toMatchSnapshot();
});

orderedTest('update OrderItem Quantity 9 -> 999', async () => {
  const result = await level1Client.request(/* GraphQL */ `
    mutation {
      updateOrderItem(id: "${id}", quantity: 999) {
        customerProduct {
          price
          product {
            name
            description
            quantity
          }
        }
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('delete OrderItem (levelPrice 1)', async () => {
  const result = await level1Client.request(/* GraphQL */ `
    mutation {
      deleteOrderItem(id: "${id}") {
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

// Custom Error
orderedTest('create OrderItem with Quantity 0 -> QUANTITY_LTE_0', async () => {
  try {
    await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice1Id}", quantity: 0) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});
orderedTest('create OrderItem with Quantity -1 -> QUANTITY_LTE_0', async () => {
  try {
    await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice1Id}", quantity: -1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});
orderedTest('create another level OrderItem -> ADD_TO_CART_FAIL', async () => {
  try {
    await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice11Id}", quantity: 11) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

// Success
orderedTest(
  `query orderItems after 
        creating OrderItem with Quantity 1 (levelPrice 1)
        creating OrderItem with Quantity 2 (levelPrice 2)`,
  async () => {
    let result = await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice1Id}", quantity: 1) {
          id
        }
      }
    `);
    result = await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice2Id}", quantity: 2) {
          id
        }
      }
    `);
    result = await level1Client.request(/* GraphQL */ `
      query {
        orderItems {
          customerProduct {
            price
            product {
              name
              description
              quantity
            }
          }
          quantity
        }
      }
    `);

    expect(result).toMatchSnapshot();
  }
);
