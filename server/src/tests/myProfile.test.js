const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client.adminDefault = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

orderedTest(`createAdmin admin-1`, async () => {
  await client.adminDefault.request(/* GraphQL */ `
    mutation {
      createAdmin(username: "admin-1", password: "1", email: "admin-1@gmail.com", name: "Admin-1") {
        id
      }
    }
  `);

  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin-1", password: "1") {
        token
      }
    }
  `);
  const { token } = result.loginAdmin;
  expect(token).toBeTruthy();
  client.admin1 = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest(`adminMe`, async () => {
  const result = await client.admin1.request(/* GraphQL */ `
    query {
      adminMe {
        username
        email
        name
        isEmailNotificationEnabled
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(`adminUpdateMyProfile`, async () => {
  const result = await client.admin1.request(/* GraphQL */ `
    mutation {
      adminUpdateMyProfile(
        email: "admin-1-update@gmail.com"
        name: "Admin-1-update"
        isEmailNotificationEnabled: false
      ) {
        username
        email
        name
        isEmailNotificationEnabled
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

const id = {};
orderedTest(
  `createLevel-1
        createCustomer customer-level-1`,
  async () => {
    let result;
    result = await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createLevel(name: "1") {
          id
        }
      }
    `);
    id.level1 = result.createLevel.id;

    await client.adminDefault.request(/* GraphQL */ `
      mutation {
        createCustomer(username: "customer-level-1", password: "1", email: "customer-level-1@gmail.com", company: "Customer-level-1", levelId: "${id.level1}") {
          id
        }
      }
    `);

    result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "customer-level-1", password: "1") {
          token
        }
      }
    `);
    const { token } = result.loginCustomer;
    expect(token).toBeTruthy();
    client.customerLevel1 = new GraphQLClient(API_URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
);

orderedTest(`customerMe`, async () => {
  const result = await client.customerLevel1.request(/* GraphQL */ `
    query {
      customerMe {
        username
        email
        company
        isEmailNotificationEnabled
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(`updateMyProfile`, async () => {
  const result = await client.customerLevel1.request(/* GraphQL */ `
    mutation {
      updateMyProfile(
        email: "customer-level-1-update@gmail.com"
        company: "Customer-level-1-update"
        phone: "02-2343-1100"
        address: "No. 21, Zhongshan S. Rd., Zhongzheng Dist., Taipei City 100, Taiwan (R.O.C.)"
        isEmailNotificationEnabled: false
      ) {
        username
        email
        company
        phone
        address
        isEmailNotificationEnabled
      }
    }
  `);
  expect(result).toMatchSnapshot();
});
