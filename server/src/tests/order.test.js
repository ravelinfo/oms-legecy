const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

let adminClient;
orderedTest('Get client of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  adminClient = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

let product1Id;
let product2Id;
orderedTest('get productIds after creating product-1, product-2', async () => {
  let result;
  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createProduct(name: "Product-1", description: "description-1", quantity: 991) {
        id
      }
    }
  `);
  product1Id = result.createProduct.id;

  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createProduct(name: "Product-2", description: "description-2", quantity: 992) {
        id
      }
    }
  `);
  product2Id = result.createProduct.id;

  result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

let level1Id;
let level10Id;
orderedTest('get levelIds after creating level-1, level-10', async () => {
  let result;
  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-1") {
        id
      }
    }
  `);
  level1Id = result.createLevel.id;

  result = await adminClient.request(/* GraphQL */ `
    mutation {
      createLevel(name: "level-10") {
        id
      }
    }
  `);
  level10Id = result.createLevel.id;

  result = await adminClient.request(/* GraphQL */ `
    query {
      levels {
        name
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

let level1Client;
let level1BClient;
orderedTest('Get client of customer-level-1', async () => {
  await adminClient.request(/* GraphQL */ `
    mutation {
      createCustomer(username: "customer-level-1", password: "1", email: "customer-level-1@gmail.com", company: "Customer-level-1", levelId: "${level1Id}") {
        id
      }
    }
  `);

  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginCustomer(username: "customer-level-1", password: "1") {
        token
      }
    }
  `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  level1Client = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest('Get client of customer-level-1-B', async () => {
  await adminClient.request(/* GraphQL */ `
    mutation {
      createCustomer(username: "customer-level-1-B", password: "1", email: "customer-level-1-B@gmail.com", company: "Customer-level-1-B", levelId: "${level1Id}") {
        id
      }
    }
  `);

  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginCustomer(username: "customer-level-1-B", password: "1") {
        token
      }
    }
  `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  level1BClient = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

let levelPrice1Id;
let levelPrice2Id;
orderedTest(
  `query adminProducts after
        creating levelPrice 1 for product-1 and level-1
        creating levelPrice 11 for product-1 and level-10
        creating levelPrice 2 for product-2 and level-1
        creating levelPrice 22 for product-2 and level-20`,
  async () => {
    let result;
    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product1Id}", levelId: "${level1Id}", price: 1) {
          id
        }
      }
    `);
    levelPrice1Id = result.createLevelPrice.id;

    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product1Id}", levelId: "${level10Id}", price: 11) {
          id
        }
      }
    `);

    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product2Id}", levelId: "${level1Id}", price: 2) {
          id
        }
      }
    `);
    levelPrice2Id = result.createLevelPrice.id;

    result = await adminClient.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${product2Id}", levelId: "${level10Id}", price: 22) {
          id
        }
      }
    `);

    result = await adminClient.request(/* GraphQL */ `
      query {
        adminProducts {
          name
          description
          quantity
          levelPrices {
            level {
              name
            }
            price
          }
        }
      }
    `);

    expect(result).toMatchSnapshot();
  }
);

let orderItem1Id;
orderedTest(
  `query orderItems after 
        creating OrderItem with Quantity 1000 (levelPrice 1)
        creating OrderItem with Quantity 2 (levelPrice 2)
        for customer-level-1`,
  async () => {
    let result = await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice1Id}", quantity: 1000) {
          id
        }
      }
    `);
    orderItem1Id = result.createOrderItem.id;
    result = await level1Client.request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${levelPrice2Id}", quantity: 2) {
          id
        }
      }
    `);
    result = await level1Client.request(/* GraphQL */ `
      query {
        orderItems {
          customerProduct {
            price
            product {
              name
              description
              quantity
            }
          }
          quantity
        }
      }
    `);

    expect(result).toMatchSnapshot();
  }
);

orderedTest(
  `query orderItems after 
        creating OrderItem with Quantity 11 (levelPrice 1)
        creating OrderItem with Quantity 22 (levelPrice 2)
        for customer-level-1-B`,
  async () => {
    let result = await level1BClient.request(/* GraphQL */ `
        mutation {
          createOrderItem(customerProductId: "${levelPrice1Id}", quantity: 11) {
            id
          }
        }
      `);
    result = await level1BClient.request(/* GraphQL */ `
        mutation {
          createOrderItem(customerProductId: "${levelPrice2Id}", quantity: 22) {
            id
          }
        }
      `);
    result = await level1BClient.request(/* GraphQL */ `
      query {
        orderItems {
          customerProduct {
            price
            product {
              name
              description
              quantity
            }
          }
          quantity
        }
      }
    `);

    expect(result).toMatchSnapshot();
  }
);

function removeIdInOrderItems(result) {
  Object.keys(result).forEach((apiName) => {
    result[apiName].orderItems.map((currentValue) => {
      const orderItem = currentValue;
      delete orderItem.customerProduct.product.id;
      return orderItem;
    });
  });
  return result;
}

let customerLevel1OrderId;
orderedTest(
  `customer-level-1 placeOrder
        after updating OrderItem with Quantity 1000 -> 1 (levelPrice 1)`,
  async () => {
    await level1Client.request(/* GraphQL */ `
      mutation {
        updateOrderItem(id: "${orderItem1Id}", quantity: 1) {
          id
        }
      }
    `);

    let result;
    result = await level1Client.request(/* GraphQL */ `
      mutation {
        placeOrder {
          id
          orderNumber
          orderItems
          total
          status
        }
      }
    `);
    customerLevel1OrderId = result.placeOrder.id;
    delete result.placeOrder.id;
    result = removeIdInOrderItems(result);

    expect(result).toMatchSnapshot();
  }
);

orderedTest("customer-level-1 query self's orderItems", async () => {
  const result = await level1Client.request(/* GraphQL */ `
    query {
      orderItems {
        customerProduct {
          price
          product {
            name
            description
            quantity
          }
        }
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest("customer-level-1-B query self's orderItems", async () => {
  const result = await level1BClient.request(/* GraphQL */ `
    query {
      orderItems {
        customerProduct {
          price
          product {
            name
            description
            quantity
          }
        }
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

let customerLevel1BOrderId;
let customerLevel1BOrderNumber;
orderedTest('customer-level-1-B placeOrder', async () => {
  let result = await level1BClient.request(/* GraphQL */ `
    mutation {
      placeOrder {
        id
        orderNumber
        orderItems
        total
        status
      }
    }
  `);
  customerLevel1BOrderId = result.placeOrder.id;
  customerLevel1BOrderNumber = result.placeOrder.orderNumber;
  delete result.placeOrder.id;
  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest(`query adminProducts`, async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('query adminOrders', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminOrders {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('customer-level-1-B query orders', async () => {
  const result = await level1BClient.request(/* GraphQL */ `
    query {
      orders {
        orderNumber
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('admin query order of customer-level-1-B orderNumber 1', async () => {
  let result = await adminClient.request(/* GraphQL */ `
    query {
      adminOrder(company: "customer-level-1-B", orderNumber: ${customerLevel1BOrderNumber}) {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        orderItems
        total
        status
      }
    }
  `);

  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest('customer-level-1-B query order of orderNumber 1', async () => {
  let result = await level1BClient.request(/* GraphQL */ `
    query {
      order(orderNumber: ${customerLevel1BOrderNumber}) {
        orderNumber
        orderItems
        total
        status
      }
    }
  `);

  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest("update customer-level-1-B's order total 55 -> -1 => TOTAL_LT_0", async () => {
  try {
    await adminClient.request(/* GraphQL */ `
      mutation {
        updateOrder(id: "${customerLevel1BOrderId}", total: -1) {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest('update customer-level-1-B\'s order total 55 -> 5 and status "PLACED" -> "SHIPPED"', async () => {
  let result = await adminClient.request(/* GraphQL */ `
    mutation {
      updateOrder(id: "${customerLevel1BOrderId}", total: 5, status: "SHIPPED") {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        orderItems
        total
        status
      }
    }
  `);

  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest("update customer-level-1-B's order nothing", async () => {
  let result = await adminClient.request(/* GraphQL */ `
    mutation {
      updateOrder(id: "${customerLevel1BOrderId}") {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        orderItems
        total
        status
      }
    }
  `);

  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest("customer-level-1-B cancel customer-level-1's order 1 -> ORDER_NOT_EXIST", async () => {
  try {
    await level1BClient.request(/* GraphQL */ `
      mutation {
        cancelOrder(id: "${customerLevel1OrderId}") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest(`query adminProducts`, async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest("admin cancel customer-level-1's order", async () => {
  let result = await adminClient.request(/* GraphQL */ `
    mutation {
      updateOrder(id: "${customerLevel1OrderId}", status: "CANCELED") {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        orderItems
        total
        status
      }
    }
  `);
  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest(`query adminProducts`, async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest("admin cancel customer-level-1-B's order -> ORDER_STATUS_IS_TOO_LATE", async () => {
  try {
    await adminClient.request(/* GraphQL */ `
      mutation {
        updateOrder(id: "${customerLevel1BOrderId}", status: "CANCELED") {
          id
        }
      }
    `);
  } catch (e) {
    expect(e.response).toMatchSnapshot();
  }
});

orderedTest('query adminProducts to see quantity is restored', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

let customerLevel1Order2Id;
orderedTest('customer-level-1 placeOrder for order-2', async () => {
  await level1Client.request(/* GraphQL */ `
    mutation {
      createOrderItem(customerProductId: "${levelPrice2Id}", quantity: 222) {
        id
      }
    }
  `);
  let result = await level1Client.request(/* GraphQL */ `
    mutation {
      placeOrder {
        id
        orderNumber
        orderItems
        total
        status
      }
    }
  `);
  customerLevel1Order2Id = result.placeOrder.id;
  delete result.placeOrder.id;
  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest(`query adminProducts`, async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('query orders with filter: status = CANCELED', async () => {
  const result = await level1Client.request(/* GraphQL */ `
    query {
      orders(filter: { status: ["CANCELED"] }) {
        orderNumber
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest("customer-level-1 cancel self's order 2", async () => {
  let result = await level1Client.request(/* GraphQL */ `
    mutation {
      cancelOrder(id: "${customerLevel1Order2Id}") {
        orderNumber
        orderItems
        total
        status
      }
    }
  `);
  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest(`query adminProducts`, async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('admin mutation adminCreateMessage for customer-level-1 orderNumber 1 1', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    mutation {
      adminCreateMessage(id: "${customerLevel1BOrderId}", content: "adminContent") {
        customer {
          company
        }
        admin {
          name
        }
        from
        content
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('admin mutation adminCreateMessage for customer-level-1 orderNumber 1 1', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    mutation {
      adminCreateMessage(id: "${customerLevel1OrderId}", content: "adminContent for customer-level-1 orderNumber 1") {
        customer {
          company
        }
        admin {
          name
        }
        from
        content
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('customer-level-1-B mutation createMessage for customer-level-1-B orderNumber 1 1', async () => {
  const result = await level1BClient.request(/* GraphQL */ `
    mutation {
      createMessage(id: "${customerLevel1BOrderId}", content: "customer-level-1-B content") {
        customer {
          company
        }
        admin {
          name
        }
        from
        content
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest(
  'customer-level-1 mutation createMessage for customer-level-1-B orderNumber 1 -> ORDER_NOT_EXIST',
  async () => {
    try {
      await level1Client.request(/* GraphQL */ `
      mutation {
        createMessage(id: "${customerLevel1BOrderId}", content: "customer-level-1 content") {
          id
        }
      }
    `);
    } catch (e) {
      expect(e.response).toMatchSnapshot();
    }
  }
);

orderedTest('admin query order of customer-level-1-B orderNumber 1 for seeing messages 1', async () => {
  let result = await adminClient.request(/* GraphQL */ `
    query {
      adminOrder(company: "customer-level-1-B", orderNumber: ${customerLevel1BOrderNumber}) {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        orderItems
        total
        status
        messages {
          customer {
            company
          }
          admin {
            name
          }
          from
          content
        }
      }
    }
  `);
  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest('customer-level-1-B query order of orderNumber 1 for seeing messages 1', async () => {
  let result = await level1BClient.request(/* GraphQL */ `
    query {
      order(orderNumber: ${customerLevel1BOrderNumber}) {
        orderNumber
        orderItems
        total
        status
        messages {
          customer {
            company
          }
          admin {
            name
          }
          from
          content
        }
      }
    }
  `);
  result = removeIdInOrderItems(result);

  expect(result).toMatchSnapshot();
});

orderedTest(`query adminProducts`, async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminProducts {
        name
        description
        quantity
        levelPrices {
          level {
            name
          }
          price
        }
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('query adminOrders with filter: status = COMPLETED, CANCELED', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminOrders(filter: { status: ["COMPLETED", "CANCELED"] }) {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('deleteOrder customer-level-1-2', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    mutation {
      deleteOrder(id: "${customerLevel1Order2Id}") {
        orderNumber
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('query adminOrders with filter: status = COMPLETED, CANCELED', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    query {
      adminOrders(filter: { status: ["COMPLETED", "CANCELED"] }) {
        orderNumber
        customer {
          company
          level {
            name
          }
        }
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('deleteOrder customer-level-1-B', async () => {
  const result = await adminClient.request(/* GraphQL */ `
    mutation {
      deleteOrder(id: "${customerLevel1BOrderId}") {
        orderNumber
        total
        status
      }
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest('seeing messages', async () => {
  const result = await prisma.messages().$fragment(/* GraphQL */ `
    {
      from
      content
    }
  `);

  expect(result).toMatchSnapshot();
});
