const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of default admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client.admin1 = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

function adminNotifications(needId) {
  let extraFields = '';
  if (needId) extraFields = 'id';
  return this.request(/* GraphQL */ `
    query {
      adminNotifications {
        ${extraFields}
        fromAdmin {
          username
          email
          name
        }
        fromCustomer {
          username
          email
          company
        }
        toAdmin {
          username
          email
          name
        }
        toCustomer {
          username
          email
          company
        }
        messageContent
        orderCustomer {
          company
        }
        orderNumber
        orderStatusFrom
        orderStatusTo
        orderTotalFrom
        orderTotalTo
        hasBeenRead
      }
    }
  `);
}

function notifications(needId) {
  let extraFields = '';
  if (needId) extraFields = 'id';
  return this.request(/* GraphQL */ `
    query {
      notifications {
        ${extraFields}
        fromAdmin {
          username
          email
          name
        }
        fromCustomer {
          username
          email
          company
        }
        toAdmin {
          username
          email
          name
        }
        toCustomer {
          username
          email
          company
        }
        messageContent
        orderCustomer {
          company
        }
        orderNumber
        orderStatusFrom
        orderStatusTo
        orderTotalFrom
        orderTotalTo
        hasBeenRead
      }
    }
  `);
}

const id = {};
orderedTest(
  `admin-1 create admin-2
        ↑ create level-1
        ↑ create customer-1
        ↑ create customer-2
        ↑ create levelPrice-1
        ↑ create product-1
        customer-1 create orderItem-1
        ↑ placeOrder-1
        admin-1 query notifications`,
  async () => {
    await client.admin1.request(/* GraphQL */ `
      mutation {
        createAdmin(username: "admin2", password: "1", email: "admin2@gmail.com", name: "Admin2") {
          id
        }
      }
    `);
    client.admin2 = new GraphQLClient(API_URL, {
      headers: {
        Authorization: `Bearer ${
          (
            await new GraphQLClient(API_URL).request(/* GraphQL */ `
              mutation {
                loginAdmin(username: "admin2", password: "1") {
                  token
                }
              }
            `)
          ).loginAdmin.token
        }`,
      },
    });

    id.level1 = (
      await client.admin1.request(/* GraphQL */ `
        mutation {
          createLevel(name: "level-1") {
            id
          }
        }
      `)
    ).createLevel.id;
    await client.admin1.request(/* GraphQL */ `
      mutation {
        createCustomer(username: "customer-1", password: "1", email: "customer-1@gmail.com", company: "Customer-1", levelId: "${id.level1}") {
          id
        }
      }
    `);
    client.customer1 = new GraphQLClient(API_URL, {
      headers: {
        Authorization: `Bearer ${
          (
            await new GraphQLClient(API_URL).request(/* GraphQL */ `
              mutation {
                loginCustomer(username: "customer-1", password: "1") {
                  token
                }
              }
            `)
          ).loginCustomer.token
        }`,
      },
    });
    await client.admin1.request(/* GraphQL */ `
      mutation {
        createCustomer(username: "customer-2", password: "1", email: "customer-2@gmail.com", company: "Customer-2", levelId: "${id.level1}") {
          id
        }
      }
    `);
    client.customer2 = new GraphQLClient(API_URL, {
      headers: {
        Authorization: `Bearer ${
          (
            await new GraphQLClient(API_URL).request(/* GraphQL */ `
              mutation {
                loginCustomer(username: "customer-2", password: "1") {
                  token
                }
              }
            `)
          ).loginCustomer.token
        }`,
      },
    });
    id.product1 = (
      await client.admin1.request(/* GraphQL */ `
        mutation {
          createProduct(name: "product-1", description: "description-1", quantity: 991) {
            id
          }
        }
      `)
    ).createProduct.id;
    id.levelPrice1 = (
      await client.admin1.request(/* GraphQL */ `
        mutation {
          createLevelPrice(productId: "${id.product1}", levelId: "${id.level1}", price: 1) {
            id
          }
        }
      `)
    ).createLevelPrice.id;
    id.orderItem1 = (
      await client.customer1.request(/* GraphQL */ `
        mutation {
          createOrderItem(customerProductId: "${id.levelPrice1}", quantity: 1) {
            id
          }
        }
      `)
    ).createOrderItem.id;

    id.order1 = (
      await client.customer1.request(/* GraphQL */ `
        mutation {
          placeOrder {
            id
          }
        }
      `)
    ).placeOrder.id;

    const result = await adminNotifications.call(client.admin1);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('admin-2 query notifications', async () => {
  const result = await adminNotifications.call(client.admin2);
  expect(result).toMatchSnapshot();
});

orderedTest(
  `admin-1 updateOrder PLACED -> IN_PROCESS and total 1 -> 2
        customer-1 query notifications`,
  async () => {
    await client.admin1.request(/* GraphQL */ `
      mutation {
        updateOrder(id: "${id.order1}", status: "IN_PROCESS", total: 2) {
          id
        }
      }
    `);
    const result = await notifications.call(client.customer1);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('admin-1 query notifications', async () => {
  const result = await adminNotifications.call(client.admin1);
  expect(result).toMatchSnapshot();
});

orderedTest('admin-2 query notifications', async () => {
  const result = await adminNotifications.call(client.admin2);
  expect(result).toMatchSnapshot();
});

orderedTest(
  `customer-1 cancelOrder
        admin-1 query notifications`,
  async () => {
    await client.customer1.request(/* GraphQL */ `
      mutation {
        cancelOrder(id: "${id.order1}") {
          id
        }
      }
    `);
    const result = await adminNotifications.call(client.admin1);
    expect(result).toMatchSnapshot();
  }
);

orderedTest(
  `customer-2 create orderItem-2
        ↑ placeOrder-2
        admin-1 updateOrder CANCELED
        customer-2 query notifications`,
  async () => {
    id.orderItem2 = (
      await client.customer2.request(/* GraphQL */ `
        mutation {
          createOrderItem(customerProductId: "${id.levelPrice1}", quantity: 1) {
            id
          }
        }
      `)
    ).createOrderItem.id;

    id.order2 = (
      await client.customer2.request(/* GraphQL */ `
        mutation {
          placeOrder {
            id
          }
        }
      `)
    ).placeOrder.id;

    await client.admin1.request(/* GraphQL */ `
      mutation {
        updateOrder(id: "${id.order2}", status: "CANCELED") {
          id
        }
      }
    `);

    const result = await notifications.call(client.customer2);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('admin-1 query notifications', async () => {
  const result = await adminNotifications.call(client.admin1);
  expect(result).toMatchSnapshot();
});

orderedTest('admin-2 query notifications', async () => {
  const result = await adminNotifications.call(client.admin2);
  expect(result).toMatchSnapshot();
});

orderedTest(
  `admin-1 adminCreateMessage
        customer-1 query notifications`,
  async () => {
    await client.admin1.request(/* GraphQL */ `
      mutation {
        adminCreateMessage(id: "${id.order1}", content: "admin content") {
          id
        }
      }
    `);
    const result = await notifications.call(client.customer1);
    expect(result).toMatchSnapshot();
  }
);

orderedTest(
  `customer-1 createMessage
        admin-1 query notifications`,
  async () => {
    await client.customer1.request(/* GraphQL */ `
      mutation {
        createMessage(id: "${id.order1}", content: "content") {
          id
        }
      }
    `);
    const result = await adminNotifications.call(client.admin1);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('customer-1 query notifications', async () => {
  let results = await notifications.call(client.customer2, true);
  id.customer2 = {};
  id.customer2.notificationIds = [];
  results.notifications = results.notifications.map((currentValue) => {
    const notification = currentValue;
    id.customer2.notificationIds.push(notification.id);
    delete notification.id;
    return notification;
  });

  results = await notifications.call(client.customer1, true);
  id.customer1 = {};
  id.customer1.notificationIds = [];
  results.notifications = results.notifications.map((currentValue) => {
    const notification = currentValue;
    id.customer1.notificationIds.push(notification.id);
    delete notification.id;
    return notification;
  });

  expect(results).toMatchSnapshot();
});

orderedTest(`customer-2 updateNotification customer-1's last notifications`, async () => {
  const result = await client.customer2.request(/* GraphQL */ `
    mutation {
      updateNotification(ids: ["${id.customer1.notificationIds[id.customer1.notificationIds.length - 1]}"]) {
        count
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest(`customer-1 updateNotification self's last notifications`, async () => {
  const result = await client.customer1.request(/* GraphQL */ `
    mutation {
      updateNotification(ids: ["${id.customer1.notificationIds[id.customer1.notificationIds.length - 1]}"]) {
        count
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest('customer-1 query notifications', async () => {
  const result = await notifications.call(client.customer1);
  expect(result).toMatchSnapshot();
});

orderedTest('admin-1 query adminNotifications', async () => {
  const results = await adminNotifications.call(client.admin1, true);
  id.admin1 = {};
  id.admin1.notificationIds = [];
  results.adminNotifications = results.adminNotifications.map((currentValue) => {
    const adminNotification = currentValue;
    id.admin1.notificationIds.push(adminNotification.id);
    delete adminNotification.id;
    return adminNotification;
  });

  expect(results).toMatchSnapshot();
});

orderedTest('admin-1 adminUpdateNotification last notifications', async () => {
  const result = await client.admin1.request(/* GraphQL */ `
    mutation {
      adminUpdateNotification(ids: ["${id.admin1.notificationIds[id.admin1.notificationIds.length - 1]}"]) {
        count
      }
    }
  `);
  expect(result).toMatchSnapshot();
});

orderedTest('admin-1 query adminNotifications', async () => {
  const result = await adminNotifications.call(client.admin1);
  expect(result).toMatchSnapshot();
});
