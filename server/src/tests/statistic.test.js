const { GraphQLClient } = require('graphql-request');
const { prisma } = require('../prisma/generated/prisma-client');

const utils = require('../utils');
const { API_URL, initialDbData, orderedTest } = require('./shared');

beforeAll(async () => {
  await initialDbData(prisma, utils);
});

const client = {};
orderedTest('Get client of Admin', async () => {
  const result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      loginAdmin(username: "admin", password: "admin") {
        token
        admin {
          name
        }
      }
    }
  `);
  const adminToken = result.loginAdmin.token;
  expect(adminToken).toBeTruthy();
  client[result.loginAdmin.admin.name] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${adminToken}`,
    },
  });
});

const id = {};
orderedTest(
  `createProduct name product-1
        createProduct name product-2
        createLevel name level-1
        createLevel name level-10
        createLevel name level-100
        createLevelPrice price 1 with product-1, level-1
        createLevelPrice price 11 with product-1, level-10
        createLevelPrice price 101 with product-1, level-100
        createLevelPrice price 2 with product-2, level-1
        createLevelPrice price 12 with product-2, level-10
        createLevelPrice price 102 with product-2, level-100
        adminProducts`,
  async () => {
    let result;
    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-1", description: "description-1", quantity: 1000) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createProduct(name: "product-2", description: "description-2", quantity: 1000) {
          id
          name
        }
      }
    `);
    id[result.createProduct.name] = result.createProduct.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-1") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-10") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevel(name: "level-100") {
          id
          name
        }
      }
    `);
    id[result.createLevel.name] = result.createLevel.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-1']}", price: 1) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-10']}", price: 11) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-1']}", levelId: "${id['level-100']}", price: 101) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-1']}", price: 2) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-10']}", price: 12) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createLevelPrice(productId: "${id['product-2']}", levelId: "${id['level-100']}", price: 102) {
          id
          price
        }
      }
    `);
    id[`levelPrice-${result.createLevelPrice.price}`] = result.createLevelPrice.id;

    result = await client.Admin.request(/* GraphQL */ `
      query {
        adminProducts {
          name
          description
          quantity
          levelPrices {
            level {
              name
            }
            price
          }
        }
      }
    `);
    expect(result).toMatchSnapshot();
  }
);

orderedTest('Get client of Customer-level-1', async () => {
  let result;
  result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createCustomer(
          username: "customer-level-1"
          password: "1"
          email: "customer-level-1@gmail.com"
          company: "Customer-level-1"
          levelId: "${id['level-1']}"
        ) {
          id
          username
        }
      }
    `);
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "${result.createCustomer.username}", password: "1") {
          token
          customer {
            company
          }
        }
      }
    `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});
orderedTest('Get client of Customer-level-10', async () => {
  let result;
  result = await client.Admin.request(/* GraphQL */ `
      mutation {
        createCustomer(
          username: "customer-level-10"
          password: "1"
          email: "customer-level-10@gmail.com"
          company: "Customer-level-10"
          levelId: "${id['level-10']}"
        ) {
          id
          username
        }
      }
    `);
  result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
      mutation {
        loginCustomer(username: "${result.createCustomer.username}", password: "1") {
          token
          customer {
            company
          }
        }
      }
    `);
  const { token } = result.loginCustomer;
  expect(token).toBeTruthy();
  client[result.loginCustomer.customer.company] = new GraphQLClient(API_URL, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
});

orderedTest(
  `Customer-level-1 createOrderItem createLevelPrice price 1 * 100
          createOrderItem createLevelPrice price 2 * 200
          placeOrder
        Admin updateOrder COMPLETED
    
        Customer-level-10 createOrderItem createLevelPrice price 11 * 300
          createOrderItem createLevelPrice price 12 * 400
          placeOrder
        Admin updateOrder COMPLETED

        Customer-level-10 createOrderItem createLevelPrice price 11 * 5
          placeOrder

        Admin totalSales today 8600`,
  async () => {
    let result;
    await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-1`]}", quantity: 100) {
          id
        }
      }
    `);
    await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-2`]}", quantity: 200) {
          id
        }
      }
    `);
    result = await client['Customer-level-1'].request(/* GraphQL */ `
      mutation {
        placeOrder {
          id
        }
      }
    `);
    await client.Admin.request(/* GraphQL */ `
      mutation {
        updateOrder(id: "${result.placeOrder.id}", status:"COMPLETED") {
          id
        }
      }
    `);

    await client['Customer-level-10'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-11`]}", quantity: 300) {
          id
        }
      }
    `);
    await client['Customer-level-10'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-12`]}", quantity: 400) {
          id
        }
      }
    `);
    result = await client['Customer-level-10'].request(/* GraphQL */ `
      mutation {
        placeOrder {
          id
        }
      }
    `);
    await client.Admin.request(/* GraphQL */ `
      mutation {
        updateOrder(id: "${result.placeOrder.id}", status: "COMPLETED") {
          id
        }
      }
    `);

    await client['Customer-level-10'].request(/* GraphQL */ `
      mutation {
        createOrderItem(customerProductId: "${id[`levelPrice-11`]}", quantity: 5) {
          id
        }
      }
    `);
    await client['Customer-level-10'].request(/* GraphQL */ `
      mutation {
        placeOrder {
          id
        }
      }
    `);

    const todayStartAt = new Date();
    todayStartAt.setHours(0, 0, 0, 0);
    const todayEndAt = new Date();
    todayEndAt.setDate(todayEndAt.getDate() + 1);
    todayEndAt.setHours(0, 0, 0, 0);

    result = await client.Admin.request(/* GraphQL */ `
      query {
        totalSales(startDate: "${todayStartAt.toISOString()}", endDate: "${todayEndAt.toISOString()}")
      }
    `);

    expect(result).toMatchSnapshot();
  }
);

orderedTest(`Admin totalSales yesterday 0`, async () => {
  const yesterdayStartAt = new Date();
  yesterdayStartAt.setDate(yesterdayStartAt.getDate() - 1);
  yesterdayStartAt.setHours(0, 0, 0, 0);
  const yesterdayEndAt = new Date();
  yesterdayEndAt.setDate(yesterdayEndAt.getDate());
  yesterdayEndAt.setHours(0, 0, 0, 0);

  const result = await client.Admin.request(/* GraphQL */ `
    query {
      totalSales(startDate: "${yesterdayStartAt.toISOString()}", endDate: "${yesterdayEndAt.toISOString()}")
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest(`Admin bestSellerList today`, async () => {
  const todayStartAt = new Date();
  todayStartAt.setHours(0, 0, 0, 0);
  const todayEndAt = new Date();
  todayEndAt.setDate(todayEndAt.getDate() + 1);
  todayEndAt.setHours(0, 0, 0, 0);

  const result = await client.Admin.request(/* GraphQL */ `
    query {
      bestSellerList(startDate: "${todayStartAt.toISOString()}", endDate: "${todayEndAt.toISOString()}")
    }
  `);

  expect(result).toMatchSnapshot();
});

orderedTest(`Admin bestSellerList yesterday {}`, async () => {
  const yesterdayStartAt = new Date();
  yesterdayStartAt.setDate(yesterdayStartAt.getDate() - 1);
  yesterdayStartAt.setHours(0, 0, 0, 0);
  const yesterdayEndAt = new Date();
  yesterdayEndAt.setDate(yesterdayEndAt.getDate());
  yesterdayEndAt.setHours(0, 0, 0, 0);

  const result = await client.Admin.request(/* GraphQL */ `
    query {
      bestSellerList(startDate: "${yesterdayStartAt.toISOString()}", endDate: "${yesterdayEndAt.toISOString()}")
    }
  `);

  expect(result).toMatchSnapshot();
});
