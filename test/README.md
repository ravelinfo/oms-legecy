## Install
`npm install` in this folder

## Before run test
In project root folder, run `docker-compose -f  test/docker-compose-test.yml --project-directory . up`.

The mysql volume is different from main volume.

## Run test
Run `npm run open` to open test board  
Run `npm run test` to run all test suites