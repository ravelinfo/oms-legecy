describe('Admin login', () => {
  it('Visit admin-login', () => {
    cy.visit('/admin-login');
  });

  it('Missing field', () => {
    cy.get('button').click();
    cy.contains("Sorry, we couldn't find an account with that username.");
  });

  it('No such admin username', () => {
    cy.visit('/admin-login');
    cy.get('input:eq(0)').type('invalid');
    cy.get('input:eq(1)').type('invalid');
    cy.get('button').click();
    cy.contains("Sorry, we couldn't find an account with that username.");
  });

  it('Wrong password', () => {
    cy.visit('/admin-login');
    cy.get('input:eq(0)').type('admin');
    cy.get('input:eq(1)').type('wrong');
    cy.get('button').click();
    cy.contains('Sorry, that password is not right.');
  });

  it('Login successfully', () => {
    cy.visit('/admin-login');
    cy.get('input:eq(0)').type('admin');
    cy.get('input:eq(1)').type('admin');
    cy.get('button').click();
    cy.url().should('eq', 'http://localhost/admin');
  });
});
