const { GraphQLClient } = require('graphql-request');

describe('Admin admins', () => {
  beforeEach(async () => {
    const result = await new GraphQLClient('http://localhost/api')
      .request(/* GraphQL */ `
      mutation {
        loginAdmin(username: "admin", password: "admin") {
          token
          admin {
            name
          }
        }
      }
    `);
    return cy.setCookie('token', result.loginAdmin.token);
  });

  it('Admin table shows admin as username', () => {
    cy.visit('/admin/admins');
    cy.get(
      '#layout > section > main > div > div.ant-table-wrapper > div > div > div > div > div > table > tbody > tr > td:nth-child(2) > span'
    ).contains('admin');
  });

  it('Add user "test-user" and show on the table', () => {
    cy.visit('/admin/admins');

    // Type Name(Test User), Username(test-user), Password(test-user) and click Confirm
    cy.get(
      '#layout > section > main > div > div:nth-child(1) > div > a > button'
    ).click();
    cy.url().should('eq', 'http://localhost/admin/add-admin');
    cy.get(
      '#layout > section > main > div > form > div:nth-child(1) > div.ant-col.ant-form-item-control-wrapper > div > span > input'
    ).type('Test User');
    cy.get(
      '#layout > section > main > div > form > div:nth-child(2) > div.ant-col.ant-form-item-control-wrapper > div > span > input'
    ).type('test-user');
    cy.get(
      '#layout > section > main > div > form > div:nth-child(3) > div.ant-col.ant-form-item-control-wrapper > div > span > span > input'
    ).type('test-user');
    cy.get('#layout > section > main > div > form > button').click();

    // Should redirect to admins page and show the username
    cy.url().should('eq', 'http://localhost/admin/admins');
    cy.contains('Test User');
    cy.contains('test-user');
  });

  it('Delete user "test-user"', () => {
    cy.visit('/admin/admins');

    cy.get(
      '#layout > section > main > div > div.ant-table-wrapper > div > div > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > button > img'
    ).click();
    cy.contains('Yes').click();

    cy.contains('test-user').should('not.exist');
  });
});
