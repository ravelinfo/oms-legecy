# Order Management System, OMS

OMS is a web-based application, a platform for order managers and customers having transaction online.

## Installation

## Run

## Generate a private key and self-signed certificate for local development

```
mkdir -p nginx/ssl/
openssl req -x509 -out nginx/ssl/localhost.crt -keyout nginx/ssl/localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

## Manually push images to ECR

```
docker tag oms_client 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com/ravelinfo/oms-client
docker tag oms_nginx 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com/ravelinfo/oms-nginx
docker tag oms_api 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com/ravelinfo/oms-server

aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com

docker push 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com/ravelinfo/oms-client
docker push 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com/ravelinfo/oms-nginx
docker push 093003787802.dkr.ecr.ap-southeast-1.amazonaws.com/ravelinfo/oms-server
```

## Test

## License
