import { message } from 'antd';

const constants = {
  ADM_EXIST: ' This account already exists!',
  ADM_NOT_EXIST: "This account doesn't exists!",
  LEVEL_EXIST: 'This level already exists!',
  LEVEL_IN_USE: 'This level is in use!',
  CTM_EXIST: 'This customer already exists!',
  CTM_NOT_EXIST: "This customer doesn't exist!",
  PRICE_LT_0: 'Price must be greater than or equal to 0.',
  TOTAL_LT_0: 'Total must be greater than or equal to 0.',
  CURRENT_PASSWORD_INCORRECT: 'Your current password is incorrect!',
  // Shopping cart
  PRODUCT_EQ_0: 'Please put at least one product in the cart!',
  ADD_TO_CART_FAIL: 'Failed to add product to the cart. Please try again, we’re going to refresh the page for you.',
  LEVEL_PRICE_ID_NOT_AVAILABLE: 'Please check the order again!',
  // Customer Order
  ORDER_STATUS_IS_TOO_LATE: 'Failed to cancel the order. Please contact us for more information.',
  ORDER_NOT_EXIST: 'Failed to cancel the order. Please contact customer us for further assistance.',
  // OrderItem
  QUANTITY_LTE_0: 'Quantity must be greater than 0!',
};

export default {
  handleError: (err) => {
    try {
      const { graphQLErrors } = err;
      // Non-customized error code
      if (!constants[graphQLErrors[0].message]) {
        message.error('Unexpected error occurs!');
      } else {
        message.warning(constants[graphQLErrors[0].message]);
      }
    } catch (e) {
      message.error('Unexpected error occurs!');
    }
  },
};
