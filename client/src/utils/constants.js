export default {
  ORDER_STATUS: {
    PLACED: 'Order Placed',
    IN_PROCESS: 'In Process',
    SHIPPED: 'Shipped',
    COMPLETED: 'Completed',
    CANCELED: 'Canceled',
  },
  PROCESS_ORDER_STATUS: {
    PLACED: 'Order Placed',
    IN_PROCESS: 'In Process',
    SHIPPED: 'Shipped',
  },
  PAST_ORDER_STATUS: {
    COMPLETED: 'Completed',
    CANCELED: 'Canceled',
  },
  DASHBOARD_TAB: [
    {
      key: 'Day',
      tab: 'Day',
    },
    {
      key: 'Week',
      tab: 'Week',
    },
    {
      key: 'Month',
      tab: 'Month',
    },
    {
      key: 'Year',
      tab: 'Year',
    },
  ],
};
