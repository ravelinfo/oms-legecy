import moment from 'moment';

export default {
  price: (price) => price.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,'),
  datetime: (datetime, area) => {
    switch (area) {
      case 'tw':
        return moment(datetime).format('YYYY/DD/MM HH:mm');
      case 'id':
        return moment(datetime).format('DD/MM/YYYY hh:mm a');
      default:
        return moment(datetime).format('MMM Do YYYY h:mm a');
    }
  },
};
