export default {
  getBadgeColor: (status) => {
    switch (status) {
      case 'PLACED':
        return '#999';
      case 'IN_PROCESS':
        return 'blue';
      case 'SHIPPED':
        return 'orange';
      case 'CANCELED':
        return 'pink';
      case 'COMPLETED':
        return 'green';
      default:
        return 'transparent';
    }
  },
  growthRate: (curSales, pastSales) => {
    return (((curSales - pastSales) / pastSales) * 100).toFixed(2);
  },
};
