import React from 'react';
import '../App.css';
import 'antd/dist/antd.css';
import { Switch, Route } from 'react-router-dom';
import LoginPage from './LoginPage';
import AdminLoginPage from './AdminLoginPage';
import Main from './Manager/Main';
import Customer from './Customer/Customer';

function App() {
  return (
    <div className="App" style={{ position: 'relative' }}>
      <Switch>
        <Route path="/" exact component={LoginPage} />
        <Route path="/admin-login" exact component={AdminLoginPage} />
        <Route path="/admin" component={Main} />
        <Route path="/customer" component={Customer} />
      </Switch>
    </div>
  );
}

export default App;
