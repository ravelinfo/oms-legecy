import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { FETCH_LEVEL, FETCH_LEVELS } from '../graphql/queries';
import { UPDATE_LEVEL } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

class EditLevel extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
    };
  }

  render() {
    const {
      match: {
        params: { levelId },
      },
    } = this.props;
    const { name } = this.state;
    return (
      <FormContainer header="Edit Level">
        <Query
          query={FETCH_LEVEL}
          variables={{ id: levelId }}
          onCompleted={(data) => {
            try {
              const { level } = data;
              this.setState({ name: level.name });
            } catch (e) {
              const { history } = this.props;
              history.push('/admin/level');
            }
          }}
        >
          {({ loading, error }) => {
            if (loading) return <div>loading</div>;
            if (error) return <div>error</div>;
            return (
              <>
                <Form.Item label="Level" colon={false} required validateStatus={name ? 'success' : 'error'}>
                  <Input onChange={(e) => this.setState({ name: e.target.value })} value={name} />
                </Form.Item>
                <Mutation
                  mutation={UPDATE_LEVEL}
                  variables={{ id: levelId, name }}
                  update={(store, { data: { updateLevel } }) => {
                    const data = store.readQuery({ query: FETCH_LEVELS });

                    const editedLevel = data.levels.find((level) => level.id === levelId);
                    editedLevel.name = updateLevel.name;

                    store.writeQuery({ query: FETCH_LEVELS, data });
                  }}
                  onError={(err) => errors.handleError(err)}
                  onCompleted={() => {
                    const { history } = this.props;
                    history.push('/admin/level');
                  }}
                >
                  {(updateLevel) => {
                    if (!name) return <FormButton disabled>Confirm</FormButton>;
                    return <FormButton onClick={updateLevel}>Confirm</FormButton>;
                  }}
                </Mutation>
              </>
            );
          }}
        </Query>
      </FormContainer>
    );
  }
}

EditLevel.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default EditLevel;
