import React from 'react';
import PropTypes from 'prop-types';

// GraphQL
import { Query } from 'react-apollo';
import { FETCH_ADMIN_ORDER } from '../graphql/queries';

// Utils
import { formatters } from '../../utils';

const OrderMessage = (props) => {
  const { company, orderNumber } = props;
  return (
    <Query query={FETCH_ADMIN_ORDER} variables={{ company, orderNumber }}>
      {({ loading, error, data }) => {
        if (loading) return <div>lodaing</div>;
        if (error) return <div>Error</div>;
        const { adminOrder } = data;
        return (
          <>
            {adminOrder.messages.map((message) => {
              const adminUsername = message.admin ? message.admin.username : '(Deleted Admin)';
              return (
                <div
                  className={`OrderDetail__chatbox OrderDetail__chatbox--${
                    message.from === 'ADMIN' ? 'right' : 'left'
                  }`}
                  key={message.id}
                >
                  <span className="OrderDetail__chatbox--messager">
                    {message.from === 'ADMIN' ? adminUsername : message.customer.company}
                  </span>
                  <p className="OrderDetail__chatbox--message">{message.content}</p>
                  <span className="OrderDetail__chatbox--date">{formatters.datetime(message.createdAt, 'id')}</span>
                </div>
              );
            })}
          </>
        );
      }}
    </Query>
  );
};

OrderMessage.propTypes = {
  company: PropTypes.string.isRequired,
  orderNumber: PropTypes.number.isRequired,
};

export default OrderMessage;
