import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Input, Checkbox } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { ADMIN_ME } from '../graphql/queries';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

const ADMIN_UPDATE_MY_PROFILE = gql`
  mutation AdminUpdateMyProfile($email: String, $name: String, $isEmailNotificationEnabled: Boolean) {
    adminUpdateMyProfile(email: $email, name: $name, isEmailNotificationEnabled: $isEmailNotificationEnabled) {
      id
      username
      name
      email
      isEmailNotificationEnabled
    }
  }
`;

class Setting extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      adminId: '',
      isEmailNotificationEnabled: false,
    };
  }

  setFormValue(data) {
    const { name, email, id, isEmailNotificationEnabled } = data.adminMe;
    this.setState({ name, email, adminId: id, isEmailNotificationEnabled });
  }

  render() {
    const { name, email, adminId, isEmailNotificationEnabled } = this.state;
    return (
      <FormContainer header="Setting">
        <Query query={ADMIN_ME} onCompleted={(data) => this.setFormValue(data)}>
          {({ loading, error, data }) => {
            if (loading) return <div>loading</div>;
            if (error) return <div>error</div>;
            const { username } = data.adminMe;
            return (
              <>
                <Form.Item label="Name" colon={false}>
                  <Input value={name} onChange={(e) => this.setState({ name: e.target.value })} />
                </Form.Item>
                <Form.Item label="Username" colon={false}>
                  <Input value={username} />
                </Form.Item>
                <Form.Item label="Email" colon={false}>
                  <Input value={email} onChange={(e) => this.setState({ email: e.target.value })} />
                </Form.Item>
              </>
            );
          }}
        </Query>
        <span style={{ display: 'block', fontSize: '14px', fontWeight: 'bold', color: '#999', marginBottom: ' 20px ' }}>
          <Checkbox
            checked={isEmailNotificationEnabled}
            onChange={(e) => this.setState({ isEmailNotificationEnabled: e.target.checked })}
          />
          <span style={{ marginLeft: '10px' }}>Email Notification</span>
        </span>
        <Link to={`/admin/edit-setting-password/${adminId}`}>
          <p
            style={{
              color: '#666',
              textDecoration: 'underline',
              display: 'inline-block',
            }}
          >
            Change password
          </p>
        </Link>
        <Mutation
          mutation={ADMIN_UPDATE_MY_PROFILE}
          variables={{ email, name, isEmailNotificationEnabled }}
          onError={(err) => errors.handleError(err)}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin');
          }}
        >
          {(adminUpdateMyProfile) => <FormButton onClick={adminUpdateMyProfile}>Save</FormButton>}
        </Mutation>
      </FormContainer>
    );
  }
}

Setting.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default Setting;
