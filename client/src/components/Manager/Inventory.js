import React, { Component } from 'react';
import '../../css/Inventory.css';
import { Input, Table, Modal, Skeleton, Tag } from 'antd';
import { Link } from 'react-router-dom';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_PRODUCTS } from '../graphql/queries';
import { DELETE_PRODUCT, DELETE_LEVEL_PRICE } from '../graphql/mutations';

// Utils
import { formatters } from '../../utils';

// Icons
import Edit from '../../assets/icons/edit.svg';
import TrashCan from '../../assets/icons/trash-can.svg';

// Component
import Button from '../commons/Button';
import Error from '../commons/Error';

const { Column } = Table;
const { confirm } = Modal;

const showDeleteProduct = (props, id) => {
  confirm({
    title: 'Are you sure to delete this product?',
    onOk() {
      // Checke if deleted product is in expandedRows
      if (JSON.parse(localStorage.expandedRows).includes(id)) {
        // Removed deleted expanded row
        const updatedExpandedRows = JSON.parse(localStorage.expandedRows).filter((item) => item !== id);
        // Save new expanded rows
        localStorage.setItem('expandedRows', JSON.stringify(updatedExpandedRows));
      }
      props.client
        .mutate({
          mutation: DELETE_PRODUCT,
          variables: { productId: id },
          update: (store, { data: { deleteProduct } }) => {
            const data = store.readQuery({
              query: FETCH_PRODUCTS,
              variables: { id },
            });

            const updatedProducts = data.adminProducts.filter((product) => product.id !== deleteProduct.id);
            data.adminProducts = updatedProducts;

            store.writeQuery({
              query: FETCH_PRODUCTS,
              variables: { id },
              data,
            });
          },
        })
        .then((res) => res)
        .catch(() => window.location.reload());
    },
    onCancel() {},
  });
};

const showDeletePrice = (props, levelPriceId, productId) => {
  confirm({
    title: 'Are you sure to delete this price?',
    onOk() {
      props.client
        .mutate({
          mutation: DELETE_LEVEL_PRICE,
          variables: { levelPriceId },
          update: (store, { data: { deleteLevelPrice } }) => {
            const data = store.readQuery({
              query: FETCH_PRODUCTS,
              variables: { levelPriceId },
            });

            // Find produt which price needs to be deleted
            const findProduct = data.adminProducts.find((adminProduct) => adminProduct.id === productId);
            // Filter deleted price
            const updatedPrices = findProduct.levelPrices.filter((price) => price.id !== deleteLevelPrice.id);
            // Update products
            findProduct.levelPrices = updatedPrices;

            store.writeQuery({
              query: FETCH_PRODUCTS,
              data,
              variables: { levelPriceId },
            });
          },
        })
        .then((res) => res)
        .catch(() => window.location.reload());
    },
    onCancel() {},
  });
};

class Inventory extends Component {
  constructor() {
    super();
    this.state = {
      expandedRows: [],
      searchStr: '',
      showResult: '',
      currentPage: 1,
    };
  }

  componentDidMount() {
    // Check if expanedRows exists
    if (localStorage.expandedRows !== undefined) {
      this.setState({ expandedRows: JSON.parse(localStorage.expandedRows) });
    }
    // Check if currentPage exists
    if (localStorage.getItem('inventory_page')) {
      this.setState({ currentPage: parseInt(localStorage.getItem('inventory_page'), 10) });
    }
  }

  handleExpandedRows = (record) => {
    this.setState(
      (prevState) =>
        // Check if current row is expanded, if it's expaneded, remove id, otherwise save the id
        prevState.expandedRows.includes(record.id)
          ? { expandedRows: prevState.expandedRows.filter((id) => id !== record.id) }
          : { expandedRows: [...prevState.expandedRows, record.id] },
      () => {
        // setState is async, use setState's callback function to make sure the state is updated when you set the items
        const { expandedRows } = this.state;
        localStorage.setItem('expandedRows', JSON.stringify(expandedRows));
      }
    );
  };

  clearSearch = (refetch) => {
    this.setState({ searchStr: '', showResult: null }, () => {
      const { searchStr } = this.state;
      refetch({ filter: { name: searchStr } });
    });
  };

  handlePagination = (pagination) => {
    this.setState({ currentPage: pagination.current }, () => {
      const { currentPage } = this.state;
      localStorage.setItem('inventory_page', currentPage);
    });
  };

  render() {
    const { expandedRows, searchStr, showResult, currentPage } = this.state;

    return (
      <div style={{ width: '90%', margin: '20px auto' }}>
        <Query query={FETCH_PRODUCTS}>
          {({ loading, error, data, refetch }) => {
            if (loading) return <Skeleton active />;
            if (error) return <Error />;

            const { adminProducts } = data;

            return (
              <>
                <div className="Inventory__search">
                  <Input.Search
                    placeholder="Search product"
                    style={{ width: '250px' }}
                    value={searchStr}
                    onChange={(e) => this.setState({ searchStr: e.target.value })}
                    onSearch={(value) => {
                      refetch({ filter: { name: value } });
                      this.setState({ showResult: value, searchStr: '' });
                    }}
                  />
                  {showResult ? (
                    <div className="Inventory__search--result">
                      <span>
                        Search results for <span style={{ fontWeight: 'bold' }}>{showResult}</span>
                      </span>
                      <button
                        type="button"
                        onClick={() => this.clearSearch(refetch)}
                        className="Inventory__search--result-btn"
                      >
                        x
                      </button>
                    </div>
                  ) : null}
                  <Link to="/admin/add-product">
                    <Button bg="#2e6f84" color="#fff">
                      Add Product
                    </Button>
                  </Link>
                </div>
                <Table
                  dataSource={adminProducts}
                  rowKey={(adminProduct) => adminProduct.id}
                  onExpand={(expanded, record) => this.handleExpandedRows(record)}
                  expandedRowKeys={expandedRows}
                  onChange={(pageination) => this.handlePagination(pageination)}
                  pagination={{ current: currentPage }}
                  expandedRowRender={(record) => (
                    <div style={{ margin: 0 }}>
                      <table
                        className="table"
                        style={{
                          width: '350px',
                          padding: '0',
                          marginBottom: '20px',
                        }}
                      >
                        <thead>
                          <tr style={{ background: 'transparent' }}>
                            <td className="table--title table--title-left">Level</td>
                            <td className="table--title table--title-right">Price</td>
                            <td className="table--title table--title-right">Available</td>
                            <td className="table--title table--title-right">Edit</td>
                          </tr>
                        </thead>
                        <tbody>
                          {record.levelPrices.map((levelPrice) => (
                            <tr
                              style={{
                                background: 'transparent',
                              }}
                              className="table__item"
                              key={levelPrice.id}
                            >
                              <th className="table__item--text table__item--text-left">{levelPrice.level.name}</th>
                              <th className="table__item--text  table__item--text-right">
                                {`Rp. ${formatters.price(levelPrice.price)}`}
                              </th>
                              <th className="table__item--text  table__item--text-right">
                                {levelPrice.isAvailable ? (
                                  <Tag color="green" style={{ margin: '0' }}>
                                    Available
                                  </Tag>
                                ) : (
                                  <Tag color="red" style={{ margin: '0' }}>
                                    Unavailable
                                  </Tag>
                                )}
                              </th>
                              <th className="table__item--text table__item--text-right">
                                <Link to={`/admin/edit-level-price/${record.id}/${levelPrice.id}`}>
                                  <span style={{ cursor: 'pointer' }}>
                                    <img src={Edit} style={{ marginRight: '10px' }} width="20px" alt="edit" />
                                  </span>
                                </Link>
                                <button
                                  type="button"
                                  style={{ cursor: 'pointer', border: 'none' }}
                                  // record stores info of  current row's product
                                  onClick={() => showDeletePrice(this.props, levelPrice.id, record.id)}
                                >
                                  <img src={TrashCan} width="20px" alt="edit" />
                                </button>
                              </th>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                      <Link to={`/admin/add-price/${record.id}`}>
                        <Button bg="#2e6f84" color="#fff">
                          Add Price
                        </Button>
                      </Link>
                    </div>
                  )}
                >
                  <Column
                    align="left"
                    dataIndex="name"
                    title="Name"
                    key="name"
                    render={(name) => <span>{name}</span>}
                  />
                  <Column
                    align="center"
                    dataIndex="description"
                    width="400px"
                    title="Description"
                    key="description"
                    render={(description) => <span>{description}</span>}
                  />
                  <Column
                    align="center"
                    dataIndex="quantity"
                    title="Quantity"
                    key="quantity"
                    render={(qty) => <span>{qty}</span>}
                  />
                  <Column
                    align="right"
                    title="Edit"
                    dataIndex="id"
                    key="edit"
                    render={(id) => (
                      <div>
                        <span>
                          <Link to={`/admin/edit-product/${id}`}>
                            <img src={Edit} width="25px" alt="edit" style={{ marginRight: '10px' }} />
                          </Link>
                        </span>
                        <button
                          type="button"
                          onClick={() => showDeleteProduct(this.props, id)}
                          style={{ cursor: 'pointer', border: 'none' }}
                        >
                          <img src={TrashCan} width="20px" alt="edit" />
                        </button>
                      </div>
                    )}
                  />
                </Table>
              </>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default withApollo(Inventory);
