import React, { Component } from 'react';
import '../../css/BestSeller.css';
import moment from 'moment';
import { Card, List, Popover, Skeleton } from 'antd';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_BEST_SELLER } from '../graphql/queries';

// Utils
import { formatters, constants } from '../../utils';

// Component
import Error from '../commons/Error';

class BestSeller extends Component {
  constructor() {
    super();
    this.state = {
      tab: 'Day',
      startDate: moment()
        .startOf('Day')
        .toDate(),
      endDate: moment()
        .endOf('Day')
        .toDate(),
    };
  }

  onTabChange = (key, refetch) => {
    this.setState(
      {
        tab: key,
        startDate: moment()
          .startOf(key)
          .toDate(),
        endDate: moment()
          .endOf(key)
          .toDate(),
      },
      () => {
        const { startDate, endDate } = this.state;
        refetch({ startDate, endDate });
      }
    );
  };

  renderList = (item, i) => {
    return (
      <Popover
        placement="right"
        content={item.levels.map((level) => (
          <p key={level.levelName} style={{ fontWeight: 'bold' }}>
            Level:<span style={{ marginRight: '10px' }}>{level.levelName}</span>Quantity: {level.total}
          </p>
        ))}
      >
        <p className="BestSeller__item">
          <span style={{ whiteSpace: 'pre' }}>{`${i + 1}. ${item.productName} - Total: ${item.total}`}</span>
          <span className="BestSeller__item--arrow">&#8594;</span>
        </p>
      </Popover>
    );
  };

  render() {
    const { startDate, endDate, tab } = this.state;
    return (
      <Query
        query={FETCH_BEST_SELLER}
        variables={{
          startDate,
          endDate,
        }}
      >
        {({ loading, error, data, refetch }) => {
          if (loading)
            return (
              <Card
                style={{ width: '45%' }}
                title="Top Selling Product"
                extra={`${formatters.datetime(startDate, 'id').slice(0, 10)} ~ ${formatters
                  .datetime(endDate, 'id')
                  .slice(0, 10)}`}
                tabList={constants.DASHBOARD_TAB}
                activeTabKey={tab}
              >
                <Skeleton active />
              </Card>
            );
          if (error)
            return (
              <Card style={{ width: '45%' }}>
                <Error />
              </Card>
            );
          return (
            <Card
              style={{ width: '45%' }}
              title="Top Selling Product"
              extra={`${formatters.datetime(startDate, 'id').slice(0, 10)} ~ ${formatters
                .datetime(endDate, 'id')
                .slice(0, 10)}`}
              tabList={constants.DASHBOARD_TAB}
              activeTabKey={tab}
              onTabChange={(key) => {
                this.onTabChange(key, refetch);
              }}
            >
              <List
                style={{ border: 'none' }}
                size="large"
                bordered
                dataSource={data.bestSellerList.slice(0, 10)}
                renderItem={(item, i) => (
                  <li style={{ borderBottom: '1px solid #e8e8e8', padding: '20px 0', textAlign: 'center' }}>
                    {this.renderList(item, i)}
                  </li>
                )}
              />
            </Card>
          );
        }}
      </Query>
    );
  }
}

export default withApollo(BestSeller);
