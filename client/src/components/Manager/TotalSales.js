import React, { Component } from 'react';
import '../../css/TotalSales.css';
import moment from 'moment';
import { Card, Skeleton } from 'antd';

// GraphQl
import { Query, withApollo } from 'react-apollo';
import { FETCH_TOTAL_SALES } from '../graphql/queries';

// Utils
import { formatters, helpers, constants } from '../../utils';

// Component
import Error from '../commons/Error';

class TotalSales extends Component {
  constructor() {
    super();
    this.state = {
      tab: 'Day',
      startDate: moment()
        .startOf('Day')
        .toDate(),
      endDate: moment()
        .endOf('Day')
        .toDate(),
      curSales: '',
      pastSales: '',
    };
  }

  componentDidMount() {
    // Get past sales from yesterday
    const { client } = this.props;
    client
      .query({
        query: FETCH_TOTAL_SALES,
        variables: {
          startDate: moment()
            .subtract(1, 'days')
            .startOf('Day')
            .toDate(),
          endDate: moment()
            .subtract(1, 'days')
            .endOf('Day')
            .toDate(),
        },
      })
      .then((res) => {
        this.setState({ pastSales: res.data.totalSales });
      });
  }

  onTabChange = (key, refetch) => {
    // Refetch current sales when tab is clicked
    this.setState(
      {
        tab: key,
        startDate: moment()
          .startOf(key)
          .toDate(),
        endDate: moment()
          .endOf(key)
          .toDate(),
      },
      () => {
        const { startDate, endDate } = this.state;
        refetch({ startDate, endDate }).then((res) => this.setState({ curSales: res.data.totalSales }));
      }
    );

    // Get past sales
    const { client } = this.props;
    client
      .query({
        query: FETCH_TOTAL_SALES,
        variables: {
          startDate: moment()
            .subtract(1, key)
            .startOf(key)
            .toDate(),
          endDate: moment()
            .subtract(1, key)
            .endOf(key)
            .toDate(),
        },
      })
      .then((res) => {
        this.setState({ pastSales: res.data.totalSales });
      });
  };

  renderTotalSales = (totalSales, from, now, curSales, pastSales) => {
    return (
      <>
        <p className="TotalSales--title">Total Sales</p>
        <p className="TotalSales--num">
          {`Rp. ${formatters.price(totalSales)}`}
          <span className="TotalSales--growth">
            {helpers.growthRate(curSales, pastSales) === 'Infinity' && null}
            {helpers.growthRate(curSales, pastSales) > 0 && helpers.growthRate(curSales, pastSales) !== 'Infinity' && (
              <span className="TotalSales--growth-desc">
                {`(+${(((curSales - pastSales) / pastSales) * 100).toFixed(2)}%)`}
              </span>
            )}
            {helpers.growthRate(curSales, pastSales) < 0 && (
              <span className="TotalSales--growth-asc">
                {`(${(((curSales - pastSales) / pastSales) * 100).toFixed(2)}%)`}
              </span>
            )}
          </span>
        </p>

        <p className="TotalSales--date">
          {`${formatters.datetime(from, 'id').slice(0, 10)} ~ ${formatters.datetime(now, 'id').slice(0, 10)}`}
        </p>
      </>
    );
  };

  render() {
    const { tab, startDate, endDate, curSales, pastSales } = this.state;
    return (
      <Query
        query={FETCH_TOTAL_SALES}
        variables={{ startDate, endDate }}
        onCompleted={(data) => {
          this.setState({ curSales: data.totalSales });
        }}
      >
        {({ loading, error, data, refetch }) => {
          if (loading)
            return (
              <Card
                style={{ width: '50%', borderRadius: '5px', height: '270px' }}
                tabList={constants.DASHBOARD_TAB}
                activeTabKey={tab}
              >
                <Skeleton active />
              </Card>
            );
          if (error)
            return (
              <Card style={{ width: '50%' }}>
                <Error />
              </Card>
            );
          return (
            <Card
              style={{ width: '50%', borderRadius: '5px', height: '270px' }}
              tabList={constants.DASHBOARD_TAB}
              activeTabKey={tab}
              onTabChange={(key) => {
                this.onTabChange(key, refetch);
              }}
            >
              {this.renderTotalSales(data.totalSales, startDate, endDate, curSales, pastSales)}
            </Card>
          );
        }}
      </Query>
    );
  }
}

export default withApollo(TotalSales);
