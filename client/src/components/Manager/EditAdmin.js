import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Input } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { FETCH_ADMINS, FETCH_ADMIN } from '../graphql/queries';
import { UPDATE_ADMIN } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

class EditAdmin extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      username: '',
      password: '',
      email: '',
    };
  }

  setFormValue = (data) => {
    try {
      const { name, username, email } = data.admin;
      this.setState({
        name,
        username,
        email,
      });
    } catch (e) {
      const { history } = this.props;
      history.push('/admin/admins');
    }
  };

  render() {
    const {
      match: {
        params: { adminId },
      },
    } = this.props;
    const { name, password, email, username } = this.state;
    return (
      <FormContainer header="Edit Admin">
        <Query query={FETCH_ADMIN} variables={{ id: adminId }} onCompleted={(data) => this.setFormValue(data)}>
          {({ loading, error }) => {
            if (loading) return <div>loading</div>;
            if (error) return <div>error</div>;
            return (
              <>
                <Form.Item label="Name" colon={false}>
                  <Input value={name} onChange={(e) => this.setState({ name: e.target.value })} />
                </Form.Item>
                <Form.Item label="Username" colon={false}>
                  <Input value={username} />
                </Form.Item>
                <Form.Item label="Email" colon={false}>
                  <Input value={email} onChange={(e) => this.setState({ email: e.target.value })} />
                </Form.Item>
              </>
            );
          }}
        </Query>
        <Link to={`/admin/edit-admin-password/${adminId}`}>
          <p
            style={{
              color: '#666',
              textDecoration: 'underline',
              display: 'inline-block',
            }}
          >
            Change password
          </p>
        </Link>
        <Mutation
          mutation={UPDATE_ADMIN}
          variables={{
            id: adminId,
            name,
            password,
            email,
          }}
          update={(store, { data: { updateAdmin } }) => {
            const data = store.readQuery({
              query: FETCH_ADMINS,
              variables: {
                id: adminId,
                name,
                username,
                email,
              },
            });

            const editedAdmin = data.admins.find((admin) => admin.id === adminId);
            editedAdmin.name = updateAdmin.name;
            editedAdmin.email = updateAdmin.email;

            store.writeQuery({
              query: FETCH_ADMINS,
              variables: {
                id: adminId,
                name,
                username,
                email,
              },
              data,
            });
          }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/admins');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(updateAdmin) => <FormButton onClick={updateAdmin}>Confirm</FormButton>}
        </Mutation>
      </FormContainer>
    );
  }
}

EditAdmin.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default EditAdmin;
