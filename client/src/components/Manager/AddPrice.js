import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Select, Checkbox } from 'antd';

// GraphQL
import { Mutation, withApollo } from 'react-apollo';
import { FETCH_PRODUCTS, FETCH_LEVELS_AND_ADMIN_PRODUCT } from '../graphql/queries';
import { CREATE_LEVEL_PRICE } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

const { Option } = Select;

class AddPrice extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      inputPrice: '',
      levelId: '',
      levels: null,
      isAvailable: false,
    };
  }

  componentDidMount() {
    const {
      match: {
        params: { productId },
      },
    } = this.props;
    const { client } = this.props;
    client
      .query({
        query: FETCH_LEVELS_AND_ADMIN_PRODUCT,
        variables: {
          productId,
        },
      })
      .then((res) => {
        const { adminProduct } = res.data;
        const { levels } = res.data;
        const levelObj = {};
        const filteredLevels = [];
        // Convert exsting level to obj
        adminProduct.levelPrices.forEach((levelPrice) => {
          if (!levelObj[levelPrice.level.id]) {
            levelObj[levelPrice.level.id] = 1;
          }
        });
        // Loop all level type to check if it already exists, if not it will be pushed to a new arry named filteredLevels
        levels.forEach((level) => {
          if (!levelObj[level.id]) {
            filteredLevels.push(level);
          }
        });
        this.setState({
          name: adminProduct.name,
          levels: filteredLevels,
          levelId: filteredLevels.length !== 0 ? filteredLevels[0].id : '',
        });
      })
      .catch(() => {
        const { history } = this.props;
        history.push('/admin/inventory');
      });
  }

  render() {
    const {
      match: {
        params: { productId },
      },
    } = this.props;
    const { name, levelId, levels, inputPrice, isAvailable } = this.state;
    const price = parseInt(inputPrice, 10);
    return (
      <FormContainer header="Add Price">
        <p
          style={{
            fontSize: '18px',
            fontWeight: 'bold',
            marginBottom: '24px',
          }}
        >
          {`Name:${name}`}
        </p>
        <Form.Item label="Level" colon={false}>
          <Select value={levelId} onChange={(value) => this.setState({ levelId: value })}>
            {levels &&
              levels.map((level) => (
                <Option value={level.id} key={level.id}>
                  {level.name}
                </Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item required label="Price" colon={false} validateStatus={inputPrice ? 'success' : 'error'}>
          <Input type="number" onChange={(e) => this.setState({ inputPrice: e.target.value })} value={inputPrice} />
        </Form.Item>
        <span style={{ display: 'block', fontSize: '14px', fontWeight: 'bold', color: '#999', marginBottom: ' 20px ' }}>
          <Checkbox checked={isAvailable} onChange={(e) => this.setState({ isAvailable: e.target.checked })} />
          <span style={{ marginLeft: '10px' }}>Product is available</span>
        </span>
        <Mutation
          mutation={CREATE_LEVEL_PRICE}
          variables={{ productId, levelId, price, isAvailable }}
          update={(store, { data: { createLevelPrice } }) => {
            const data = store.readQuery({
              query: FETCH_PRODUCTS,
              variables: {
                productId,
                levelId,
                price,
                isAvailable,
              },
            });
            const findProduct = data.adminProducts.find((adminProduct) => adminProduct.id === productId);
            findProduct.levelPrices.push(createLevelPrice);
            store.writeQuery({
              query: FETCH_PRODUCTS,
              data,
              variables: {
                productId,
                levelId,
                price,
                isAvailable,
              },
            });
          }}
          onCompleted={(data) => {
            // Remove used level and update levels
            const updatedLevels = levels.filter((level) => level.id !== data.createLevelPrice.level.id);
            this.setState({ levels: updatedLevels });
            const { history } = this.props;
            history.push('/admin/inventory');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(createLevelPrice) => {
            if (!price) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={createLevelPrice}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

AddPrice.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withApollo(AddPrice);
