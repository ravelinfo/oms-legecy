import React from 'react';
import '../../css/commons/Oms.css';
import '../../css/commons/AntTable.css';
import { Layout, Dropdown } from 'antd';
import { Switch, Route, useRouteMatch, Link, Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';

// GraphQL
import { Query } from 'react-apollo';
import { ADMIN_ME, FETCH_ADMIN_NOTIFICATIONS } from '../graphql/queries';

// Icons
import Signout from '../../assets/icons/sign-out.svg';
import Customer from '../../assets/icons/customer.svg';
import InventoryIcon from '../../assets/icons/inventory.svg';
import Clipboard from '../../assets/icons/list-all.svg';
import UserConfig from '../../assets/icons/user-config.svg';
import Admin from '../../assets/icons/admin.svg';
import Spinner from '../../assets/icons/spinner.svg';
import Bell from '../../assets/icons/bell.svg';

// Components
import Level from './Level';
import PastOrders from './PastOrders';
import OrderDetail from './OrderDetail';
import Admins from './Admins';
import Customers from './Customers';
import Inventory from './Inventory';
import Orders from './Orders';
import Setting from './Setting';
import AddCustomer from './AddCustomer';
import AddPrice from './AddPrice';
import AddLevel from './AddLevel';
import AddAdmin from './AddAdmin';
import AddProduct from './AddProduct';
import EditAdmin from './EditAdmin';
import EditAccount from './EditAccount';
import EditProduct from './EditProduct';
import EditLevel from './EditLevel';
import EditLevelPrice from './EditLevelPrice';
import EditSettingPassword from './EditSettingPassword';
import EditAdminPassword from './EditAdminPassword';
import EditCustomerPassword from './EditCustomerPassword';
import NotificationList from '../commons/NotificationList';
import Dashboard from './Dashboard';
import version from '../../utils/version';

const { Header, Sider, Content } = Layout;

const renderLinks = (url) => {
  const links = [
    { title: 'Admins', icon: Admin },
    { title: 'Customers', icon: Customer },
    { title: 'Inventory', icon: InventoryIcon },
    { title: 'Orders', icon: Clipboard },
    { title: 'Setting', icon: UserConfig },
  ];
  return links.map((link) => (
    <li className="sider__nav--link" key={link.title}>
      <Link to={`${url}/${link.title.toLowerCase()}`}>
        <img className="sider__nav--icon" src={link.icon} alt={link.title} />
        {link.title}
      </Link>
    </li>
  ));
};

const Main = () => {
  const { path, url } = useRouteMatch();
  return (
    <div className="Oms">
      <img className="spinner" src={Spinner} alt="loading" />
      <Layout id="layout" style={{ display: 'none' }}>
        <Sider className="Oms__sider" width="280px">
          <div className="sider__top">
            <span className="sider__top--link">
              <Link to={`${url}`}>OMS</Link>
            </span>
          </div>
          <div className="sider__bottom">
            <ul className="sider__nav">{renderLinks(url)}</ul>
          </div>
          <p className="sider__version">Powered by Ravel Info - v{version}</p>
        </Sider>
        <Layout>
          <Header className="Oms__header">
            <div className="header__container">
              <Query
                query={ADMIN_ME}
                onCompleted={() => {
                  // Get elements
                  const layout = document.getElementById('layout');
                  const spinner = document.querySelector('.spinner');
                  // Hide spinner and show app when data is compeletely loaded
                  layout.style.display = 'flex';
                  spinner.style.display = 'none';
                }}
              >
                {({ loading, error, data }) => {
                  if (loading) return <span>Loading</span>;
                  if (error) return <Redirect to="/admin-login" />;
                  const { name } = data.adminMe;
                  return <span>{name}</span>;
                }}
              </Query>
              <div className="header__ntf">
                <Query query={FETCH_ADMIN_NOTIFICATIONS}>
                  {({ loading, error, data }) => {
                    if (loading || error) return <img src={Bell} height="27px" alt="bell" />;

                    return (
                      <Dropdown
                        overlay={<NotificationList user="admin" />}
                        overlayStyle={{ width: '400px' }}
                        trigger={['click']}
                      >
                        <div>
                          <img src={Bell} height="27px" alt="bell" />
                          <div className="ntf--badge">
                            <span className="ntf--num">{data.adminNotifications.length}</span>
                          </div>
                        </div>
                      </Dropdown>
                    );
                  }}
                </Query>
              </div>
              <span style={{ marginLeft: '20px' }}>
                <Link
                  to="/admin-login"
                  onClick={() => {
                    Cookies.remove('token', {
                      path: '/',
                    });
                  }}
                >
                  Signout
                </Link>
                <img className="logout-icon" src={Signout} alt="signout" />
              </span>
            </div>
          </Header>
          <Content className="Oms__content">
            <Switch>
              <Route exact path={`${path}`} component={Dashboard} /> {/* Remove dashboard temporarily for v1.3 */}
              <Route path={`${path}/level`} component={Level} />
              <Route path={`${path}/admins`} component={Admins} />
              <Route path={`${path}/customers`} component={Customers} />
              <Route path={`${path}/inventory`} component={Inventory} />
              <Route path={`${path}/orders`} component={Orders} />
              <Route path={`${path}/order/:orderId`} component={OrderDetail} />
              <Route path={`${path}/past-orders`} component={PastOrders} />
              <Route path={`${path}/setting`} component={Setting} />
              <Route path={`${path}/add-admin`} component={AddAdmin} />
              <Route path={`${path}/add-customer`} component={AddCustomer} />
              <Route path={`${path}/add-level`} component={AddLevel} />
              <Route path={`${path}/add-product`} component={AddProduct} />
              <Route path={`${path}/add-price/:productId`} component={AddPrice} />
              <Route path={`${path}/edit-account/:accountId`} component={EditAccount} />
              <Route path={`${path}/edit-level/:levelId`} component={EditLevel} />
              <Route path={`${path}/edit-product/:productId`} component={EditProduct} />
              <Route path={`${path}/edit-admin/:adminId`} component={EditAdmin} />
              <Route path={`${path}/edit-level-price/:productId/:levelPriceId`} component={EditLevelPrice} />
              <Route path={`${path}/edit-setting-password/:adminId`} component={EditSettingPassword} />
              <Route path={`${path}/edit-admin-password/:adminId`} component={EditAdminPassword} />
              <Route path={`${path}/edit-customer-password/:customerId`} component={EditCustomerPassword} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default Main;
