import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// GraphQL
import { Mutation, withApollo } from 'react-apollo';
import { gql } from 'apollo-boost';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

export const UPDATE_CUSTOMER_PASSWORD = gql`
  mutation UpdateCustomer($id: ID!, $password: String) {
    updateCustomer(id: $id, password: $password) {
      id
      username
      company
      email
      level {
        id
        name
      }
    }
  }
`;

class EditCustomerPassword extends Component {
  constructor() {
    super();
    this.state = {
      password: '',
    };
  }

  render() {
    const {
      match: {
        params: { customerId },
      },
    } = this.props;
    const { password } = this.state;
    return (
      <FormContainer header="Change password">
        <Form.Item label="New password" colon={false} required validateStatus={password ? 'success' : 'error'}>
          <Input.Password value={password} onChange={(e) => this.setState({ password: e.target.value })} />
        </Form.Item>
        <Mutation
          mutation={UPDATE_CUSTOMER_PASSWORD}
          variables={{ id: customerId, password }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/customers');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(updateCustomer) => {
            if (!password) return <FormButton disabled>Save</FormButton>;
            return <FormButton onClick={updateCustomer}>Save</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

EditCustomerPassword.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withApollo(EditCustomerPassword);
