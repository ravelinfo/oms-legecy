import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { FETCH_PRODUCT } from '../graphql/queries';
import { UPDATE_PRODUCT } from '../graphql/mutations';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';
import { errors } from '../../utils';

class EditProduct extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      description: '',
      inputQuantity: null,
    };
  }

  setFormValue(data) {
    try {
      const { name, description, quantity } = data.adminProduct;
      this.setState({ name, description, inputQuantity: quantity });
    } catch (e) {
      const { history } = this.props;
      history.push('/admin/inventory');
    }
  }

  render() {
    const { name, description, inputQuantity } = this.state;
    const quantity = parseInt(inputQuantity, 10);
    const {
      match: {
        params: { productId },
      },
    } = this.props;
    return (
      <FormContainer header="Edit Product">
        <Query query={FETCH_PRODUCT} variables={{ productId }} onCompleted={(data) => this.setFormValue(data)}>
          {({ loading, error }) => {
            if (loading) return <div>loading</div>;
            if (error) return <div>error</div>;
            return (
              <>
                <Form.Item label="Item" colon={false} required validateStatus={name ? 'success' : 'error'}>
                  <Input value={name} onChange={(e) => this.setState({ name: e.target.value })} />
                </Form.Item>
                <Form.Item label="Description" colon={false}>
                  <Input value={description} onChange={(e) => this.setState({ description: e.target.value })} />
                </Form.Item>
                <Form.Item label="Quantity" colon={false}>
                  <Input
                    type="number"
                    value={inputQuantity}
                    onChange={(e) => this.setState({ inputQuantity: e.target.value })}
                  />
                </Form.Item>
              </>
            );
          }}
        </Query>

        <Mutation
          mutation={UPDATE_PRODUCT}
          variables={{ productId, name, description, quantity }}
          onError={(error) => {
            // const { graphQLErrors } = error;
            // if (
            //   graphQLErrors[0].message ===
            //   'A unique constraint would be violated on Product. Details: Field name = name'
            // ) {
            //   message.warning('This item already exists!');
            // } else if (
            //   graphQLErrors[0].message === `No Node for the model Product with value ${productId} for id found.`
            // ) {
            //   message.warning("This product doesn't exist!");
            // }
            errors.handleError(error);
          }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/inventory');
          }}
        >
          {(updateProduct) => {
            if (!name) {
              return <FormButton disabled>Confirm</FormButton>;
            }
            return <FormButton onClick={updateProduct}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

EditProduct.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default EditProduct;
