import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, message } from 'antd';

// GraphQL
import { Mutation } from 'react-apollo';
import { FETCH_PRODUCTS } from '../graphql/queries';
import { CREATE_PRODUCT } from '../graphql/mutations';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

class AddProduct extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      description: '',
      inputQuantity: '',
    };
  }

  render() {
    const { name, description, inputQuantity } = this.state;
    const quantity = parseInt(inputQuantity, 10);
    return (
      <FormContainer header="Add Product">
        <Form.Item label="Name" colon={false} required>
          <Input value={name} onChange={(e) => this.setState({ name: e.target.value })} />
        </Form.Item>
        <Form.Item label="Description" colon={false}>
          <Input value={description} onChange={(e) => this.setState({ description: e.target.value })} />
        </Form.Item>
        <Form.Item label="Quantity" colon={false} required>
          <Input
            type="number"
            value={inputQuantity}
            onChange={(e) => this.setState({ inputQuantity: e.target.value })}
          />
        </Form.Item>
        <Mutation
          mutation={CREATE_PRODUCT}
          variables={{ name, description, quantity }}
          update={(store, { data: { createProduct } }) => {
            const data = store.readQuery({
              query: FETCH_PRODUCTS,
              variables: {
                id: createProduct.id,
                name,
                description,
                quantity,
              },
            });
            data.adminProducts.unshift(createProduct);

            store.writeQuery({
              query: FETCH_PRODUCTS,
              variables: {
                id: createProduct.id,
                name,
                description,
                quantity,
              },
              data,
            });
          }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/inventory');
          }}
          onError={(err) => {
            const { graphQLErrors } = err;
            if (
              graphQLErrors[0].message ===
              'A unique constraint would be violated on Product. Details: Field name = name'
            ) {
              message.warning('This product already exists!');
            }
          }}
        >
          {(createProduct) => {
            if (!name || !quantity) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={createProduct}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

AddProduct.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default AddProduct;
