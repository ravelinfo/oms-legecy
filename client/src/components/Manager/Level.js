import React from 'react';
import { Table, Modal } from 'antd';
import { Link } from 'react-router-dom';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_LEVELS } from '../graphql/queries';
import { DELETE_LEVEL } from '../graphql/mutations';

// Icons
import Edit from '../../assets/icons/edit.svg';
import TrashCan from '../../assets/icons/trash-can.svg';

// Compoenent
import Button from '../commons/Button';

const { Column } = Table;
const { confirm } = Modal;

const showConfirm = (props, id) => {
  confirm({
    title: 'Are you sure to delete this level?',
    onOk() {
      props.client
        .mutate({
          mutation: DELETE_LEVEL,
          variables: { id },
          update: (store, { data: { deleteLevel } }) => {
            const data = store.readQuery({
              query: FETCH_LEVELS,
              variables: { id },
            });
            const updatedLevels = data.levels.filter((level) => {
              return level.id !== deleteLevel.id;
            });
            data.levels = updatedLevels;

            store.writeQuery({
              query: FETCH_LEVELS,
              variables: { id },
              data,
            });
          },
        })
        .then((res) => res)
        .catch(() => window.location.reload());
    },
    onCancel() {},
  });
};

const Level = (props) => {
  return (
    <div style={{ width: '30%', margin: '20px auto' }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: '20px',
        }}
      >
        <Link to="/admin/add-level">
          <Button bg="transparent" bd="2px solid #2e6f84" color="#2e6f84" mr="0 10px 0 0">
            Add Level
          </Button>
        </Link>
      </div>
      <Query query={FETCH_LEVELS}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading</div>;
          if (error) return <div>{error.message}</div>;

          const { levels } = data;
          return (
            <Table dataSource={levels} rowKey={(level) => level.id}>
              <Column align="left" dataIndex="name" title="Level" key="name" render={(name) => <span>{name}</span>} />
              <Column
                align="right"
                title="Edit"
                key="edit"
                dataIndex="id"
                render={(id) => (
                  <div>
                    <span>
                      <Link to={`/admin/edit-level/${id}`}>
                        <img src={Edit} width="25px" alt="edit" style={{ marginRight: '10px' }} />
                      </Link>
                    </span>
                    <button
                      type="button"
                      onClick={() => showConfirm(props, id)}
                      style={{ cursor: 'pointer', border: 'none' }}
                    >
                      <img src={TrashCan} width="20px" alt="delete" />
                    </button>
                  </div>
                )}
              />
            </Table>
          );
        }}
      </Query>
    </div>
  );
};

export default withApollo(Level);
