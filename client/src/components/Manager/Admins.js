import React from 'react';
import { Table, Modal, Skeleton } from 'antd';
import { Link } from 'react-router-dom';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_ADMINS } from '../graphql/queries';
import { DELETE_ADMIN } from '../graphql/mutations';

// Icons
import UserEdit from '../../assets/icons/user-edit.svg';
import TrashCan from '../../assets/icons/trash-can.svg';

// Component
import Button from '../commons/Button';
import Error from '../commons/Error';

const { Column } = Table;
const { confirm } = Modal;

const showConfirm = (props, id) => {
  confirm({
    title: 'Are you sure to delete this account?',
    okText: 'Yes',
    onOk() {
      props.client
        .mutate({
          mutation: DELETE_ADMIN,
          variables: { id },
          update: (store, { data: { deleteAdmin } }) => {
            const data = store.readQuery({
              query: FETCH_ADMINS,
              variables: { id },
            });
            const updatedAdmins = data.admins.filter((admin) => admin.id !== deleteAdmin.id);
            data.admins = updatedAdmins;
            store.writeQuery({
              query: FETCH_ADMINS,
              variables: { id },
              data,
            });
          },
        })
        .then((res) => res)
        .catch((err) => {
          const { graphQLErrors } = err;
          if (graphQLErrors[0].message === 'ADM_NOT_EXIST') {
            window.location.reload();
          }
        });
    },
    onCancel() {},
  });
};

const Admins = (props) => {
  return (
    <div style={{ width: '90%', margin: '20px auto' }}>
      <Query query={FETCH_ADMINS}>
        {({ loading, error, data }) => {
          if (loading) return <Skeleton active />;
          if (error) return <Error />;
          const { admins } = data;
          return (
            <>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginBottom: '20px',
                }}
              >
                {/* <input
                placeholder="Search customer"
                style={{ width: '200px', padding: '5px 10px' }}
              /> */}
                <div>
                  <Link to="/admin/add-admin">
                    <Button bg="#2e6f84" color="#fff" bd="2px solid #2e6f84">
                      Create Admin
                    </Button>
                  </Link>
                </div>
              </div>
              <Table dataSource={admins} rowKey={(admin) => admin.id}>
                <Column align="left" dataIndex="name" title="Name" key="name" render={(name) => <span>{name}</span>} />
                <Column
                  align="center"
                  dataIndex="username"
                  title="Username"
                  key="username"
                  render={(username) => <span>{username}</span>}
                />
                <Column
                  align="center"
                  dataIndex="email"
                  title="Email"
                  key="email"
                  render={(email) => <span>{email}</span>}
                />

                <Column
                  align="right"
                  title="Edit"
                  key="edit"
                  dataIndex="id"
                  render={(id) => (
                    <div>
                      <span>
                        <Link to={`/admin/edit-admin/${id}`}>
                          <img src={UserEdit} width="25px" alt="edit" style={{ marginRight: '10px' }} />
                        </Link>
                      </span>
                      <button
                        type="button"
                        onClick={() => showConfirm(props, id)}
                        style={{ cursor: 'pointer', border: 'none' }}
                      >
                        <img src={TrashCan} width="20px" alt="edit" />
                      </button>
                    </div>
                  )}
                />
              </Table>
            </>
          );
        }}
      </Query>
    </div>
  );
};

export default withApollo(Admins);
