import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Input, Select } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { FETCH_CUSTOMERS, FETCH_CUSTOMER, FETCH_LEVELS } from '../graphql/queries';
import { UPDATE_CUSTOMER } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

const { Option } = Select;

class EditAccount extends Component {
  constructor() {
    super();
    this.state = {
      company: '',
      username: '',
      phone: '',
      address: '',
      email: '',
      levelId: '',
    };
  }

  setFormValue = (data) => {
    try {
      const { company, username, phone, address, email } = data.customer;
      this.setState({
        company,
        username,
        phone,
        address,
        email,
        levelId: data.customer.level.id,
      });
    } catch (e) {
      const { history } = this.props;
      history.push('/admin/customers');
    }
  };

  render() {
    const {
      match: {
        params: { accountId },
      },
    } = this.props;
    const { company, username, phone, address, email, levelId } = this.state;
    return (
      <FormContainer header="Edit Account">
        <Query query={FETCH_CUSTOMER} variables={{ id: accountId }} onCompleted={(data) => this.setFormValue(data)}>
          {({ loading, error }) => {
            if (loading) return <div>loading</div>;
            if (error) return <div>error</div>;
            return (
              <>
                <Form.Item label="Company" colon={false} required validateStatus={company ? 'success' : 'error'}>
                  <Input value={company} onChange={(e) => this.setState({ company: e.target.value })} />
                </Form.Item>
                <Form.Item label="Username" colon={false}>
                  <Input value={username} />
                </Form.Item>
                <Form.Item label="Phone" colon={false}>
                  <Input value={phone} onChange={(e) => this.setState({ phone: e.target.value })} />
                </Form.Item>
                <Form.Item label="Address" colon={false}>
                  <Input value={address} onChange={(e) => this.setState({ address: e.target.value })} />
                </Form.Item>
                <Form.Item label="Email" colon={false}>
                  <Input value={email} onChange={(e) => this.setState({ email: e.target.value })} />
                </Form.Item>
              </>
            );
          }}
        </Query>
        <Form.Item label="Level" colon={false}>
          <Query query={FETCH_LEVELS}>
            {({ loading, error, data }) => {
              if (loading) return <div>Loading</div>;
              if (error) return <div>{error.message}</div>;
              const { levels } = data;

              return (
                <Select
                  style={{ width: '100%', fontSize: '16px' }}
                  value={levelId}
                  onChange={(value) => this.setState({ levelId: value })}
                >
                  {levels.map((level) => (
                    <Option value={level.id} key={level.id}>
                      {level.name}
                    </Option>
                  ))}
                </Select>
              );
            }}
          </Query>
        </Form.Item>
        <Link to={`/admin/edit-customer-password/${accountId}`}>
          <p
            style={{
              color: '#666',
              textDecoration: 'underline',
              display: 'inline-block',
            }}
          >
            Change password
          </p>
        </Link>
        <Mutation
          mutation={UPDATE_CUSTOMER}
          variables={{
            id: accountId,
            username,
            phone,
            address,
            email,
            company,
            levelId,
          }}
          update={(store, { data: { updateCustomer } }) => {
            const data = store.readQuery({
              query: FETCH_CUSTOMERS,
              variables: {
                id: accountId,
                username,
                email,
                company,
                level: updateCustomer.level,
              },
            });

            const editedCustomer = data.customers.find((customer) => customer.id === accountId);
            editedCustomer.phone = updateCustomer.phone;
            editedCustomer.address = updateCustomer.address;
            editedCustomer.eamil = updateCustomer.email;
            editedCustomer.company = updateCustomer.company;
            editedCustomer.level.id = updateCustomer.level.id;
            editedCustomer.level.name = updateCustomer.level.name;

            store.writeQuery({
              query: FETCH_CUSTOMERS,
              variables: {
                id: accountId,
                username,
                email,
                company,
                level: updateCustomer.level,
              },
              data,
            });
          }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/customers');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(updateCustomer) => {
            if (!company) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={updateCustomer}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

EditAccount.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default EditAccount;
