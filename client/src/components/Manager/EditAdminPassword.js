import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// GraphQL
import { Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

export const UPDATE_ADMIN_PASSWORD = gql`
  mutation UpdateAdmin($id: ID!, $password: String) {
    updateAdmin(id: $id, password: $password) {
      id
      username
      email
      name
    }
  }
`;

class EditAdminPassword extends Component {
  constructor() {
    super();
    this.state = {
      password: '',
    };
  }

  render() {
    const {
      match: {
        params: { adminId },
      },
    } = this.props;
    const { password } = this.state;
    return (
      <FormContainer header="Change password">
        <Form.Item label="New password" colon={false} required validateStatus={password ? 'success' : 'error'}>
          <Input.Password value={password} onChange={(e) => this.setState({ password: e.target.value })} />
        </Form.Item>
        <Mutation
          mutation={UPDATE_ADMIN_PASSWORD}
          variables={{ id: adminId, password }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/admins');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(updateAdmin) => {
            if (!password) return <FormButton disabled>Save</FormButton>;
            return <FormButton onClick={updateAdmin}>Save</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

EditAdminPassword.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default EditAdminPassword;
