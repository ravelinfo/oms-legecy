import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Select, Alert } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { FETCH_LEVELS, FETCH_CUSTOMERS } from '../graphql/queries';
import { CREATE_CUSTOMER } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

const { Option } = Select;

class AddCustomer extends Component {
  constructor() {
    super();
    this.state = {
      company: '',
      username: '',
      password: '',
      phone: '',
      address: '',
      email: '',
      levelId: '',
    };
  }

  render() {
    const { company, username, password, phone, address, email, levelId } = this.state;
    return (
      <FormContainer header="Create Account">
        <Form.Item label="Company" colon={false} required validateStatus={company ? 'success' : 'error'}>
          <Input onChange={(e) => this.setState({ company: e.target.value })} value={company} />
        </Form.Item>
        <Form.Item required label="Username" colon={false} validateStatus={username ? 'success' : 'error'}>
          <Input required onChange={(e) => this.setState({ username: e.target.value })} value={username} />
        </Form.Item>
        <Form.Item required label="Password" colon={false} validateStatus={password ? 'success' : 'error'}>
          <Input.Password required onChange={(e) => this.setState({ password: e.target.value })} value={password} />
        </Form.Item>
        <Form.Item label="Phone" colon={false}>
          <Input required onChange={(e) => this.setState({ phone: e.target.value })} value={phone} />
        </Form.Item>
        <Form.Item label="Address" colon={false}>
          <Input onChange={(e) => this.setState({ address: e.target.value })} value={address} />
        </Form.Item>
        <Form.Item label="Eamil" colon={false}>
          <Input type="email" onChange={(e) => this.setState({ email: e.target.value })} value={email} />
        </Form.Item>
        <Form.Item label="Level" colon={false}>
          <Query
            query={FETCH_LEVELS}
            onCompleted={(data) => {
              if (data.levels.length !== 0) {
                this.setState({ levelId: data.levels[0].id });
              }
            }}
          >
            {({ loading, error, data }) => {
              if (loading) return <div>Loading</div>;
              if (error) return <div>{error.message}</div>;
              const { levels } = data;

              return (
                <>
                  <Select
                    style={{ width: '100%', fontSize: '16px' }}
                    value={levelId}
                    onChange={(value) => this.setState({ levelId: value })}
                  >
                    {levels.map((level) => (
                      <Option value={level.id} key={level.id}>
                        {level.name}
                      </Option>
                    ))}
                  </Select>
                  {!levelId && <Alert message="Please create a level first" type="error" />}
                </>
              );
            }}
          </Query>
        </Form.Item>
        <Mutation
          mutation={CREATE_CUSTOMER}
          variables={{ company, username, password, phone, address, email, levelId }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/customers');
          }}
          onError={(err) => errors.handleError(err)}
          update={(store, { data: { createCustomer } }) => {
            const data = store.readQuery({
              query: FETCH_CUSTOMERS,
              variables: {
                id: createCustomer.id,
                company,
                username,
                phone,
                address,
                email,
                level: createCustomer.level,
              },
            });
            data.customers.push(createCustomer);
            store.writeQuery({
              query: FETCH_CUSTOMERS,
              variables: {
                id: createCustomer.id,
                company,
                username,
                phone,
                address,
                email,
                level: createCustomer.level,
              },
              data,
            });
          }}
        >
          {(createCustomer) => {
            if (!company || !username || !password || !levelId) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={createCustomer}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

AddCustomer.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default AddCustomer;
