import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../css/OrderDetail.css';
import { Select, Skeleton, message } from 'antd';

// GraphQL
import { Query, Mutation, withApollo } from 'react-apollo';
import { FETCH_ADMIN_ORDER } from '../graphql/queries';
import { UPDATE_ORDER } from '../graphql/mutations';

// Utils
import { constants, formatters, errors } from '../../utils';

// Components
import Button from '../commons/Button';
import OrderMessageForm from './OrderMessageForm';
import OrderMessage from './OrderMessage';
import Error from '../commons/Error';

const { Option } = Select;

class OrderDetail extends Component {
  constructor() {
    super();
    this.state = {
      id: null,
      inputPrice: 0,
      orderStatus: null,
      isDisabled: true,
      originalPrice: 0,
      isLoading: true,
      disableUpdate: false,
      hasError: false,
    };
  }

  componentDidMount() {
    // Fetch order detail
    const {
      match: {
        params: { orderId },
      },
    } = this.props;
    const findLastIndexOfHyphen = orderId.lastIndexOf('-');
    const company = orderId.slice(0, findLastIndexOfHyphen);
    const orderNumber = parseInt(orderId.slice(findLastIndexOfHyphen + 1), 10);
    const { client } = this.props;
    client
      .query({
        query: FETCH_ADMIN_ORDER,
        variables: { company, orderNumber },
      })
      .then((res) => {
        const { adminOrder } = res.data;
        const orerItems = adminOrder.orderItems;
        // Get total price of each product
        const itemPrices = orerItems.map((item) => item.quantity * item.customerProduct.price);
        // Sum price of all products
        const originalPrice = itemPrices.reduce((itemPrice, total) => itemPrice + total);
        this.setState({
          id: adminOrder.id,
          inputPrice: adminOrder.total,
          orderStatus: adminOrder.status,
          originalPrice,
          isLoading: false,
          disableUpdate: adminOrder.status === 'COMPLETED' || adminOrder.status === 'CANCELED',
        });
        this.resizeInput();
      })
      .catch(() => {
        this.setState({ hasError: true, isLoading: false });
      });
  }

  componentDidUpdate() {
    // Resize input width everytime input value changes
    const el = document.querySelector('input[type="text"]');
    if (el) {
      const { inputPrice } = this.state;
      const value = inputPrice;
      const offsetWidth = value.length / 7;
      el.style.width = `${value.length + offsetWidth}ch`;
    }
  }

  resizeInput = () => {
    const el = document.querySelector('input[type="text"]');
    const { value } = el;
    // Check if digits contain comma
    if (value.includes(',')) {
      const offsetWidth = value.replace(/,/g, '').length / 9;
      el.style.width = `${value.length - offsetWidth}ch`;
    } else {
      el.style.width = `${value.length}ch`;
    }
  };

  renderOrderItems = (orderItems) => {
    return (
      <>
        <thead>
          <tr>
            <td className="table--title table--title-left">Product</td>
            <td className="table--title table--title-right">Price</td>
            <td className="table--title">Quantity</td>
            <td className="table--title table--title-right">Total</td>
          </tr>
        </thead>
        <tbody>
          {orderItems.map((item) => (
            <tr className="table__item" key={item.customerProduct.product.name}>
              <th className="table__item--text table__item--text-left">{item.customerProduct.product.name}</th>
              <th className="table__item--text table__item--text-right">
                {`Rp. ${formatters.price(item.customerProduct.price)}`}
              </th>
              <th className="table__item--text">{item.quantity}</th>
              <th className="table__item--text table__item--text-right">
                {`Rp. ${formatters.price(item.quantity * item.customerProduct.price)}`}
              </th>
            </tr>
          ))}
        </tbody>
      </>
    );
  };

  renderOriginalPrice = (originalPrice, totalPrice) => {
    if (originalPrice !== totalPrice) {
      return (
        <p className="table--original-price">
          Original Price :<span className="table--price">{`Rp. ${formatters.price(originalPrice)}`}</span>
        </p>
      );
    }
    return null;
  };

  render() {
    const {
      match: {
        params: { orderId },
      },
    } = this.props;
    const {
      id,
      orderStatus,
      originalPrice,
      isDisabled,
      isLoading,
      inputPrice,
      isEditing,
      disableUpdate,
      hasError,
    } = this.state;
    const findLastIndexOfHyphen = orderId.lastIndexOf('-');
    const company = orderId.slice(0, findLastIndexOfHyphen);
    const orderNumber = parseInt(orderId.slice(findLastIndexOfHyphen + 1), 10);
    const totalPrice = parseInt(inputPrice, 10);
    if (isLoading)
      return (
        <div className="OrderDetail">
          <div className="OrderDetail__left">
            <div className="OrderDetail__card">
              <div className="OrderDetail__card--header">Order details</div>
              <div className="OrderDetail__card card__info">
                <Skeleton active />
              </div>
            </div>
          </div>
        </div>
      );
    if (hasError) return <Error />;
    return (
      <div className="OrderDetail">
        <div className="OrderDetail__left">
          <div className="OrderDetail__card">
            <div className="OrderDetail__card--header">Order details</div>
            <Query query={FETCH_ADMIN_ORDER} variables={{ company, orderNumber }}>
              {({ loading, error, data }) => {
                if (loading) return <div>Lodaing</div>;
                if (error) return <div>Error</div>;
                const { adminOrder } = data;
                return (
                  <>
                    <div className="OrderDetail__card card__info">
                      <div className="OrderDetail__card card__info--left">
                        <span>Order # :</span>
                        <span>Company :</span>
                        <span>Level :</span>
                        <span>Date :</span>
                        <span>Last Updated :</span>
                      </div>
                      <div className="OrderDetail__card card__info--right">
                        <span>{`${orderId}`}</span>
                        <span>{`${company}`}</span>
                        <span>{adminOrder.customer.level.name}</span>
                        <span>{formatters.datetime(adminOrder.createdAt, 'id')}</span>
                        <span>{formatters.datetime(adminOrder.updatedAt, 'id')}</span>
                      </div>
                    </div>
                    <div className="OrderDetail__card--divider" />
                    <div className="OrderDetail__table">
                      <table className="OrderDetail__table table">{this.renderOrderItems(adminOrder.orderItems)}</table>
                      {this.renderOriginalPrice(originalPrice, totalPrice)}
                      <p className="table--total">
                        Order Total :
                        {isEditing ? (
                          <input
                            style={{
                              textAlign: 'right',
                              fontWeight: 'bold',
                              border: 'none',
                            }}
                            type="number"
                            className="table--price"
                            onChange={(e) => this.setState({ inputPrice: e.target.value })}
                            value={inputPrice}
                            onBlur={() =>
                              this.setState({
                                isEditing: false,
                                isDisabled: false,
                              })
                            }
                          />
                        ) : (
                          <span className="table--price">
                            Rp.
                            <input
                              type="text"
                              style={{
                                textAlign: 'right',
                                fontWeight: 'bold',
                                border: 'none',
                              }}
                              value={formatters.price(totalPrice)}
                              onFocus={() => this.setState({ isEditing: true })}
                              readOnly
                            />
                          </span>
                        )}
                      </p>
                    </div>
                  </>
                );
              }}
            </Query>
            <div className="OrderDetail__card--divider" />
            <div className="OrderDetail__status">
              <div className="OrderDetail__status--text">
                <span>Order Status :</span>
                {!disableUpdate ? (
                  <Select
                    style={{ marginLeft: '10px' }}
                    value={orderStatus}
                    onChange={(value) => this.setState({ orderStatus: value, isDisabled: false })}
                  >
                    {Object.keys(constants.ORDER_STATUS).map((key) => (
                      <Option key={constants.ORDER_STATUS[key]} value={key}>
                        {constants.ORDER_STATUS[key]}
                      </Option>
                    ))}
                  </Select>
                ) : (
                  <span style={{ color: '#333', marginLeft: '5px' }}>{constants.PAST_ORDER_STATUS[orderStatus]}</span>
                )}
              </div>
              <Mutation
                mutation={UPDATE_ORDER}
                variables={{ id, totalPrice, orderStatus }}
                onCompleted={(data) => {
                  this.setState({
                    isDisabled: true,
                    disableUpdate: data.updateOrder.status === 'COMPLETED' || data.updateOrder.status === 'CANCELED',
                  });
                  message.success('This order has been updated successfully!');
                }}
                onError={(err) => errors.handleError(err)}
                update={(store, { data: { updateOrder } }) => {
                  const data = store.readQuery({
                    query: FETCH_ADMIN_ORDER,
                    variables: { company, orderNumber },
                  });

                  data.adminOrder.total = updateOrder.total;
                  data.adminOrder.status = updateOrder.status;
                  data.adminOrder.updatedAt = updateOrder.updatedAt;

                  store.writeQuery({
                    query: FETCH_ADMIN_ORDER,
                    data,
                    variables: { company, orderNumber },
                  });
                }}
              >
                {(updateOrder) => {
                  if (!disableUpdate)
                    return (
                      <Button
                        bg={!isDisabled ? '#dd7575' : '#999'}
                        color="#fff"
                        onClick={updateOrder}
                        isDisabled={isDisabled}
                      >
                        Update order
                      </Button>
                    );
                  return null;
                }}
              </Mutation>
            </div>
          </div>
          <div style={{ textAlign: 'right' }}>
            <OrderMessageForm id={id} company={company} orderNumber={orderNumber} />
          </div>
        </div>
        <div className="OrderDetail__right">
          <OrderMessage company={company} orderNumber={orderNumber} />
        </div>
      </div>
    );
  }
}

OrderDetail.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
};

export default withApollo(OrderDetail);
