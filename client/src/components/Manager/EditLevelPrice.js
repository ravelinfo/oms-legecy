import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Input, Checkbox } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { FETCH_LEVEL_PRICE_AND_PRODUCT } from '../graphql/queries';
import { UPDATE_LEVEL_PRICE } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Compoenents
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

class EditLevelPrice extends Component {
  constructor() {
    super();
    this.state = {
      inputPrice: '',
      isAvailable: false,
    };
  }

  render() {
    const {
      match: {
        params: { productId },
      },
    } = this.props;
    const {
      match: {
        params: { levelPriceId },
      },
    } = this.props;
    const { inputPrice, isAvailable } = this.state;
    const price = parseInt(inputPrice, 10);
    return (
      <FormContainer header="Edit Price">
        <Query
          query={FETCH_LEVEL_PRICE_AND_PRODUCT}
          variables={{ productId, levelPriceId }}
          onCompleted={(data) => {
            try {
              this.setState({ inputPrice: data.levelPrice.price, isAvailable: data.levelPrice.isAvailable });
            } catch (e) {
              // Unexpected errors
            }
          }}
        >
          {({ loading, error, data }) => {
            if (loading) return <div>loading</div>;
            if (error) return <div>error</div>;
            try {
              const { level } = data.levelPrice;
              const { name } = data.adminProduct;
              return (
                <>
                  <p style={{ fontWeight: 'bold', fontSize: '18px' }}>
                    Name:
                    {name}
                  </p>
                  <p style={{ fontWeight: 'bold', fontSize: '18px' }}>
                    Level:
                    {level.name}
                  </p>
                  <Form.Item label="Price" colon={false} required validateStatus={inputPrice ? 'success' : 'error'}>
                    <Input
                      type="number"
                      onChange={(e) => this.setState({ inputPrice: e.target.value })}
                      value={inputPrice}
                    />
                  </Form.Item>
                  <span
                    style={{
                      display: 'block',
                      fontSize: '14px',
                      fontWeight: 'bold',
                      color: '#999',
                      marginBottom: ' 20px ',
                    }}
                  >
                    <Checkbox
                      checked={isAvailable}
                      onChange={(e) => this.setState({ isAvailable: e.target.checked })}
                    />
                    <span style={{ marginLeft: '10px' }}>Product is available</span>
                  </span>
                </>
              );
            } catch (e) {
              return <Redirect to="/admin/inventory" />;
            }
          }}
        </Query>
        <Mutation
          mutation={UPDATE_LEVEL_PRICE}
          variables={{ levelPriceId, price, isAvailable }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/inventory');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(updateLevelPrice) => {
            if (!inputPrice) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={updateLevelPrice}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

EditLevelPrice.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default EditLevelPrice;
