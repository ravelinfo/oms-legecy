import React from 'react';
import { Table, Modal, Skeleton } from 'antd';
import { Link } from 'react-router-dom';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_CUSTOMERS } from '../graphql/queries';
import { DELETE_CUSTOMER } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Icons
import UserEdit from '../../assets/icons/user-edit.svg';
import TrashCan from '../../assets/icons/trash-can.svg';

// Component
import Button from '../commons/Button';
import Error from '../commons/Error';

const { Column } = Table;
const { confirm } = Modal;

const showConfirm = (props, id) => {
  confirm({
    title: 'Are you sure to delete this account?',
    okText: 'Yes',
    onOk() {
      props.client
        .mutate({
          mutation: DELETE_CUSTOMER,
          variables: { id },
          update: (store, { data: { deleteCustomer } }) => {
            const data = store.readQuery({
              query: FETCH_CUSTOMERS,
              variables: { id },
            });
            const updatedCustomers = data.customers.filter((customer) => customer.id !== deleteCustomer.id);
            data.customers = updatedCustomers;
            store.writeQuery({
              query: FETCH_CUSTOMERS,
              variables: { id },
              data,
            });
          },
        })
        .then((res) => res)
        .catch((err) => errors.handleError(err));
    },
    onCancel() {},
  });
};

const Customers = (props) => {
  return (
    <div style={{ width: '90%', margin: '20px auto' }}>
      <Query query={FETCH_CUSTOMERS}>
        {({ loading, error, data }) => {
          if (loading) return <Skeleton active />;
          if (error) return <Error />;
          const { customers } = data;
          return (
            <>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginBottom: '20px',
                }}
              >
                {/* <input
            placeholder="Search customer"
            style={{ width: '200px', padding: '5px 10px' }}
          /> */}
                <div>
                  <Link to="/admin/level">
                    <Button bg="transparent" bd="2px solid #2e6f84" color="#2e6f84" mr="0 10px 0 0">
                      Manage Level
                    </Button>
                  </Link>
                  <Link to="/admin/add-customer">
                    <Button bg="#2e6f84" color="#fff" bd="2px solid #2e6f84">
                      Create Account
                    </Button>
                  </Link>
                </div>
              </div>
              <Table dataSource={customers} rowKey={(customer) => customer.id} scroll={{ x: 1400 }}>
                <Column
                  align="left"
                  dataIndex="company"
                  title="Company"
                  key="company"
                  width="150px"
                  fixed="left"
                  render={(company) => <span>{company}</span>}
                />
                <Column
                  align="center"
                  dataIndex="username"
                  title="Username"
                  key="username"
                  width="150px"
                  fixed="left"
                  render={(username) => <span>{username}</span>}
                />
                <Column
                  align="center"
                  dataIndex="level"
                  title="Level"
                  key="level"
                  render={(level) => <span>{level.name}</span>}
                />
                <Column
                  align="center"
                  dataIndex="phone"
                  title="Phone"
                  key="1"
                  width="150px"
                  render={(phone) => <span>{phone}</span>}
                />
                <Column
                  align="center"
                  width="350px"
                  dataIndex="address"
                  title="Address"
                  key="2"
                  render={(address) => <span>{address}</span>}
                />
                <Column
                  align="center"
                  dataIndex="email"
                  title="Email"
                  key="3"
                  width="300px"
                  render={(email) => <span>{email}</span>}
                />
                <Column
                  align="right"
                  title="Edit"
                  key="edit"
                  dataIndex="id"
                  width="150px"
                  fixed="right"
                  render={(id) => (
                    <div>
                      <span>
                        <Link to={`/admin/edit-account/${id}`}>
                          <img src={UserEdit} width="25px" alt="edit" style={{ marginRight: '10px' }} />
                        </Link>
                      </span>
                      <button
                        type="button"
                        onClick={() => showConfirm(props, id)}
                        style={{ cursor: 'pointer', border: 'none' }}
                      >
                        <img src={TrashCan} width="20px" alt="edit" />
                      </button>
                    </div>
                  )}
                />
              </Table>
            </>
          );
        }}
      </Query>
    </div>
  );
};

export default withApollo(Customers);
