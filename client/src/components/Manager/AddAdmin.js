import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// GraphQL
import { Mutation } from 'react-apollo';
import { FETCH_ADMINS } from '../graphql/queries';
import { CREATE_ADMIN } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

class AddAdmin extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      username: '',
      password: '',
      email: '',
    };
  }

  // handleError = (err) => {
  //   const { graphQLErrors } = err;
  //   message.warning(errors[graphQLErrors[0].message]);
  // };

  render() {
    const { name, username, password, email } = this.state;
    return (
      <FormContainer header="Create Admin">
        <Form.Item required label="Name" colon={false} validateStatus={name ? 'success' : 'error'}>
          <Input onChange={(e) => this.setState({ name: e.target.value })} value={name} />
        </Form.Item>
        <Form.Item required label="Username" colon={false} validateStatus={username ? 'success' : 'error'}>
          <Input onChange={(e) => this.setState({ username: e.target.value })} value={username} />
        </Form.Item>
        <Form.Item required label="Password" colon={false} validateStatus={password ? 'success' : 'error'}>
          <Input.Password onChange={(e) => this.setState({ password: e.target.value })} value={password} />
        </Form.Item>
        <Form.Item label="email" colon={false}>
          <Input onChange={(e) => this.setState({ email: e.target.value })} value={email} />
        </Form.Item>
        <Mutation
          mutation={CREATE_ADMIN}
          variables={{ name, username, password, email }}
          update={(store, { data: { createAdmin } }) => {
            const data = store.readQuery({
              query: FETCH_ADMINS,
              variables: {
                id: createAdmin.id,
                username,
                email,
                name,
              },
            });
            data.admins.push(createAdmin);
            store.writeQuery({
              query: FETCH_ADMINS,
              variables: {
                id: createAdmin.id,
                username,
                email,
                name,
              },
              data,
            });
          }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/admins');
          }}
          onError={(err) => errors.handleError(err)}
        >
          {(createAdmin) => {
            if (!name || !username || !password) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={createAdmin}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

AddAdmin.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default AddAdmin;
