import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Badge, Skeleton, Modal } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_ADMIN_ORDERS, FETCH_LEVELS_AND_COMPANIES } from '../graphql/queries';
import { DELETE_ORDER } from '../graphql/mutations';

// Utils
import { constants, formatters, errors, helpers } from '../../utils';

// Component
import Error from '../commons/Error';

// Icons
import TrashCan from '../../assets/icons/trash-can.svg';

const { Column } = Table;
const { confirm } = Modal;

class PastOrders extends Component {
  constructor() {
    super();
    this.state = { levels: [], companies: [] };
  }

  componentDidMount() {
    const { client } = this.props;
    client
      .query({
        query: FETCH_LEVELS_AND_COMPANIES,
      })
      .then((res) => {
        const { levels } = res.data;
        const { customers } = res.data;
        this.setState({ levels, companies: customers });
      })
      .catch((error) => errors.handleError(error));
  }

  onOrderDelete = (props, id) => {
    confirm({
      title: 'Are you sure to delete this order?',
      okText: 'Yes',
      onOk() {
        props.client
          .mutate({
            mutation: DELETE_ORDER,
            variables: { id },
            update: (store, { data: { deleteOrder } }) => {
              const data = store.readQuery({
                query: FETCH_ADMIN_ORDERS,
                variables: { id },
              });
              const updatedOrders = data.adminOrders.filter((order) => order.id !== deleteOrder.id);
              data.adminOrders = updatedOrders;
              store.writeQuery({
                query: FETCH_ADMIN_ORDERS,
                variables: { id },
                data,
              });
            },
          })
          .then((res) => res)
          .catch(() => {
            window.location.reload();
          });
      },
      onCancel() {},
    });
  };

  render() {
    const { levels, companies } = this.state;
    return (
      <div style={{ width: '90%', margin: '20px auto' }}>
        <Query query={FETCH_ADMIN_ORDERS}>
          {({ loading, error, data }) => {
            if (loading) return <Skeleton active />;
            if (error) return <Error />;

            // Get past order list
            const adminOrders = data.adminOrders.filter((order) => {
              if (constants.PAST_ORDER_STATUS[order.status]) {
                return true;
              }
              return false;
            });

            return (
              <Table dataSource={adminOrders} rowKey={(adminOrder) => adminOrder.id}>
                <Column
                  align="left"
                  title="Order #"
                  key="id"
                  sorter={(a, b) => a.orderNumber - b.orderNumber}
                  render={(adminOrder) => (
                    <span
                      style={{
                        fontWeight: 'bold',
                        textDecoration: 'underline',
                      }}
                    >
                      <Link to={`/admin/order/${adminOrder.customer.company}-${adminOrder.orderNumber}`}>
                        {`${adminOrder.customer.company}-${adminOrder.orderNumber}`}
                      </Link>
                    </span>
                  )}
                />
                <Column
                  align="center"
                  dataIndex="customer"
                  title="Company"
                  key="customer"
                  filters={companies.map(({ company }) => ({
                    text: company,
                    value: company,
                  }))}
                  onFilter={(value, record) => value === record.customer.company}
                  render={(customer) => <span>{customer.company}</span>}
                />
                <Column
                  align="right"
                  dataIndex="total"
                  sorter={(a, b) => a.total - b.total}
                  title="Total"
                  key="total"
                  render={(total) => <span>{`Rp. ${formatters.price(total)}`}</span>}
                />
                <Column
                  align="center"
                  title="Level"
                  key="level"
                  filters={levels.map((level) => ({
                    text: level.name,
                    value: level.name,
                  }))}
                  onFilter={(value, record) => value === record.customer.level.name}
                  render={(adminOrder) => <span>{adminOrder.customer.level.name}</span>}
                />
                <Column
                  align="center"
                  dataIndex="updatedAt"
                  title="Last Updated"
                  sorter={(a, b) => moment(a.updatedAt) - moment(b.updatedAt)}
                  defaultSortOrder="descend"
                  key="updatedAt"
                  render={(updatedAt) => <span>{formatters.datetime(updatedAt, 'id')}</span>}
                />
                <Column
                  align="right"
                  dataIndex="status"
                  title="Status"
                  key="status"
                  filters={Object.keys(constants.PAST_ORDER_STATUS).map((key) => ({
                    text: constants.PAST_ORDER_STATUS[key],
                    value: key,
                  }))}
                  onFilter={(value, record) => value === record.status}
                  render={(status) => (
                    <Badge color={helpers.getBadgeColor(status)} text={constants.PAST_ORDER_STATUS[status]} />
                  )}
                />
                <Column
                  align="right"
                  dataIndex="id"
                  key="delete"
                  render={(id) => (
                    <button
                      type="button"
                      onClick={() => this.onOrderDelete(this.props, id)}
                      style={{ cursor: 'pointer', border: 'none' }}
                    >
                      <img src={TrashCan} width="20px" alt="edit" />
                    </button>
                  )}
                />
              </Table>
            );
          }}
        </Query>
      </div>
    );
  }
}

PastOrders.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default withApollo(PastOrders);
