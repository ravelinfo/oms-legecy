import React from 'react';
import TotalSales from './TotalSales';
import BestSeller from './BestSeller';

const Dashboard = () => {
  return (
    <div
      style={{ width: '90%', margin: '20px auto', textAlign: 'left', display: 'flex', justifyContent: 'space-between' }}
    >
      <TotalSales />
      <BestSeller />
    </div>
  );
};

export default Dashboard;
