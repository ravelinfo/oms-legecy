import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

// GraphQL
import { Mutation } from 'react-apollo';
import { FETCH_ADMIN_ORDER } from '../graphql/queries';
import { ADMIN_CREATE_MESSAGE } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Component
import Button from '../commons/Button';

const { TextArea } = Input;

class OrderMessageForm extends Component {
  constructor() {
    super();
    this.state = { content: '' };
  }

  render() {
    const { content } = this.state;
    const { id, company, orderNumber } = this.props;
    return (
      <>
        <TextArea
          rows={4}
          style={{
            margin: '25px 0 10px 0',
            whiteSpace: 'pre-line',
            resize: 'none',
          }}
          value={content}
          onChange={(e) => this.setState({ content: e.target.value })}
        />
        <Mutation
          mutation={ADMIN_CREATE_MESSAGE}
          variables={{ id, content }}
          onCompleted={() => this.setState({ content: '' })}
          onError={(err) => errors.handleError(err)}
          update={(store, { data: { adminCreateMessage } }) => {
            const data = store.readQuery({
              query: FETCH_ADMIN_ORDER,
              variables: { company, orderNumber },
            });

            data.adminOrder.messages.push(adminCreateMessage);

            store.writeQuery({
              query: FETCH_ADMIN_ORDER,
              data,
              variables: { company, orderNumber },
            });
          }}
        >
          {(adminCreateMessage) => (
            <Button bg="#2e6f84" color="#fff" onClick={adminCreateMessage}>
              Send messages
            </Button>
          )}
        </Mutation>
      </>
    );
  }
}

OrderMessageForm.propTypes = {
  id: PropTypes.string.isRequired,
  company: PropTypes.string.isRequired,
  orderNumber: PropTypes.number.isRequired,
};

export default OrderMessageForm;
