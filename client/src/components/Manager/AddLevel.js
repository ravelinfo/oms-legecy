import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

// GraphQL
import { Mutation } from 'react-apollo';
import { FETCH_LEVELS } from '../graphql/queries';
import { CREATE_LEVEL } from '../graphql/mutations';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

class AddLevel extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
    };
  }

  render() {
    const { name } = this.state;
    return (
      <FormContainer header="Add Level" onSubmit={this.onFormSubmit}>
        <Form.Item label="Level" colon={false} required validateStatus={name ? 'success' : 'error'}>
          <Input onChange={(e) => this.setState({ name: e.target.value })} value={name} />
        </Form.Item>
        <Mutation
          mutation={CREATE_LEVEL}
          variables={{ name }}
          update={(store, { data: { createLevel } }) => {
            const data = store.readQuery({ query: FETCH_LEVELS });
            data.levels.push(createLevel);
            store.writeQuery({ query: FETCH_LEVELS, data });
          }}
          onCompleted={() => {
            const { history } = this.props;
            history.push('/admin/level');
          }}
          onError={(error) => errors.handleError(error)}
        >
          {(createLevel) => {
            if (!name) return <FormButton disabled>Confirm</FormButton>;
            return <FormButton onClick={createLevel}>Confirm</FormButton>;
          }}
        </Mutation>
      </FormContainer>
    );
  }
}

AddLevel.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default AddLevel;
