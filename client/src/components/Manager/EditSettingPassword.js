import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, message } from 'antd';

// GraphQL
import { withApollo } from 'react-apollo';
import { gql } from 'apollo-boost';

// Utils
import { errors } from '../../utils';

// Components
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

const ADMIN_UPDATE_MY_PASSWORD = gql`
  mutation AdminUpdateMyPassword($currentPassword: String!, $newPassword: String!) {
    adminUpdateMyPassword(currentPassword: $currentPassword, newPassword: $newPassword) {
      id
      username
      name
      email
    }
  }
`;

class EditSettingPassword extends Component {
  constructor() {
    super();
    this.state = {
      currentPassword: '',
      newPassword: '',
      confirmPassword: '',
    };
  }

  onFormSubmit = (e) => {
    e.preventDefault();
    const { currentPassword, newPassword, confirmPassword } = this.state;
    if (newPassword !== confirmPassword) {
      message.warning('The new password and confirmation password do no match!');
    } else {
      const { client } = this.props;
      client
        .mutate({
          mutation: ADMIN_UPDATE_MY_PASSWORD,
          variables: { currentPassword, newPassword },
        })
        .then(() => {
          const { history } = this.props;
          history.push('/admin');
        })
        .catch((error) => {
          errors.handleError(error);
        });
    }
  };

  render() {
    const { currentPassword, newPassword, confirmPassword } = this.state;
    return (
      <FormContainer header="Change password">
        <Form.Item
          label="Current password"
          colon={false}
          required
          validateStatus={currentPassword ? 'success' : 'error'}
        >
          <Input.Password
            value={currentPassword}
            onChange={(e) => this.setState({ currentPassword: e.target.value })}
          />
        </Form.Item>
        <Form.Item label="New password" colon={false} required validateStatus={newPassword ? 'success' : 'error'}>
          <Input.Password value={newPassword} onChange={(e) => this.setState({ newPassword: e.target.value })} />
        </Form.Item>
        <Form.Item
          label="Confirm new password"
          colon={false}
          required
          validateStatus={confirmPassword ? 'success' : 'error'}
        >
          <Input.Password
            value={confirmPassword}
            onChange={(e) => this.setState({ confirmPassword: e.target.value })}
          />
        </Form.Item>
        {!currentPassword || !newPassword || !confirmPassword ? (
          <FormButton disabled>Save</FormButton>
        ) : (
          <FormButton onClick={this.onFormSubmit}>Save</FormButton>
        )}
      </FormContainer>
    );
  }
}

EditSettingPassword.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withApollo(EditSettingPassword);
