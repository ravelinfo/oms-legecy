import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../css/LoginPage.css';
import { Form, Input } from 'antd';
import Cookies from 'js-cookie';

// GraphQL
import { Mutation, withApollo } from 'react-apollo';
import { ADMIN_ME, CUSTOMER_ME } from './graphql/queries';
import { LOGIN_CUSTOMER } from './graphql/mutations';

// Component
import FormButton from './commons/FormButton';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isUsernameEmpty: false,
      isPasswordEmpty: false,
    };
  }

  componentDidMount() {
    const token = Cookies.get('token');
    const { client } = this.props;
    const { history } = this.props;
    if (token) {
      client
        .query({
          query: ADMIN_ME,
        })
        .then(() => {
          history.push('/admin');
        })
        .catch(() => {
          client
            .query({
              query: CUSTOMER_ME,
            })
            .then(() => {
              history.push('/customer');
            })
            .catch(() => {
              Cookies.remove('token', {
                path: '/',
              });
            });
        });
    }
  }

  setToken = (token) => {
    Cookies.set('token', token);
  };

  handleLoginError = (err) => {
    this.setState({ isPasswordEmpty: true, isUsernameEmpty: true });
    const { message } = err;
    const el = document.getElementById('login-error');
    if (el) {
      if (message === 'GraphQL error: CTM_NOT_EXIST') {
        el.style.display = 'block';
        el.textContent = "Sorry, we couldn't find an account with that username.";
      }
      if (message === 'GraphQL error: CTM_WRONG_PASSWORD') {
        el.style.display = 'block';
        el.textContent = 'Sorry, that password is not right.';
      }
    }
  };

  render() {
    const { username, password, isUsernameEmpty, isPasswordEmpty } = this.state;
    return (
      <div className="LoginPage">
        <div className="LoginPage__left">
          <p className="LoginPage__left--text">
            Order
            <br />
            Manamgment
            <br />
            System
          </p>
        </div>
        <div className="LoginPage__right">
          <Form style={{ width: '280px' }}>
            <Form.Item
              label="Username"
              colon={false}
              required
              validateStatus={!isUsernameEmpty || username !== '' ? 'success' : 'error'}
            >
              <Input onChange={(e) => this.setState({ username: e.target.value })} value={username} type="text" />
            </Form.Item>
            <Form.Item
              label="Password"
              colon={false}
              required
              validateStatus={!isPasswordEmpty || password !== '' ? 'success' : 'error'}
            >
              <Input.Password onChange={(e) => this.setState({ password: e.target.value })} value={password} />
            </Form.Item>
            <p id="login-error" style={{ display: 'none', color: 'red' }} />
            <Mutation
              mutation={LOGIN_CUSTOMER}
              variables={{ username, password }}
              onError={(err) => {
                this.handleLoginError(err);
              }}
              onCompleted={(data) => {
                this.setToken(data.loginCustomer.token);
                window.location.href = '/customer';
              }}
            >
              {(loginCutomer) => <FormButton onClick={loginCutomer}>Login</FormButton>}
            </Mutation>
          </Form>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withApollo(LoginPage);
