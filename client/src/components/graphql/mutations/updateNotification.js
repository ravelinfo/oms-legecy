import { gql } from 'apollo-boost';

const UPDATE_NOTIFICATION = gql`
  mutation UpdateNotification($ids: [ID]!) {
    updateNotification(ids: $ids) {
      count
    }
  }
`;

export default UPDATE_NOTIFICATION;
