import { gql } from 'apollo-boost';

const ADMIN_UPDATE_NOTIFICATION = gql`
  mutation AdminUpdateNotification($ids: [ID]!) {
    adminUpdateNotification(ids: $ids) {
      count
    }
  }
`;

export default ADMIN_UPDATE_NOTIFICATION;
