import { gql } from 'apollo-boost';

const DELETE_CUSTOMER = gql`
  mutation DeleteCustomer($id: ID!) {
    deleteCustomer(id: $id) {
      id
      company
      username
    }
  }
`;

export default DELETE_CUSTOMER;
