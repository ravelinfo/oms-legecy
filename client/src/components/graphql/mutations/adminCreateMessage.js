import { gql } from 'apollo-boost';

const ADMIN_CREATE_MESSAGE = gql`
  mutation AdminCreateMessage($id: ID!, $content: String!) {
    adminCreateMessage(id: $id, content: $content) {
      id
      customer {
        id
        username
      }
      admin {
        id
        username
        email
      }
      from
      content
      createdAt
    }
  }
`;

export default ADMIN_CREATE_MESSAGE;
