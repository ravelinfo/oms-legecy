import { gql } from 'apollo-boost';

const UPDATE_ORDER_ITEM = gql`
  mutation UpdateOrderItem($id: ID!, $quantity: Int!) {
    updateOrderItem(id: $id, quantity: $quantity) {
      id
      quantity
    }
  }
`;
export default UPDATE_ORDER_ITEM;
