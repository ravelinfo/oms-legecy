import { gql } from 'apollo-boost';

const DELETE_ORDER_ITEM = gql`
  mutation DeleteOrderItem($id: ID!) {
    deleteOrderItem(id: $id) {
      id
    }
  }
`;

export default DELETE_ORDER_ITEM;
