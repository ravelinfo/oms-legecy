import { gql } from 'apollo-boost';

const DELETE_ADMIN = gql`
  mutation DeleteAdmin($id: ID!) {
    deleteAdmin(id: $id) {
      id
      name
      username
      email
    }
  }
`;

export default DELETE_ADMIN;
