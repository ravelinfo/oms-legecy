import { gql } from 'apollo-boost';

const CREATE_PRODUCT = gql`
  mutation CreateProduct($name: String!, $description: String, $quantity: Int!) {
    createProduct(name: $name, description: $description, quantity: $quantity) {
      id
      name
      description
      quantity
      levelPrices {
        id
        level {
          id
          name
        }
        price
      }
    }
  }
`;

export default CREATE_PRODUCT;
