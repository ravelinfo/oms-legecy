import { gql } from 'apollo-boost';

const DELETE_ORDER = gql`
  mutation DeleteOrder($id: ID!) {
    deleteOrder(id: $id) {
      id
      orderNumber
    }
  }
`;

export default DELETE_ORDER;
