import { gql } from 'apollo-boost';

const DELETE_LEVEL = gql`
  mutation DeleveLevel($id: ID!) {
    deleteLevel(id: $id) {
      id
      name
    }
  }
`;

export default DELETE_LEVEL;
