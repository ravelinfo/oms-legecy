import { gql } from 'apollo-boost';

const LOGIN_ADMIN = gql`
  mutation LoginAdmin($username: String!, $password: String!) {
    loginAdmin(username: $username, password: $password) {
      token
      admin {
        id
        username
        name
      }
    }
  }
`;

export default LOGIN_ADMIN;
