import { gql } from 'apollo-boost';

const CREATE_ADMIN = gql`
  mutation CreateAdmin($username: String!, $password: String!, $email: String, $name: String!) {
    createAdmin(username: $username, password: $password, email: $email, name: $name) {
      id
      username
      name
      email
    }
  }
`;

export default CREATE_ADMIN;
