import { gql } from 'apollo-boost';

const UPDATE_CUSTOMER = gql`
  mutation UpdateCustomer(
    $id: ID!
    $username: String
    $password: String
    $phone: String
    $address: String
    $email: String
    $company: String
    $levelId: ID
  ) {
    updateCustomer(
      id: $id
      username: $username
      password: $password
      phone: $phone
      address: $address
      email: $email
      company: $company
      levelId: $levelId
    ) {
      id
      username
      company
      phone
      address
      email
      level {
        id
        name
      }
    }
  }
`;

export default UPDATE_CUSTOMER;
