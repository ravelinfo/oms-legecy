import { gql } from 'apollo-boost';

const CREATE_ORDER_ITEM = gql`
  mutation CreateOrderItem($customerProductId: ID!, $quantity: Int!) {
    createOrderItem(customerProductId: $customerProductId, quantity: $quantity) {
      id
      customerProduct {
        id
        price
        product {
          id
          name
          description
          quantity
        }
      }
      quantity
    }
  }
`;

export default CREATE_ORDER_ITEM;
