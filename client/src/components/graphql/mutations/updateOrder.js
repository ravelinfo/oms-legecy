import { gql } from 'apollo-boost';

const UPDATE_ORDER = gql`
  mutation UpdateOrder($id: ID!, $totalPrice: Int, $orderStatus: String) {
    updateOrder(id: $id, total: $totalPrice, status: $orderStatus) {
      id
      total
      status
      updatedAt
    }
  }
`;

export default UPDATE_ORDER;
