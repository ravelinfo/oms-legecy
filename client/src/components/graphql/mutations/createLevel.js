import { gql } from 'apollo-boost';

const CREATE_LEVEL = gql`
  mutation CreateLevel($name: String!) {
    createLevel(name: $name) {
      id
      name
    }
  }
`;

export default CREATE_LEVEL;
