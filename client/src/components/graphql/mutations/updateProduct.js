import { gql } from 'apollo-boost';

const UPDATE_PRODUCT = gql`
  mutation UpdateProduct($productId: ID!, $name: String, $description: String, $quantity: Int) {
    updateProduct(id: $productId, name: $name, description: $description, quantity: $quantity) {
      id
      name
      description
      quantity
    }
  }
`;

export default UPDATE_PRODUCT;
