import { gql } from 'apollo-boost';

const CREATE_MESSAGE = gql`
  mutation CreateMessage($id: ID!, $content: String!) {
    createMessage(id: $id, content: $content) {
      id
      customer {
        id
        username
      }
      admin {
        id
        username
        email
      }
      from
      content
      createdAt
    }
  }
`;

export default CREATE_MESSAGE;
