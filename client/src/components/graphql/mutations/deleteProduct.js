import { gql } from 'apollo-boost';

const DELETE_PRODUCT = gql`
  mutation DeleteProduct($productId: ID!) {
    deleteProduct(id: $productId) {
      id
      name
      description
      levelPrices {
        id
        level {
          id
          name
        }
        price
      }
    }
  }
`;

export default DELETE_PRODUCT;
