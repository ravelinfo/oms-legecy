import { gql } from 'apollo-boost';

const DELETE_LEVEL_PRICE = gql`
  mutation DeleteLevelPrice($levelPriceId: ID!) {
    deleteLevelPrice(id: $levelPriceId) {
      id
      price
    }
  }
`;

export default DELETE_LEVEL_PRICE;
