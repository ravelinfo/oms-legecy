import { gql } from 'apollo-boost';

const CREATE_CUSTOMER = gql`
  mutation CreateCustomer(
    $company: String!
    $username: String!
    $password: String!
    $phone: String
    $address: String
    $email: String
    $levelId: ID!
  ) {
    createCustomer(
      company: $company
      username: $username
      password: $password
      phone: $phone
      address: $address
      email: $email
      levelId: $levelId
    ) {
      id
      username
      company
      phone
      address
      email
      level {
        id
        name
      }
    }
  }
`;

export default CREATE_CUSTOMER;
