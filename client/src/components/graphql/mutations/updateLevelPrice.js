import { gql } from 'apollo-boost';

const UPDATE_LEVEL_PRICE = gql`
  mutation UpdateLevelPrice($levelPriceId: ID!, $price: Int!, $isAvailable: Boolean) {
    updateLevelPrice(id: $levelPriceId, price: $price, isAvailable: $isAvailable) {
      id
      level {
        id
        name
      }
      price
      isAvailable
    }
  }
`;

export default UPDATE_LEVEL_PRICE;
