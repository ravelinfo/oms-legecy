import { gql } from 'apollo-boost';

const UPDATE_ADMIN = gql`
  mutation UpdateAdmin($id: ID!, $name: String, $password: String, $email: String) {
    updateAdmin(id: $id, name: $name, password: $password, email: $email) {
      id
      username
      email
      name
    }
  }
`;

export default UPDATE_ADMIN;
