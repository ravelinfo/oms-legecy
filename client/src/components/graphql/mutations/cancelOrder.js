import { gql } from 'apollo-boost';

const CANCEL_ORDER = gql`
  mutation CancelOrder($id: ID!) {
    cancelOrder(id: $id) {
      id
      status
      updatedAt
    }
  }
`;

export default CANCEL_ORDER;
