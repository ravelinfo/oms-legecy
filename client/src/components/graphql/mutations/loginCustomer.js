import { gql } from 'apollo-boost';

const LOGIN_CUSTOMER = gql`
  mutation LoginCustomer($username: String!, $password: String!) {
    loginCustomer(username: $username, password: $password) {
      token
      customer {
        id
        username
        email
        company
      }
    }
  }
`;

export default LOGIN_CUSTOMER;
