import { gql } from 'apollo-boost';

const PLACE_ORDER = gql`
  mutation {
    placeOrder {
      id
      orderNumber
      createdAt
      updatedAt
      orderItems
      total
      status
    }
  }
`;

export default PLACE_ORDER;
