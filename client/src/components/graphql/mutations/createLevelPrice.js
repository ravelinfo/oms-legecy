import { gql } from 'apollo-boost';

const CREATE_LEVEL_PRICE = gql`
  mutation CreateLevelPrice($productId: ID!, $levelId: ID!, $price: Int!, $isAvailable: Boolean) {
    createLevelPrice(productId: $productId, levelId: $levelId, price: $price, isAvailable: $isAvailable) {
      id
      level {
        id
        name
      }
      price
      isAvailable
    }
  }
`;

export default CREATE_LEVEL_PRICE;
