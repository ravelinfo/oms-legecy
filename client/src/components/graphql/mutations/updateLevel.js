import { gql } from 'apollo-boost';

const UPDATE_LEVEL = gql`
  mutation UpdateLevel($id: ID!, $name: String) {
    updateLevel(id: $id, name: $name) {
      id
      name
    }
  }
`;

export default UPDATE_LEVEL;
