import { gql } from 'apollo-boost';

const FETCH_PRODUCTS = gql`
  query AdminProducts($filter: JSON) {
    adminProducts(filter: $filter) {
      id
      name
      description
      quantity
      levelPrices {
        id
        level {
          id
          name
        }
        price
        isAvailable
      }
    }
  }
`;

export default FETCH_PRODUCTS;
