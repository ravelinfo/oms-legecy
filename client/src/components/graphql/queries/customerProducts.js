import { gql } from 'apollo-boost';

const FETCH_CUSTOMER_PRODUCTS = gql`
  query CustomerProducts($filter: JSON) {
    customerProducts(filter: $filter) {
      id
      price
      product {
        id
        name
        description
        quantity
      }
    }
  }
`;

export default FETCH_CUSTOMER_PRODUCTS;
