import { gql } from 'apollo-boost';

const FETCH_ORDER_ITEMS = gql`
  {
    orderItems {
      id
      customerProduct {
        id
        price
        product {
          id
          name
          description
          quantity
        }
      }
      quantity
    }
  }
`;

export default FETCH_ORDER_ITEMS;
