import { gql } from 'apollo-boost';

const FETCH_LEVELS = gql`
  {
    levels {
      id
      name
    }
  }
`;

export default FETCH_LEVELS;
