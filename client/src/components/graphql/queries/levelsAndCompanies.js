import { gql } from 'apollo-boost';

const FETCH_LEVELS_AND_COMPANIES = gql`
  query {
    levels {
      name
    }
    customers {
      company
    }
  }
`;

export default FETCH_LEVELS_AND_COMPANIES;
