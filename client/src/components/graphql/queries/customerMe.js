import { gql } from 'apollo-boost';

const CUSTOMER_ME = gql`
  {
    customerMe {
      id
      username
      company
      phone
      address
      email
      isEmailNotificationEnabled
    }
  }
`;

export default CUSTOMER_ME;
