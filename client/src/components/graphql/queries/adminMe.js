import { gql } from 'apollo-boost';

const ADMIN_ME = gql`
  {
    adminMe {
      id
      name
      username
      email
      isEmailNotificationEnabled
    }
  }
`;

export default ADMIN_ME;
