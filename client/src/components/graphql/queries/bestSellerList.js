import { gql } from 'apollo-boost';

const FETCH_BEST_SELLER = gql`
  query BestSellerList($startDate: String!, $endDate: String!) {
    bestSellerList(startDate: $startDate, endDate: $endDate)
  }
`;

export default FETCH_BEST_SELLER;
