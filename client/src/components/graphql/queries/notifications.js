import { gql } from 'apollo-boost';

const FETCH_NOTIFICATIONS = gql`
  {
    notifications {
      id
      fromAdmin {
        username
      }
      toAdmin {
        username
      }
      fromCustomer {
        username
        company
      }
      toCustomer {
        username
        company
      }
      orderCustomer {
        username
        company
      }
      messageContent
      orderNumber
      orderStatusFrom
      orderStatusTo
      orderTotalFrom
      orderTotalTo
      createdAt
      hasBeenRead
    }
  }
`;

export default FETCH_NOTIFICATIONS;
