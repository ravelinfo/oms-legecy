import { gql } from 'apollo-boost';

const FETCH_ORDERS = gql`
  query orders($filter: JSON) {
    orders(filter: $filter) {
      id
      orderNumber
      createdAt
      updatedAt
      orderItems
      total
      status
      messages {
        id
        customer {
          id
          username
          company
        }
        admin {
          id
          username
        }
        from
        content
        createdAt
      }
    }
  }
`;

export default FETCH_ORDERS;
