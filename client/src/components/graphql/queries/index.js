import ADMIN_ME from './adminMe';
import FETCH_ADMIN from './admin';
import FETCH_ADMINS from './admins';
import FETCH_PRODUCT from './adminProduct';
import FETCH_PRODUCTS from './adminProducts';
import FETCH_CUSTOMER from './customer';
import FETCH_CUSTOMERS from './customers';
import FETCH_LEVEL from './level';
import FETCH_LEVELS from './levels';
import FETCH_ADMIN_ORDER from './adminOrder';
import FETCH_ADMIN_ORDERS from './adminOrders';
import FETCH_LEVEL_PRICE_AND_PRODUCT from './levelPriceAndAdminProduct';
import FETCH_LEVELS_AND_ADMIN_PRODUCT from './levelsAndAdminProduct';
import FETCH_LEVELS_AND_COMPANIES from './levelsAndCompanies';
import FETCH_ADMIN_NOTIFICATIONS from './adminNotifications';
import FETCH_TOTAL_SALES from './totalSales';
import FETCH_BEST_SELLER from './bestSellerList';

// Customer
import CUSTOMER_ME from './customerMe';
import FETCH_CUSTOMER_PRODUCTS from './customerProducts';
import FETCH_ORDER from './order';
import FETCH_ORDERS from './orders';
import FETCH_ORDER_ITEMS from './orderItems';
import FETCH_NOTIFICATIONS from './notifications';

export {
  ADMIN_ME,
  FETCH_ADMIN,
  FETCH_ADMINS,
  FETCH_PRODUCT,
  FETCH_PRODUCTS,
  FETCH_CUSTOMER,
  FETCH_CUSTOMERS,
  FETCH_LEVEL,
  FETCH_LEVELS,
  FETCH_ADMIN_ORDER,
  FETCH_ADMIN_ORDERS,
  FETCH_LEVEL_PRICE_AND_PRODUCT,
  FETCH_LEVELS_AND_ADMIN_PRODUCT,
  FETCH_LEVELS_AND_COMPANIES,
  FETCH_ADMIN_NOTIFICATIONS,
  FETCH_TOTAL_SALES,
  FETCH_BEST_SELLER,
  // Customer
  CUSTOMER_ME,
  FETCH_CUSTOMER_PRODUCTS,
  FETCH_ORDER,
  FETCH_ORDERS,
  FETCH_ORDER_ITEMS,
  FETCH_NOTIFICATIONS,
};
