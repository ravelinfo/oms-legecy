import { gql } from 'apollo-boost';

const FETCH_ORDER = gql`
  query order($orderNumber: Int!) {
    order(orderNumber: $orderNumber) {
      id
      orderNumber
      createdAt
      updatedAt
      orderItems
      total
      status
      messages {
        id
        customer {
          id
          username
          company
        }
        admin {
          id
          username
        }
        from
        content
        createdAt
      }
    }
  }
`;

export default FETCH_ORDER;
