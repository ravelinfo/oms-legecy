import { gql } from 'apollo-boost';

const FETCH_ADMINS = gql`
  {
    admins {
      id
      name
      username
      email
    }
  }
`;

export default FETCH_ADMINS;
