import { gql } from 'apollo-boost';

const FETCH_ADMIN_ORDERS = gql`
  query AdminOrders($filter: JSON) {
    adminOrders(filter: $filter) {
      id
      orderNumber
      customer {
        id
        username
        email
        company
        level {
          id
          name
        }
      }
      createdAt
      updatedAt
      orderItems
      total
      status
      messages {
        id
        customer {
          id
          username
          company
        }
        admin {
          id
          username
        }
        from
        content
        createdAt
      }
    }
  }
`;

export default FETCH_ADMIN_ORDERS;
