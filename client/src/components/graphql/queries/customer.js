import { gql } from 'apollo-boost';

const FETCH_CUSTOMER = gql`
  query FetchCustomer($id: ID!) {
    customer(id: $id) {
      id
      username
      email
      company
      phone
      address
      level {
        id
        name
      }
    }
  }
`;

export default FETCH_CUSTOMER;
