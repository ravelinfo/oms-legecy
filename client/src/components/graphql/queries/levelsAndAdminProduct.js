import { gql } from 'apollo-boost';

const FETCH_LEVELS_AND_ADMIN_PRODUCT = gql`
  query($productId: ID!) {
    levels {
      id
      name
    }
    adminProduct(id: $productId) {
      id
      name
      description
      quantity
      levelPrices {
        id
        level {
          id
          name
        }
        price
      }
    }
  }
`;

export default FETCH_LEVELS_AND_ADMIN_PRODUCT;
