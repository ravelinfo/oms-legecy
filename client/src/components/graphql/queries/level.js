import { gql } from 'apollo-boost';

const FETCH_LEVEL = gql`
  query FetchLevel($id: ID!) {
    level(id: $id) {
      id
      name
    }
  }
`;

export default FETCH_LEVEL;
