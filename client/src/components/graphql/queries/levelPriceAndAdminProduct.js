import { gql } from 'apollo-boost';

const FETCH_LEVEL_PRICE_AND_PRODUCT = gql`
  query($levelPriceId: ID!, $productId: ID!) {
    levelPrice(id: $levelPriceId) {
      id
      level {
        id
        name
      }
      price
      isAvailable
    }
    adminProduct(id: $productId) {
      id
      name
      description
      quantity
    }
  }
`;

export default FETCH_LEVEL_PRICE_AND_PRODUCT;
