import { gql } from 'apollo-boost';

const FETCH_CUSTOMERS = gql`
  {
    customers {
      id
      company
      username
      phone
      address
      email
      level {
        id
        name
      }
    }
  }
`;

export default FETCH_CUSTOMERS;
