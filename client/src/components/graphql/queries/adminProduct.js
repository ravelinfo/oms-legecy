import { gql } from 'apollo-boost';

const FETCH_PRODUCT = gql`
  query adminProduct($productId: ID!) {
    adminProduct(id: $productId) {
      id
      name
      description
      quantity
      levelPrices {
        id
        level {
          id
          name
        }
        price
      }
    }
  }
`;

export default FETCH_PRODUCT;
