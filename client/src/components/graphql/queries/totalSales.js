import { gql } from 'apollo-boost';

const FETCH_TOTAL_SALES = gql`
  query TotalSales($startDate: String!, $endDate: String!) {
    totalSales(startDate: $startDate, endDate: $endDate)
  }
`;

export default FETCH_TOTAL_SALES;
