import { gql } from 'apollo-boost';

const FETCH_ADMIN = gql`
  query FetchAdmin($id: ID!) {
    admin(id: $id) {
      id
      username
      email
      name
    }
  }
`;

export default FETCH_ADMIN;
