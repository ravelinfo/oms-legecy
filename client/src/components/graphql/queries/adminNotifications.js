import { gql } from 'apollo-boost';

const FETCH_ADMIN_NOTIFICATIONS = gql`
  {
    adminNotifications {
      id
      fromAdmin {
        username
      }
      toAdmin {
        username
      }
      fromCustomer {
        username
        company
      }
      toCustomer {
        username
        company
      }
      orderCustomer {
        username
        company
      }
      messageContent
      orderNumber
      orderStatusFrom
      orderStatusTo
      orderTotalFrom
      orderTotalTo
      createdAt
      hasBeenRead
    }
  }
`;

export default FETCH_ADMIN_NOTIFICATIONS;
