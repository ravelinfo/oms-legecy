import React from 'react';
import PropTypes from 'prop-types';
import '../../css/commons/FormContainer.css';
import { Form } from 'antd';

const FormContainer = ({ children, header, onSubmit }) => {
  return (
    <div className="FormContainer">
      <div className="FormContainer__header">
        <p className="FormContainer__header--text">{header}</p>
      </div>
      <Form className="FormContainer__form" onSubmit={onSubmit}>
        {children}
      </Form>
    </div>
  );
};

FormContainer.defaultProps = {
  onSubmit: (e) => {
    e.preventDefault();
  },
};

FormContainer.propTypes = {
  children: PropTypes.node.isRequired,
  header: PropTypes.string.isRequired,
  onSubmit: PropTypes.func,
};

export default FormContainer;
