import React from 'react';
import PropTypes from 'prop-types';
import '../../css/commons/FormButton.css';

const FormButton = ({ children, onClick, disabled }) => {
  return (
    <button
      onClick={onClick}
      className="FormButton"
      type="submit"
      disabled={disabled}
      style={disabled ? { background: '#999' } : {}}
    >
      {children}
    </button>
  );
};

FormButton.defaultProps = {
  disabled: false,
  onClick: () => {},
};

FormButton.propTypes = {
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export default FormButton;
