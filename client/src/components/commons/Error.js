import React from 'react';
import '../../css/commons/Error.css';
import { Result } from 'antd';

const Error = () => {
  return (
    <div>
      <Result
        status="error"
        /* eslint-disable */
        title={
          <div className="Error__title">
            <h3 className="Error__title--main"> Network Error</h3>
            <p className="Error__title--sub">
              Slow or no internet connection.
              <br />
              Please check your internet setting.
            </p>
          </div>
        }
        /* eslint-disable */
      />
    </div>
  );
};

export default Error;
