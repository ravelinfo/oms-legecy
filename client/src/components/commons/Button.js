import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ children, bg, color, bd, mr, style, onClick, isDisabled }) => {
  return (
    <button
      type="button"
      style={{
        ...style,
        color: `${color}`,
        padding: '8px 32px',
        border: `${bd}`,
        background: `${bg}`,
        borderRadius: '3px',
        fontWeight: 'bold',
        margin: `${mr}`,
        cursor: 'pointer',
      }}
      onClick={onClick}
      disabled={isDisabled}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  bg: null,
  color: null,
  bd: null,
  mr: null,
  style: null,
  isDisabled: false,
  onClick: () => {},
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  bg: PropTypes.string,
  color: PropTypes.string,
  bd: PropTypes.string,
  mr: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.string),
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export default Button;
