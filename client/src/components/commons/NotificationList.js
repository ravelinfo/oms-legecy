import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, List, message, Skeleton } from 'antd';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_NOTIFICATIONS, FETCH_ADMIN_NOTIFICATIONS } from '../graphql/queries';
import { UPDATE_NOTIFICATION, ADMIN_UPDATE_NOTIFICATION } from '../graphql/mutations';

// Utils
import { constants, formatters } from '../../utils';

// Component
import Error from './Error';

const onReadClick = async (ntfIds, user, apolloClient) => {
  // Check if mark all btn is clicked
  if (Array.isArray(ntfIds)) {
    ntfIds.forEach((id) => {
      document.getElementById(id).style.background = '#fff';
    });
  } else {
    const el = document.getElementById(ntfIds);
    el.style.background = '#fff';
  }

  try {
    await apolloClient.mutate({
      mutation: user === 'admin' ? ADMIN_UPDATE_NOTIFICATION : UPDATE_NOTIFICATION,
      variables: { ids: ntfIds },
    });
  } catch (err) {
    message.error('Unexpected error occurs when clicking read');
  }
};

const renderItem = (ntf, user, apolloClient) => {
  // Get order number combination for routing
  let company;
  if (ntf.fromCustomer) company = ntf.fromCustomer.company;
  else if (ntf.toCustomer) company = ntf.toCustomer.company;
  else company = ntf.orderCustomer.company;
  const orderNumComb = `${company}-${ntf.orderNumber}`;

  // Get from
  let from;
  if (user === 'admin') from = ntf.fromCustomer ? ntf.fromCustomer.username : ntf.fromAdmin.username;

  // Get description
  let description = '';
  if (ntf.messageContent) {
    description = ntf.messageContent;
  } else {
    if (ntf.orderStatusTo) {
      // Order status change
      if (!ntf.orderStatusFrom) {
        // New order
        description += constants.ORDER_STATUS[ntf.orderStatusTo];
      } else {
        description += `Order status from "${constants.ORDER_STATUS[ntf.orderStatusFrom]}" to "${
          constants.ORDER_STATUS[ntf.orderStatusTo]
        }".\n`;
      }
    }
    if (ntf.orderTotalTo) {
      // Order total change
      description += `Order total from Rp. ${formatters.price(ntf.orderTotalFrom)} to Rp. ${formatters.price(
        ntf.orderTotalTo
      )}.`;
    }
  }

  const title = user === 'customer' ? orderNumComb : `${orderNumComb} (From: ${from})`;

  return (
    <Link to={`/${user}/order/${orderNumComb}`} key={ntf.id}>
      <List.Item
        id={ntf.id}
        style={{ background: '#f7f7f7', padding: '10px', borderBottom: '2px solid #f7f7f7', whiteSpace: 'pre' }}
        onClick={() => onReadClick([ntf.id], user, apolloClient)}
      >
        <List.Item.Meta title={<span style={{ fontWeight: 'bold' }}>{title}</span>} description={description} />
      </List.Item>
    </Link>
  );
};

// User is either 'admin' or 'customer'
const NotificationList = (props) => {
  const { user, client } = props;
  const query = user === 'admin' ? FETCH_ADMIN_NOTIFICATIONS : FETCH_NOTIFICATIONS;

  return (
    <Query query={query} fetchPolicy="cache-and-network">
      {({ loading, error, data }) => {
        if (loading)
          return (
            <Card>
              <Skeleton active />
            </Card>
          );
        if (error)
          return (
            <Card>
              <Error />
            </Card>
          );
        const { notifications, adminNotifications } = data;
        const ntfs = [...(notifications || adminNotifications)]; // Assign customer's or amdin's
        ntfs.reverse();
        return (
          <Card>
            <div style={{ borderBottom: '2px solid rgba(0,0,0,.09)', textAlign: 'right ' }}>
              <Button
                onClick={() =>
                  onReadClick(
                    ntfs.map((ntf) => ntf.id),
                    user,
                    client
                  )
                }
                style={{ border: 'none', padding: '0' }}
              >
                Mark all as read
              </Button>
            </div>
            <List style={{ maxHeight: '400px', overflowY: 'scroll' }}>
              {ntfs.map((ntf) => renderItem(ntf, user, client))}
            </List>
          </Card>
        );
      }}
    </Query>
  );
};

export default withApollo(NotificationList);
