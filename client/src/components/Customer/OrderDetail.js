import React from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import '../../css/OrderDetail.css';
import { message, Popconfirm, Skeleton } from 'antd';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_ORDER } from '../graphql/queries';
import { CANCEL_ORDER } from '../graphql/mutations';

// Utils
import { constants, formatters, errors } from '../../utils';

// Components
import Button from '../commons/Button';
import OrderMessageForm from './OrderMessageForm';
import OrderMessage from './OrderMessage';
import Error from '../commons/Error';

const presentTotal = (order) => {
  let accu = 0;
  order.orderItems.forEach((orderItem) => {
    accu += orderItem.customerProduct.price * orderItem.quantity;
  });

  if (accu === order.total) {
    return (
      <span className="table--total">
        Order Total :<span className="table--price">{`Rp. ${formatters.price(accu)}`}</span>
      </span>
    );
  }
  return (
    <>
      <span className="table--original-price">
        Original Price :<span className="table--price">{`Rp. ${formatters.price(accu)}`}</span>
      </span>
      <span className="table--total">
        Order Total :<span className="table--price">{`Rp. ${formatters.price(order.total)}`}</span>
      </span>
    </>
  );
};

const onCancelClick = (props, orderId) => {
  const { match } = props;
  const findLastIndexOfHyphen = match.params.orderId.lastIndexOf('-');
  const orderNumber = parseInt(String(match.params.orderId).slice(findLastIndexOfHyphen + 1), 10);
  props.client
    .mutate({
      mutation: CANCEL_ORDER,
      variables: { id: orderId },
      update: (store, { data: { cancelOrder } }) => {
        const data = store.readQuery({
          query: FETCH_ORDER,
          variables: {
            orderNumber,
          },
        });

        data.order.status = cancelOrder.status;
        data.order.updatedAt = cancelOrder.updatedAt;

        store.writeQuery({
          query: FETCH_ORDER,
          data,
          variables: {
            orderNumber,
          },
        });
      },
    })
    .then(() => {
      message.success('Your order has been canceled!');
    })
    .catch((err) => {
      errors.handleError(err);
    });
};

const isCancelDisabled = (status) => status === 'SHIPPED' || status === 'COMPLETED' || status === 'CANCELED';

const renderOrderItems = (orderItems) => {
  return (
    <>
      <thead>
        <tr>
          <td className="table--title table--title-left">Product</td>
          <td className="table--title table--title-right">Price</td>
          <td className="table--title">Quantity</td>
          <td className="table--title table--title-right">Total</td>
        </tr>
      </thead>
      <tbody>
        {orderItems.map((orderItem) => (
          <tr className="table__item" key={orderItem.customerProduct.product.name}>
            <th className="table__item--text table__item--text-left">{orderItem.customerProduct.product.name}</th>
            <th className="table__item--text table__item--text-right">
              {`Rp. ${formatters.price(orderItem.customerProduct.price)}`}
            </th>
            <th className="table__item--text">{orderItem.quantity}</th>
            <th className="table__item--text table__item--text-right">
              {`Rp. ${formatters.price(orderItem.quantity * orderItem.customerProduct.price)}`}
            </th>
          </tr>
        ))}
      </tbody>
    </>
  );
};

const OrderDetail = (props) => {
  const { match } = props;
  const findLastIndexOfHyphen = match.params.orderId.lastIndexOf('-');
  const orderNumber = parseInt(String(match.params.orderId).slice(findLastIndexOfHyphen + 1), 10);
  return (
    <Query
      query={FETCH_ORDER}
      variables={{
        // Extract OrderNumber from Company-OrderNumber format
        orderNumber,
      }}
    >
      {({ loading, error, data }) => {
        if (loading)
          return (
            <div className="OrderDetail">
              <div className="OrderDetail__left">
                <div className="OrderDetail__card">
                  <div className="OrderDetail__card--header">Order details</div>
                  <div className="OrderDetail__card card__info">
                    <Skeleton active />
                  </div>
                </div>
              </div>
            </div>
          );
        if (error) return <Error />;
        try {
          const { order } = data;
          return (
            <div className="OrderDetail">
              <div className="OrderDetail__left">
                <div className="OrderDetail__card">
                  <div className="OrderDetail__card--header">Order details</div>
                  <div className="OrderDetail__card card__info">
                    <div className="OrderDetail__card card__info--left">
                      <span>Order # :</span>
                      <span>Date :</span>
                      <span>Last Updated :</span>
                    </div>
                    <div className="OrderDetail__card card__info--right">
                      <span>{props.match.params.orderId}</span>
                      <span>{formatters.datetime(order.createdAt, 'id')}</span>
                      <span>{formatters.datetime(order.updatedAt, 'id')}</span>
                    </div>
                  </div>
                  <div className="OrderDetail__card--divider" />
                  <div className="OrderDetail__table">
                    <table className="OrderDetail__table table">{renderOrderItems(order.orderItems)}</table>
                    <p>{presentTotal(order)}</p>
                  </div>
                  <div className="OrderDetail__card--divider" />
                  <div className="OrderDetail__status">
                    <p className="OrderDetail__status--text">
                      Order Status :<span style={{ color: '#333' }}>{constants.ORDER_STATUS[order.status]}</span>
                    </p>
                    {isCancelDisabled(order.status) ? (
                      <div />
                    ) : (
                      <Popconfirm
                        title="Are you sure to cancel this order?"
                        onConfirm={() => onCancelClick(props, order.id)}
                        okText="Yes"
                        cancelText="No"
                      >
                        <Button bg="#dd7575" color="#fff">
                          Cancel order
                        </Button>
                      </Popconfirm>
                    )}
                  </div>
                </div>
                <div style={{ textAlign: 'right' }}>
                  <OrderMessageForm id={order.id} orderNumber={orderNumber} />
                </div>
              </div>
              <div className="OrderDetail__right">
                <OrderMessage messages={order.messages} />
              </div>
            </div>
          );
        } catch (e) {
          return <Redirect to="/customer/orders" />;
        }
      }}
    </Query>
  );
};

OrderDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
};

export default withApollo(OrderDetail);
