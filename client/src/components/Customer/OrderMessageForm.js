import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

// GraphQL
import { Mutation } from 'react-apollo';
import { FETCH_ORDER } from '../graphql/queries';
import { CREATE_MESSAGE } from '../graphql/mutations';

// Component
import Button from '../commons/Button';

const { TextArea } = Input;

class OrderMessageForm extends Component {
  constructor() {
    super();
    this.state = { content: '' };
  }

  render() {
    const { content } = this.state;
    const { id, orderNumber } = this.props;
    return (
      <>
        <TextArea
          rows={4}
          style={{
            margin: '25px 0 10px 0',
            resize: 'none',
            whiteSpace: 'pre-line',
          }}
          value={content}
          onChange={(e) => this.setState({ content: e.target.value })}
        />
        <Mutation
          mutation={CREATE_MESSAGE}
          variables={{ id, content }}
          onCompleted={() => this.setState({ content: '' })}
          update={(store, { data: { createMessage } }) => {
            const data = store.readQuery({
              query: FETCH_ORDER,
              variables: {
                orderNumber,
              },
            });

            data.order.messages.push(createMessage);

            store.writeQuery({
              query: FETCH_ORDER,
              data,
              variables: {
                orderNumber,
              },
            });
          }}
        >
          {(createMessage) => (
            <Button bg="#2e6f84" color="#fff" onClick={createMessage}>
              Send messages
            </Button>
          )}
        </Mutation>
      </>
    );
  }
}

OrderMessageForm.propTypes = {
  id: PropTypes.string.isRequired,
  orderNumber: PropTypes.number.isRequired,
};

export default OrderMessageForm;
