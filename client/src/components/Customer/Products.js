import React from 'react';
import '../../css/Inventory.css';
import { Input, Table, message, Skeleton } from 'antd';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_ORDER_ITEMS, FETCH_CUSTOMER_PRODUCTS } from '../graphql/queries';
import { CREATE_ORDER_ITEM } from '../graphql/mutations';

// Utils
import { formatters, errors } from '../../utils';

// Component
import Error from '../commons/Error';

const { Column } = Table;

const onAddToCartClick = (props, customerProduct) => {
  props.client
    .mutate({
      mutation: CREATE_ORDER_ITEM,
      variables: {
        customerProductId: customerProduct.id,
        quantity: 1,
      },
      update: (store, { data: { createOrderItem } }) => {
        const data = store.readQuery({
          query: FETCH_ORDER_ITEMS,
        });

        store.writeQuery({
          query: FETCH_ORDER_ITEMS,
          data: {
            orderItems: [...data.orderItems, createOrderItem],
          },
        });
      },
    })
    .then((res) => {
      message.info(`${res.data.createOrderItem.customerProduct.product.name} is added`);
    })
    .catch((err) => {
      const { graphQLErrors } = err;
      message.error(errors[graphQLErrors[0].message]);
      setInterval(() => {
        window.location.reload();
      }, 3000);
    });
};

const Products = (props) => {
  let searchStr = null;
  return (
    <div style={{ width: '90%', margin: '20px auto' }}>
      <Query query={FETCH_CUSTOMER_PRODUCTS}>
        {({ loading, error, data, refetch }) => {
          if (loading) return <Skeleton active />;
          if (error) return <Error />;

          const { customerProducts } = data;

          return (
            <>
              <div className="Inventory__search">
                <Input.Search
                  placeholder="Search product"
                  style={{ width: '250px' }}
                  onSearch={(value) => {
                    searchStr = value;
                    refetch({ filter: { name: searchStr } });
                  }}
                />
                {searchStr ? (
                  <div className="Inventory__search--result">
                    <span>
                      Search results for <span style={{ fontWeight: 'bold' }}>{searchStr}</span>
                    </span>
                    <button
                      type="button"
                      onClick={() => {
                        searchStr = null;
                        refetch({ filter: { name: searchStr } });
                      }}
                      className="Inventory__search--result-btn"
                    >
                      x
                    </button>
                  </div>
                ) : null}
              </div>
              <Table dataSource={customerProducts} rowKey={(customerProduct) => customerProduct.id}>
                <Column
                  align="left"
                  dataIndex={customerProducts.product}
                  title="Name"
                  key={customerProducts}
                  render={(customerProduct) => <span>{customerProduct.product.name}</span>}
                />
                <Column
                  align="center"
                  dataIndex={customerProducts.product}
                  title="Description"
                  width="400px"
                  key="description"
                  render={(customerProduct) => <span>{customerProduct.product.description}</span>}
                />
                <Column
                  align="center"
                  dataIndex={customerProducts.product}
                  title="Quantity"
                  key="quantity"
                  render={(customerProduct) => <span>{customerProduct.product.quantity}</span>}
                />
                <Column
                  align="right"
                  dataIndex="price"
                  title="Price"
                  key="price"
                  width="200px"
                  render={(price) => <span>{`Rp. ${formatters.price(price)}`}</span>}
                  sorter={(a, b) => a.price - b.price}
                />
                <Column
                  align="right"
                  title=""
                  width="150px"
                  key="add to cart"
                  render={(customerProduct) => (
                    <div>
                      <button
                        type="button"
                        style={{
                          background: '#2e6f84',
                          padding: '4px 8px',
                          color: '#fff',
                          fontWeight: 'bold',
                          borderRadius: '5px',
                          fontSize: '12px',
                          cursor: 'pointer',
                          border: 'none',
                        }}
                        onClick={() => onAddToCartClick(props, customerProduct)}
                      >
                        Add to cart
                      </button>
                    </div>
                  )}
                />
              </Table>
            </>
          );
        }}
      </Query>
    </div>
  );
};

export default withApollo(Products);
