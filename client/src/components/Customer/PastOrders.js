import React from 'react';
import PropTypes from 'prop-types';
import { Table, Badge } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { CUSTOMER_ME, FETCH_ORDERS } from '../graphql/queries';

// Utils
import { constants, formatters, helpers } from '../../utils';

const { Column } = Table;

const PastOrders = (props) => {
  return (
    <div style={{ width: '90%', margin: '20px auto' }}>
      <Query query={FETCH_ORDERS}>
        {({ loading, error, data }) => {
          if (loading) return <div>loading</div>;
          if (error) return <div>error</div>;

          // Get company name
          const {
            customerMe: { company },
          } = props.client.readQuery({ query: CUSTOMER_ME });

          // Get past order list
          const orders = data.orders.filter((order) => {
            if (constants.PAST_ORDER_STATUS[order.status]) {
              return true;
            }
            return false;
          });

          return (
            <Table dataSource={orders} rowKey={(order) => order.id}>
              <Column
                align="left"
                title="Order #"
                dataIndex={orders.orderNumber}
                sorter={(a, b) => a.orderNumber - b.orderNumber}
                key="orderNumber"
                render={(order) => (
                  <span style={{ fontWeight: 'bold', textDecoration: 'underline' }}>
                    <Link to={`/customer/order/${company}-${order.orderNumber}`}>
                      {`${company}-${order.orderNumber}`}
                    </Link>
                  </span>
                )}
              />
              <Column
                align="right"
                title="Total"
                dataIndex={orders.total}
                sorter={(a, b) => a.total - b.total}
                key="total"
                render={(order) => <span>{`Rp. ${formatters.price(order.total)}`}</span>}
              />
              <Column
                align="center"
                title="Last Updated"
                defaultSortOrder="descend"
                dataIndex={orders.updatedAt}
                sorter={(a, b) => moment(a.updatedAt) - moment(b.updatedAt)}
                key="updatedAt"
                render={(order) => <span>{formatters.datetime(order.updatedAt, 'id')}</span>}
              />
              <Column
                align="right"
                dataIndex="status"
                title="Status"
                key="status"
                filters={Object.keys(constants.PAST_ORDER_STATUS).map((key) => ({
                  text: constants.PAST_ORDER_STATUS[key],
                  value: key,
                }))}
                onFilter={(value, record) => value === record.status}
                render={(status) => (
                  <Badge color={helpers.getBadgeColor(status)} text={constants.PAST_ORDER_STATUS[status]} />
                )}
              />
            </Table>
          );
        }}
      </Query>
    </div>
  );
};

PastOrders.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default withApollo(PastOrders);
