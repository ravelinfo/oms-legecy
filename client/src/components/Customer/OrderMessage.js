import React from 'react';
import PropTypes from 'prop-types';
import { formatters } from '../../utils';

const OrderMessage = (props) => {
  const { messages } = props;
  return (
    <>
      {messages.map((message) => (
        <div
          className={`OrderDetail__chatbox OrderDetail__chatbox--${message.from === 'CUSTOMER' ? 'right' : 'left'}`}
          key={message.id}
        >
          <span className="OrderDetail__chatbox--messager">
            {message.from === 'CUSTOMER' ? message.customer.username : message.from}
          </span>
          <p className="OrderDetail__chatbox--message">{message.content}</p>
          <span className="OrderDetail__chatbox--date">{formatters.datetime(message.createdAt, 'id')}</span>
        </div>
      ))}
    </>
  );
};

OrderMessage.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default OrderMessage;
