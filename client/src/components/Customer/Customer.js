import React from 'react';
import '../../css/commons/Oms.css';
import '../../css/commons/AntTable.css';
import { Layout, Dropdown } from 'antd';
import { Switch, Route, useRouteMatch, Link, Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';

// GraphQL
import { Query } from 'react-apollo';
import { CUSTOMER_ME, FETCH_ORDER_ITEMS, FETCH_NOTIFICATIONS } from '../graphql/queries';

// Icons
import Signout from '../../assets/icons/sign-out.svg';
import Shoppingcart from '../../assets/icons/shopping-cart.svg';
import InventoryIcon from '../../assets/icons/inventory.svg';
import Clipboard from '../../assets/icons/list-all.svg';
import UserConfig from '../../assets/icons/user-config.svg';
import Spinner from '../../assets/icons/spinner.svg';
import Bell from '../../assets/icons/bell.svg';

// Components
import Products from './Products';
import ShoppingCart from './ShoppingCart';
import Orders from './Orders';
import OrderDetail from './OrderDetail';
import PastOrders from './PastOrders';
import Setting from './Setting';
import EditSettingPassword from './EditSettingPassword';
import NotificationList from '../commons/NotificationList';
import version from '../../utils/version';

const { Header, Sider, Content } = Layout;

const renderLinks = (url) => {
  const links = [
    { title: 'Products', icon: InventoryIcon },
    { title: 'Orders', icon: Clipboard },
    { title: 'Setting', icon: UserConfig },
  ];
  return links.map((link) => (
    <li className="sider__nav--link" key={link.title}>
      <Link to={`${url}/${link.title.toLowerCase()}`}>
        <img className="sider__nav--icon" src={link.icon} alt={link.title} />
        {link.title}
      </Link>
    </li>
  ));
};

// const Dashboard = () => <div>dashboard</div>;

const Customer = () => {
  const { path, url } = useRouteMatch();

  return (
    <div className="Oms">
      <img className="spinner" src={Spinner} alt="loading" />
      <Layout id="layout" style={{ display: 'none' }}>
        <Sider className="Oms__sider" width="280px">
          <div className="sider__top">
            <span className="sider__top--link">
              <Link to={`${url}`}>OMS</Link>
            </span>
          </div>
          <div className="sider__bottom">
            <ul className="sider__nav">{renderLinks(url)}</ul>
          </div>
          <p className="sider__version">Powered by Ravel Info - v{version}</p>
        </Sider>
        <Layout>
          <Header className="Oms__header">
            <div className="header__container">
              <Query
                query={CUSTOMER_ME}
                onCompleted={() => {
                  // Get elements
                  const layout = document.getElementById('layout');
                  const spinner = document.querySelector('.spinner');
                  // Hide spinner and show app when data is compeletely loaded
                  layout.style.display = 'flex';
                  spinner.style.display = 'none';
                }}
              >
                {({ loading, error, data }) => {
                  if (loading) return <span>Loading</span>;
                  if (error) return <Redirect to="/" />;
                  const { username } = data.customerMe;
                  return <span>{username}</span>;
                }}
              </Query>
              <div className="header__ntf">
                <Query query={FETCH_NOTIFICATIONS}>
                  {({ loading, error, data }) => {
                    if (loading || error) return <img src={Bell} height="27px" alt="bell" />;
                    return (
                      <Dropdown
                        overlay={<NotificationList user="customer" />}
                        overlayStyle={{ width: '400px' }}
                        trigger={['click']}
                      >
                        <div>
                          <img src={Bell} height="27px" alt="bell" />
                          <div className="ntf--badge">
                            <span className="ntf--num">{data.notifications.length}</span>
                          </div>
                        </div>
                      </Dropdown>
                    );
                  }}
                </Query>
              </div>
              <div className="header__shoppincart">
                <Query query={FETCH_ORDER_ITEMS}>
                  {({ loading, error, data }) => {
                    if (loading || error) {
                      return <img src={Shoppingcart} width="30px" alt="shopping cart" />;
                    }
                    return (
                      <div>
                        <Link to={`${url}/shopping-cart`}>
                          <img src={Shoppingcart} width="30px" alt="shopping cart" />
                        </Link>
                        <div className="shoppincart--badge">
                          <span className="shoppincart--num">{data.orderItems.length}</span>
                        </div>
                      </div>
                    );
                  }}
                </Query>
              </div>
              <span style={{ marginLeft: '20px' }}>
                <Link
                  to="/"
                  onClick={() => {
                    Cookies.remove('token', { path: '/' });
                  }}
                >
                  Signout
                </Link>
                <img src={Signout} className="logout-icon" alt="signout" />
              </span>
            </div>
          </Header>
          <Content className="Oms__content">
            <Switch>
              <Route path={`${path}`} exact component={Orders} /> {/* Remove dashboard temporarily for v1.3 */}
              <Route path={`${path}/products`} component={Products} />
              <Route path={`${path}/shopping-cart`} component={ShoppingCart} />
              <Route path={`${path}/orders`} component={Orders} />
              <Route path={`${path}/past-orders`} component={PastOrders} />
              <Route path={`${path}/order/:orderId`} component={OrderDetail} />
              <Route path={`${path}/setting`} component={Setting} />
              <Route path={`${path}/edit-setting-password/:customerId`} component={EditSettingPassword} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default Customer;
