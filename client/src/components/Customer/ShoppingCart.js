import React from 'react';
import '../../css/ShoppingCart.css';
import { Select, message } from 'antd';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { FETCH_ORDER_ITEMS, FETCH_ORDERS } from '../graphql/queries';
import { UPDATE_ORDER_ITEM, DELETE_ORDER_ITEM, PLACE_ORDER } from '../graphql/mutations';

// Utils
import { formatters, errors } from '../../utils';

// Components
import Button from '../commons/Button';

const getQtyArr = (qty) => {
  const arr = [];
  for (let i = 1; i <= qty; i += 1) arr.push(i);
  return arr;
};

const onQtySelect = (value, props, orderItem) => {
  props.client
    .mutate({
      mutation: UPDATE_ORDER_ITEM,
      variables: {
        id: orderItem.id,
        quantity: value,
      },
    })
    .then((res) => res)
    .catch((err) => {
      errors.handleError(err);
    });
};

const onDeleteClick = (props, orderItem) => {
  props.client
    .mutate({
      mutation: DELETE_ORDER_ITEM,
      variables: {
        id: orderItem.id,
      },
      update: (store, { data: { deleteOrderItem } }) => {
        const data = store.readQuery({
          query: FETCH_ORDER_ITEMS,
        });

        const orderItems = data.orderItems.filter((item) => item.id !== deleteOrderItem.id);

        store.writeQuery({
          query: FETCH_ORDER_ITEMS,
          data: {
            orderItems,
          },
        });
      },
    })
    .then((res) => res)
    .catch((err) => {
      errors.handleError(err);
    });
};

const onPlaceOrderClick = (props) => {
  props.client
    .mutate({
      mutation: PLACE_ORDER,
      update: (store, { data: { placeOrder } }) => {
        // Set orderItems empty
        store.writeQuery({
          query: FETCH_ORDER_ITEMS,
          data: {
            orderItems: [],
          },
        });

        // Update orders
        try {
          const { orders } = store.readQuery({ query: FETCH_ORDERS });
          store.writeQuery({
            query: FETCH_ORDERS,
            data: { orders: [placeOrder, ...orders] },
          });
        } catch (err) {
          // Orders might not been fetched yet
        }
      },
    })
    .then(() => {
      message.success('Your order has been placed!');
    })
    .catch((err) => {
      errors.handleError(err);
    });
};

const ShoppingCart = (props) => {
  return (
    <div style={{ width: '90%', margin: '20px auto' }}>
      <div className="ShoppingCart">
        <p className="ShoppingCart--header">Shopping Cart</p>
      </div>

      <Query query={FETCH_ORDER_ITEMS}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading</div>;
          if (error) {
            return <div>Error</div>;
          }
          const { orderItems } = data;

          return (
            <div className="ShoppingCart__container">
              <table className="ShoppingCart__table">
                <thead>
                  <tr className="ShoppingCart__table table__head">
                    <td className="ShoppingCart__table table__head--left">Product</td>
                    <td className="ShoppingCart__table table__head--right">Price</td>
                    <td className="ShoppingCart__table table__head--mid">Quantity</td>
                    <td className="ShoppingCart__table table__head--right">Subtotal</td>
                  </tr>
                </thead>
                <tbody>
                  {orderItems.map((orderItem) => (
                    <tr className="ShoppingCart__table table__body" key={orderItem.id}>
                      <th className="ShoppingCart__table table__body--left">
                        {orderItem.customerProduct.product.name}
                      </th>
                      <th className="ShoppingCart__table table__body--right">
                        {`Rp. ${formatters.price(orderItem.customerProduct.price)}`}
                      </th>
                      <th className="ShoppingCart__table table__body--mid">
                        <Select
                          defaultValue={orderItem.quantity}
                          onSelect={(value) => onQtySelect(value, props, orderItem)}
                          style={{ width: 80 }}
                        >
                          {getQtyArr(orderItem.customerProduct.product.quantity).map((i) => (
                            <Select.Option key={i} value={i}>
                              {i}
                            </Select.Option>
                          ))}
                        </Select>
                      </th>
                      <th className="ShoppingCart__table table__body--right">
                        {`Rp. ${formatters.price(orderItem.customerProduct.price * orderItem.quantity)}`}
                      </th>
                      <th className="ShoppingCart__table table__body--right">
                        <button
                          type="button"
                          onClick={() => onDeleteClick(props, orderItem)}
                          style={{
                            cursor: 'pointer',
                            border: 'none',
                            background: 'transparent',
                          }}
                        >
                          x
                        </button>
                      </th>
                    </tr>
                  ))}
                </tbody>
              </table>
              <div className="ShoppingCart__summary">
                <div className="summary__box">
                  <p className="summary--header">Order Summary</p>
                  <div className="summary__info">
                    <div className="summary__info--left">
                      <p>Items : </p>
                      <p>Order Total :</p>
                    </div>
                    <div className="summary__info--right">
                      <p>{orderItems.length}</p>
                      <p className="summary__info--price">
                        Rp.
                        {(() => {
                          // Calculate order total
                          let total = 0;
                          orderItems.forEach((orderItem) => {
                            total += orderItem.customerProduct.price * orderItem.quantity;
                          });
                          return formatters.price(total);
                        })()}
                      </p>
                    </div>
                  </div>
                </div>
                <div>
                  <Button onClick={() => onPlaceOrderClick(props)} color="#fff" bg="#2e6f84" style={{ width: '320px' }}>
                    Place Order
                  </Button>
                </div>
              </div>
            </div>
          );
        }}
      </Query>
    </div>
  );
};

export default withApollo(ShoppingCart);
