import React from 'react';
import PropTypes from 'prop-types';
import { Table, Badge, Skeleton } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';

// GraphQL
import { Query, withApollo } from 'react-apollo';
import { CUSTOMER_ME, FETCH_ORDERS } from '../graphql/queries';

// Utils
import { constants, formatters, helpers } from '../../utils';

// Component
import Error from '../commons/Error';

const { Column } = Table;

const Orders = (props) => {
  return (
    <div style={{ width: '90%', margin: '20px auto' }}>
      <Query query={FETCH_ORDERS}>
        {({ loading, error, data }) => {
          if (loading) return <Skeleton active />;
          if (error) return <Error />;

          // Get company name
          const {
            customerMe: { company },
          } = props.client.readQuery({ query: CUSTOMER_ME });

          // Get current order list
          const orders = data.orders.filter((order) => {
            if (constants.PROCESS_ORDER_STATUS[order.status]) {
              return true;
            }
            return false;
          });

          return (
            <>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginBottom: '20px',
                  alignItems: 'center',
                }}
              >
                {/* <input
              placeholder="Search order"
              style={{ width: '200px', padding: '5px 10px' }}
            /> */}
                <span style={{ fontWeight: 'bold', textDecoration: 'underline' }}>
                  <Link to="/customer/past-orders">View past orders</Link>
                </span>
              </div>
              <Table dataSource={orders} rowKey={(order) => order.id}>
                <Column
                  align="left"
                  title="Order #"
                  dataIndex={orders.orderNumber}
                  sorter={(a, b) => a.orderNumber - b.orderNumber}
                  key="orderNumber"
                  render={(order) => (
                    <span style={{ fontWeight: 'bold', textDecoration: 'underline' }}>
                      <Link to={`/customer/order/${company}-${order.orderNumber}`}>
                        {`${company}-${order.orderNumber}`}
                      </Link>
                    </span>
                  )}
                />
                <Column
                  align="right"
                  title="Total"
                  dataIndex={orders.total}
                  sorter={(a, b) => a.total - b.total}
                  key="total"
                  render={(order) => <span>{`Rp. ${formatters.price(order.total)}`}</span>}
                />
                <Column
                  align="center"
                  title="Last Updated"
                  defaultSortOrder="descend"
                  dataIndex={orders.updatedAt}
                  sorter={(a, b) => moment(a.updatedAt) - moment(b.updatedAt)}
                  key="updatedAt"
                  render={(order) => <span>{formatters.datetime(order.updatedAt, 'id')}</span>}
                />
                <Column
                  align="right"
                  dataIndex="status"
                  title="Status"
                  key="status"
                  filters={Object.keys(constants.PROCESS_ORDER_STATUS).map((key) => ({
                    text: constants.PROCESS_ORDER_STATUS[key],
                    value: key,
                  }))}
                  onFilter={(value, record) => value === record.status}
                  render={(status) => (
                    <Badge color={helpers.getBadgeColor(status)} text={constants.PROCESS_ORDER_STATUS[status]} />
                  )}
                />
              </Table>
            </>
          );
        }}
      </Query>
    </div>
  );
};

Orders.propTypes = {
  client: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default withApollo(Orders);
