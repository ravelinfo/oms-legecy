import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Input, Checkbox } from 'antd';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import { gql } from 'apollo-boost';
import { CUSTOMER_ME } from '../graphql/queries';

// Utils
import { errors } from '../../utils';

// Component
import FormContainer from '../commons/FormContainer';
import FormButton from '../commons/FormButton';

const UPDATE_MY_PROFILE = gql`
  mutation UpdateMyProfile(
    $phone: String
    $address: String
    $email: String
    $company: String
    $isEmailNotificationEnabled: Boolean
  ) {
    updateMyProfile(
      phone: $phone
      address: $address
      email: $email
      company: $company
      isEmailNotificationEnabled: $isEmailNotificationEnabled
    ) {
      id
      username
      company
      phone
      address
      email
      isEmailNotificationEnabled
    }
  }
`;

class Setting extends Component {
  constructor() {
    super();
    this.state = {
      company: '',
      phone: '',
      address: '',
      email: '',
      customerId: '',
      isEmailNotificationEnabled: false,
    };
  }

  setFormValue(data) {
    const { phone, address, email, company, id, isEmailNotificationEnabled } = data;
    this.setState({ phone, address, email, company, customerId: id, isEmailNotificationEnabled });
  }

  render() {
    const { company, phone, address, email, customerId, isEmailNotificationEnabled } = this.state;
    return (
      <Query query={CUSTOMER_ME} onCompleted={(data) => this.setFormValue(data.customerMe)}>
        {({ loading, error, data }) => {
          if (loading) return <div>Loading</div>;
          if (error) return <div>{error.message}</div>;
          const { username } = data.customerMe;

          return (
            <FormContainer header="Edit Profile">
              <Form.Item label="Company" colon={false}>
                <Input value={company} onChange={(e) => this.setState({ company: e.target.value })} />
              </Form.Item>
              <Form.Item label="Username" colon={false}>
                <Input value={username} />
              </Form.Item>
              <Form.Item label="Phone" colon={false}>
                <Input value={phone} onChange={(e) => this.setState({ phone: e.target.value })} />
              </Form.Item>
              <Form.Item label="Address" colon={false}>
                <Input value={address} onChange={(e) => this.setState({ address: e.target.value })} />
              </Form.Item>
              <Form.Item label="Email" colon={false}>
                <Input value={email} onChange={(e) => this.setState({ email: e.target.value })} />
              </Form.Item>
              <span
                style={{
                  display: 'block',
                  fontSize: '14px',
                  fontWeight: 'bold',
                  color: '#999',
                  marginBottom: ' 20px ',
                }}
              >
                <Checkbox
                  checked={isEmailNotificationEnabled}
                  onChange={(e) => this.setState({ isEmailNotificationEnabled: e.target.checked })}
                />
                <span style={{ marginLeft: '10px' }}>Email Notification</span>
              </span>
              <Link to={`/customer/edit-setting-password/${customerId}`}>
                <p
                  style={{
                    color: '#666',
                    textDecoration: 'underline',
                    display: 'inline-block',
                  }}
                >
                  Change password
                </p>
              </Link>
              <Mutation
                mutation={UPDATE_MY_PROFILE}
                variables={{ phone, address, email, company, isEmailNotificationEnabled }}
                onError={(err) => errors.handleError(err)}
                onCompleted={() => {
                  const { history } = this.props;
                  history.push('/customer');
                }}
              >
                {(updateMyProfile) => <FormButton onClick={updateMyProfile}>Save</FormButton>}
              </Mutation>
            </FormContainer>
          );
        }}
      </Query>
    );
  }
}

Setting.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default Setting;
